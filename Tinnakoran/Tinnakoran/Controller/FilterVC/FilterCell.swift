//
//  FilterCell.swift
//  Tinnakoran
//
//  Created by Jaydeep on 24/03/20.
//  Copyright © 2020 Yash. All rights reserved.
//

import UIKit

class FilterCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var btnRadio:UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()        
    
        lblTitle.textColor = .appThemeGreenColor
        lblTitle.font = themeFont(size: 17, fontname: .regular)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
