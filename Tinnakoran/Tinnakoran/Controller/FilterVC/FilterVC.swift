//
//  FilterVC.swift
//  Tinnakoran
//
//  Created by Jaydeep on 24/03/20.
//  Copyright © 2020 Yash. All rights reserved.
//

import UIKit
import SwiftyJSON

class FilterVC: UIViewController {
    
    //MARK:- Variable Declaration
    
    var arrFilter:[JSON] = []
    var handlerData:([JSON]) -> Void = {_ in}
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var tblFilter:UITableView!
    @IBOutlet weak var btnOkOutlet:UIButton!
    @IBOutlet weak var btnCancelOutlet:UIButton!
    @IBOutlet weak var constantHeightTbl: NSLayoutConstraint!
    
    //MARK:- ViewLife Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = .clear
        setupUI()
        
        tblFilter.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        tblFilter.removeObserver(self, forKeyPath: "contentSize")
    }
    
    //MARK:- Overide Method
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if object is UITableView {
            
            self.constantHeightTbl.constant = tblFilter.contentSize.height
        }
    }
    
    func setupUI() {
        
        lblTitle.text = "Select Filter"
        lblTitle.textColor = .black
        lblTitle.font = themeFont(size: 17, fontname: .bold)
        
        [btnOkOutlet,btnCancelOutlet].forEach { (btn) in
            btn?.titleLabel?.font = themeFont(size: 15, fontname: .medium)
            btn?.setTitleColor(.appThemeGreenColor, for: .normal)
        }
        
        btnOkOutlet.setTitle("OK", for: .normal)
        btnCancelOutlet.setTitle("Cancel", for: .normal)
    }
    
    @IBAction func btnOkAction(_ sender:UIButton) {
        handlerData(arrFilter)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnCancelAction(_ sender:UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

//MARK:- Table View Delegate & DataSource

extension FilterVC:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrFilter.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FilterCell") as! FilterCell
        let dict = arrFilter[indexPath.row]
        cell.lblTitle.text = dict["value"].stringValue
        if dict["is_selected"].stringValue == "0" {
            cell.btnRadio.isSelected = false
        } else {
           cell.btnRadio.isSelected = true
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        for i in 0..<self.arrFilter.count {
            var dict = self.arrFilter[i]
            dict["is_selected"] = "0"
            self.arrFilter[i] = dict
        }
        self.arrFilter[indexPath.row]["is_selected"].stringValue = "1"
        self.tblFilter.reloadData()
    }
}
