//
//  SaleOrderListVc.swift
//  Tinnakoran
//
//  Created by YASH on 10/10/19.
//  Copyright © 2019 Yash. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON


enum CheckSOParentViewController{
    case SOPending
    case SOAll
}



class SaleOrderDetailsVc: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var vwChangeDate: UIView!
    
    
    @IBOutlet weak var lblCustomeName: UILabel!
    @IBOutlet weak var txtCustomerName: UITextView!
    
    @IBOutlet weak var lblInvoiceAddress: UILabel!
    @IBOutlet weak var txtInvoiceAddress: UITextView!
    
    @IBOutlet weak var lblShippingAddress: UILabel!
    @IBOutlet weak var txtvwShippingAddress: UITextView!
    
    @IBOutlet weak var lblOrderDate: UILabel!
    @IBOutlet weak var txtvwOrderDate: UITextView!
    
    @IBOutlet weak var lblRequestedDate: UILabel!
    @IBOutlet weak var txtRequestedDate: UITextView!
    
    @IBOutlet weak var OrderRefrence: UILabel!
    @IBOutlet weak var txtOrderRefrence: UITextView!
    
    @IBOutlet weak var lblOrderStatus: UILabel!
    @IBOutlet weak var txtOrderStatus: UITextView!
    
    @IBOutlet weak var tblSOList: UITableView!
    
    @IBOutlet weak var constantTblHeight: NSLayoutConstraint!
    
    @IBOutlet weak var btnChangeDate: CustomButton!
    
    
    //MARK: - Check
    
    var dictCusotmerData = JSON()
    var selectedCotroller = CheckSOParentViewController.SOPending
    
    var arraySOList : [JSON] = []
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        
        print("SelectedController : \(selectedCotroller)")
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        self.setupNavigationbar(titleText: "Details")
        
        if selectedCotroller == .SOAll{
            
        }else{
            let rightButton = CustomButton()
            rightButton.frame = CGRect(x: 0, y: 0, width: 60, height: 35)
            rightButton.backgroundColor = UIColor(red: 144.0/255.0, green: 0, blue: 0, alpha: 1)
            rightButton.cornerRadius = 5
            rightButton.setTitle("Urgent", for: .normal)
            rightButton.titleLabel?.font = themeFont(size: 13, fontname: .medium)
            rightButton.addTarget(self, action: #selector(btnUrgentTapped), for: .touchUpInside)
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: rightButton)
            
            self.vwChangeDate.isHidden = false
        }
        
        tblSOList.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        tblSOList.removeObserver(self, forKeyPath: "contentSize")
    }
    
    //MARK: - SetupUI
    
    func setupUI(){
        
        
//        sale_header: name, parent_id = Customer name, invoice_id = Invoice Address, shipping_id = shiping Address, Order Date, Requested Date, client_order_ref = Order Reference, state = Status
        
        lblName.font = themeFont(size: 17, fontname: .medium)
        lblName.textColor = UIColor.appThemeGreenColor
        [lblCustomeName,lblInvoiceAddress,lblShippingAddress,lblOrderDate,lblRequestedDate,OrderRefrence,lblOrderStatus].forEach { (lbl) in
            lbl?.font = themeFont(size: 16, fontname: .regular)
            lbl?.textColor = UIColor.appThemeGreenColor
        }
        [txtCustomerName,txtInvoiceAddress,txtvwShippingAddress,txtvwOrderDate,txtRequestedDate,txtOrderRefrence,txtOrderStatus].forEach { (lbl) in
            lbl?.font = themeFont(size: 16, fontname: .regular)
            lbl?.textColor = UIColor.appThemeLightGrayColor
            lbl?.textContainerInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
        
        tblSOList.register(UINib(nibName: "ContactTblCell", bundle: nil), forCellReuseIdentifier: "ContactTblCell")
        
       btnChangeDate.backgroundColor = UIColor.appThemeLightGrayColor
        
       setData(dict:dictCusotmerData)
        
    }
    //MARK:- Overide Method
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if object is UITableView {
            
            self.constantTblHeight.constant = tblSOList.contentSize.height
        }
    }
    
    func setData(dict:JSON){
        
        print("dict:\(dict)")
        
        lblName.text = dict["name"].stringValue == "" ? "-" : dict["name"].stringValue
        
        txtCustomerName.text = dict["partner_id"][1].stringValue == "" ? "-" : dict["partner_id"][1].stringValue
        
        txtInvoiceAddress.text = dict["partner_invoice_id"][1].stringValue == "" ? "-" : dict["partner_invoice_id"][1].stringValue
        
        txtvwShippingAddress.text = dict["partner_shipping_id"][1].stringValue == "" ? "-" : dict["partner_shipping_id"][1].stringValue
        
        txtvwOrderDate.text = dict["date_order"].stringValue == "" ? "-" : dict["date_order"].stringValue
        
        txtRequestedDate.text = dict["requested_date"].stringValue == "" ? "-" : dict["requested_date"].stringValue
        
        txtOrderRefrence.text = dict["client_order_ref"].stringValue == "" ? "-" : dict["client_order_ref"].stringValue
        
        txtOrderStatus.text = dict["state"].stringValue == "" ? "-" : dict["state"].stringValue
        
        lblOrderStatus.attributedText = setColorAndFontString(str1: "Status : ", str1Color: UIColor.appThemeGreenColor, str2: dict["state"].stringValue == "" ? "-" : dict["state"].stringValue.capitalized, str2Color: UIColor.appThemeLightGrayColor)
        
        self.arraySOList = dict["saleOrderLineList"].arrayValue
        self.tblSOList.reloadData()
    }
    
    @objc func btnUrgentTapped() {
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "SaleOrderUrgentVc") as! SaleOrderUrgentVc
        obj.dictData = dictCusotmerData
        obj.handlerRefreshPendingList = { [weak self] in
           
        }
        self.navigationController?.pushViewController(obj, animated: true)
        
    }
    
    @IBAction func btnChangeDateTapped(_ sender: UIButton) {
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "SaleOrderUrgentVc") as! SaleOrderUrgentVc
        obj.dictData = dictCusotmerData
        obj.isChangeDateButtonClick = true
        obj.handlerRefreshPendingList = { [weak self] in
            
        }
        self.navigationController?.pushViewController(obj, animated: true)
        
    }
    
}

//MARK: - TableView Delegate
extension SaleOrderDetailsVc: UITableViewDelegate,UITableViewDataSource{
    
    //    func numberOfSections(in tableView: UITableView) -> Int {
    //        if self.arrayContactList.count != 0{
    //            return 1
    //        }
    //    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.arraySOList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactTblCell") as! ContactTblCell
        
        cell.imgBack.isHidden = true
        
        let dict = self.arraySOList[indexPath.row]
        
        let strName = dict["name"].stringValue == "" ? "-" : dict["name"].stringValue
        let strProductName = dict["product_id"][1].stringValue == "" ? "-" : dict["product_id"][1].stringValue
        let strQty = dict["product_uom_qty"].stringValue == "" ? "-" : dict["product_uom_qty"].stringValue + " " + dict["product_uom"][1].stringValue
        let strUnitSubTotal = dict["price_subtotal"].stringValue == "" ? "-" : dict["price_subtotal"].stringValue + " \(dictCusotmerData["currency_id"][1].stringValue)"
        let strUnitPrice = dict["price_unit"].stringValue == "" ? "-" : dict["price_unit"].stringValue + " \(dictCusotmerData["currency_id"][1].stringValue)/\(dict["product_uom"][1].stringValue)"
        
    //    let struom = "\(dict["product_uom"][0].stringValue) \(dict["product_uom"][1].stringValue)" == "" ? "-" : "\(dict["product_uom"][0].stringValue) \(dict["product_uom"][1].stringValue)"
        
        var jsontaxList = dict["taxList"].arrayValue
        var arrayStrtaxListy : [String] = []
        for i in 0..<jsontaxList.count{
            arrayStrtaxListy.append(jsontaxList[i]["name"].stringValue)
        }
        
        let taxListy = arrayStrtaxListy.count == 0 ? "-" : arrayStrtaxListy.joined(separator: ", ")
        
        let name = setColorAndFontString(str1:"Description: " ,str2: strName+"\n")
        let productName = setColorAndFontString(str1:"Product Name: " ,str2: strProductName+"\n")
        let qty = setColorAndFontString(str1:"Quantity: " ,str2: strQty+"\n")
    //    let uom = setColorAndFontString(str1:"UnitOfMeasure: " ,str2: struom+"\n")
        let unitSubtotal = setColorAndFontString(str1:"Unit Subtotal: " ,str2: strUnitSubTotal+"\n")
        let unitSubPrice = setColorAndFontString(str1:"Unit Price: " ,str2: strUnitPrice+"\n")
        let texasList = setColorAndFontString(str1:"Taxes: " ,str2: taxListy)
        
        let attributedString = NSMutableAttributedString()
        attributedString.append(productName)
        attributedString.append(name)
        attributedString.append(qty)
    //    attributedString.append(uom)
        attributedString.append(unitSubPrice)
        attributedString.append(unitSubtotal)
        attributedString.append(texasList)
        
        cell.lblMain.attributedText = attributedString
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if self.arraySOList.count != 0{
            let lbl = UILabel()
            lbl.text = "Sale order lines"
            lbl.textColor = UIColor.black
            lbl.font = themeFont(size: 14, fontname: .regular)
            return lbl.text
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if self.arraySOList.count != 0{
            return 50
        }
        return 0
    }
}

