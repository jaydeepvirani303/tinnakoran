//
//  SalePriceRequestListVc.swift
//  Tinnakoran
//
//  Created by YASH on 11/1/19.
//  Copyright © 2019 Yash. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON

class SalePriceRequestListVc: UIViewController {

    @IBOutlet weak var vwSearch: UIView!
    @IBOutlet weak var txtSearch: CustomTextField!
    @IBOutlet weak var tblPriceList: UITableView!
    
    var dictCusotmerData = JSON()

    var arrayPriceList : [JSON] = []
    var selectedController = fromHomeOrCustomer.customer
    var nextOffset = 0


    override func viewDidLoad() {
        super.viewDidLoad()

        tblPriceList.register(UINib(nibName: "ContactTblCell", bundle: nil), forCellReuseIdentifier: "ContactTblCell")
        tblPriceList.tableFooterView = UIView()
        
        getPriceRequestList(isRefreshing: true)

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        
        self.setupNavigationbar(titleText: "Sale's price request")
        
        if selectedController == .customer{
            let rightButton = UIBarButtonItem(image: UIImage(named:"add-plus-button") , style: .plain, target: self, action: #selector(btnAddPriceRequestTapped))
            rightButton.tintColor = .white
            self.navigationItem.rightBarButtonItem = rightButton
            
            self.vwSearch.isHidden = true
        }else{
            print("Nothing")
            self.vwSearch.isHidden = false
        }
        
    }
    
    @objc func btnAddPriceRequestTapped(){
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "AddEditPriceRequestVc") as! AddEditPriceRequestVc
        obj.selectedCotroller = .add
        obj.customerData = self.dictCusotmerData
        obj.handlerForAddPriceRequest = { [weak self] in
            self?.getPriceRequestList()
        }
        self.navigationController?.pushViewController(obj, animated: true)
        
    }

}

extension SalePriceRequestListVc: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.arrayPriceList.count == 0{
            let lbl = UILabel()
            lbl.text = "No record found."
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = UIColor.black
            lbl.center = tableView.center
            tableView.backgroundView = lbl
            return 0
        }else{
            tableView.backgroundView = nil
            return self.arrayPriceList.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactTblCell") as! ContactTblCell
        
        let dict = self.arrayPriceList[indexPath.row]
        
        let strName = dict["name"].stringValue == "" ? "-" : dict["name"].stringValue
        let strCustomerName = dict["partner_id"][1].stringValue == "" ? "-" : dict["partner_id"][1].stringValue
        let strApproverName = dict["approver_id"][1].stringValue == "" ? "-" : dict["approver_id"][1].stringValue
        let strRequestedDate = dict["request_date"].stringValue == "" ? "-" : dict["request_date"].stringValue
        
        let strState = dict["state"].stringValue == "" ? "-" : dict["state"].stringValue
        
        let name = setColorAndFontString(str1:"Name: " ,str2: strName+"\n")
        let customerName = setColorAndFontString(str1:"Customer Name: " ,str2: strCustomerName+"\n")
        let approverName = setColorAndFontString(str1:"Approver Name: " ,str2: strApproverName+"\n")
        let requestedDate = setColorAndFontString(str1:"Requested Date: " ,str2: strRequestedDate+"\n")
        let state = setColorAndFontString(str1:"State: " ,str2: strState)
        
        let attributedString = NSMutableAttributedString()
        attributedString.append(name)
        attributedString.append(customerName)
        attributedString.append(approverName)
        attributedString.append(requestedDate)
        attributedString.append(state)
        
        cell.lblMain.attributedText = attributedString
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let dict = self.arrayPriceList[indexPath.row]
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "SalePriceRequestDetailsVc") as! SalePriceRequestDetailsVc
        obj.dictCusotmerData = self.dictCusotmerData
        obj.dictData = dict
        if selectedController == .customer{
            obj.selectedController = .customer
        }else if selectedController == .home{
            obj.selectedController = .home
        }
        self.navigationController?.pushViewController(obj, animated: true)
        
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if selectedController == .home{
            let lastSectionIndex = tableView.numberOfSections - 1
            
            let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
            
            if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && arrayPriceList != [] && arrayPriceList.count >= 10 {
                
                let spinner = UIActivityIndicatorView(style: .gray)
                spinner.color = UIColor.appThemeGreenColor
                spinner.startAnimating()
                spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
                tableView.tableFooterView = spinner
                tableView.tableFooterView?.isHidden = false
                
                self.nextOffset += 10
                
                if (txtSearch.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
                {
                    self.getPriceRequestList()
                }else{
                    self.searchLoggedCalls()
                }
                
            } else {
                tableView.tableFooterView?.isHidden = true
                tableView.tableFooterView = nil
            }
        }else{
            print("Nothing")
        }
        
    }
}

//MARK; - TextField Delegate

extension SalePriceRequestListVc:UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        self.nextOffset = 0
        searchLoggedCalls(isRefreshing: true)
        return true
        
    }
    
}



//MARK: - API calling

extension SalePriceRequestListVc{
    
    func getPriceRequestList(isRefreshing:Bool = false){
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            var url = ""
            
            if selectedController == .customer{
                url = "\(GlobalVariable.baseURL)getSalesPriceRequest?UserId=\(getUserDetail("user_id"))&Username=\(getUserDetail("username"))&Password=\(getUserDetail("password"))&Company=\(getUserDetail("company_id"))&CustomerId=\(dictCusotmerData["id"].stringValue)"
            }else
            {
                url = "\(GlobalVariable.baseURL)getSalesPriceRequest?UserId=\(getUserDetail("user_id"))&Username=\(getUserDetail("username"))&Password=\(getUserDetail("password"))&Company=\(getUserDetail("company_id"))&CustomerId=0&offset=\(nextOffset)&pazeSize=10"
            }
            
            print("URL: \(url)")
            
            if isRefreshing{
                self.showLoader()
            }
            
            
            CommonService().ServiceGETMethod(url: url, param: [:]) { (respones) in
                
                if isRefreshing{
                    self.hideLoader()
                }
                
                
                if let json = respones.value{
                    print("JSON : \(json)")
                    
                    if json["isError"].boolValue == true{
                        makeToast(strMessage: "Something went wrong")
                        return
                    }
                    
                    
                    if self.selectedController == .customer{
                        self.arrayPriceList = []
                        self.arrayPriceList = json.arrayValue
                    }
                    else if self.selectedController == .home{
                        if self.nextOffset == 0{
                            self.arrayPriceList = []
                        }
                        
                        let aryData = json.arrayValue
                        self.arrayPriceList = self.arrayPriceList + aryData
                        
                    }

                    print("self.arraSoList:\(self.arrayPriceList)")
                    
                    self.tblPriceList.reloadData()
                    
                }
                else
                {
                    makeToast(strMessage: GlobalVariable.serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: GlobalVariable.interNotWorking)
        }
    }
    
    func searchLoggedCalls(isRefreshing:Bool = false){
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            print("nextOffset:\(nextOffset)")
            
            var strSearch = (txtSearch.text?.trimmingCharacters(in: .whitespaces)) ?? ""
            
            strSearch = strSearch.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
            
            let url = "\(GlobalVariable.baseURL)getSalesPriceRequest?UserId=\(getUserDetail("user_id"))&Username=\(getUserDetail("username"))&Password=\(getUserDetail("password"))&Company=\(getUserDetail("company_id"))&CustomerId=0&offset=\(nextOffset)&pazeSize=10&salsepricereuestName=\(strSearch)"
            
            print("URL: \(url)")
            
            if isRefreshing{
                self.showLoader()
            }
            
            CommonService().ServiceGETMethod(url: url, param: [:]) { (respones) in
                if isRefreshing{
                    self.hideLoader()
                }
                
                if let json = respones.value{
                    print("JSON : \(json)")
                    
                    if self.nextOffset == 0{
                        self.arrayPriceList = []
                    }
                    
                    let aryData = json.arrayValue
                    self.arrayPriceList = self.arrayPriceList + aryData
                    self.tblPriceList.reloadData()
                    
                }
                else
                {
                    makeToast(strMessage: GlobalVariable.serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: GlobalVariable.interNotWorking)
        }
        
    }

    
}

