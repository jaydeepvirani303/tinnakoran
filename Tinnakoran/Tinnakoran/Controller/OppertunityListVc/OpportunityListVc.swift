//
//  OpportunityListVc.swift
//  Tinnakoran
//
//  Created by YASH on 10/13/19.
//  Copyright © 2019 Yash. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import AlamofireSwiftyJSON

class OpportunityListVc: UIViewController {

    @IBOutlet weak var vwSearch: UIView!
    @IBOutlet weak var tblOppertunity: UITableView!
    @IBOutlet weak var txtSearch: CustomTextField!
    
    var dictCusotmerData = JSON()
    var arrayOpportunity : [JSON] = []
    var arrFilter :[JSON] = []
    var strFilterName = String()
    var dictContact = JSON()
    
    var selectedController = fromHomeOrCustomer.customer
    var nextOffset = 0
    var isDataOver = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblOppertunity.register(UINib(nibName: "CustomerTblCell", bundle: nil), forCellReuseIdentifier: "CustomerTblCell")
        
        tblOppertunity.tableFooterView = UIView()
        
        getOppoertunityList(isRefreshing: true)
        getOppoertunityFilterList()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        self.setupNavigationbar(titleText: "Opportunity")
        if selectedController == .customer{
            let rightButton = UIBarButtonItem(image: UIImage(named:"add-plus-button") , style: .plain, target: self, action: #selector(btnAddOpportunity))
            rightButton.tintColor = .white
//            self.navigationItem.rightBarButtonItem = rightButton
            
            self.vwSearch.isHidden = true
            
            let btnFilter = UIBarButtonItem(image: UIImage(named:"menu") , style: .plain, target: self, action: #selector(btnFilterAction))
            btnFilter.tintColor = .white
            self.navigationItem.rightBarButtonItems = [rightButton,btnFilter]
            
            self.vwSearch.isHidden = true
            
        }else {
            print("Nothing")
            let btnFilter = UIBarButtonItem(image: UIImage(named:"menu") , style: .plain, target: self, action: #selector(btnFilterAction))
            btnFilter.tintColor = .white
            self.navigationItem.rightBarButtonItems = [btnFilter]
            self.vwSearch.isHidden = false
        }
    }
    
    @objc func btnAddOpportunity(){
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "AddEditOpportunityVc") as! AddEditOpportunityVc
        obj.selectedController = .add
        obj.dictCustomerData = self.dictCusotmerData
        obj.dictContact = dictContact
        obj.handlerForAddOpportunity = { [weak self] in
            self?.getOppoertunityList()
        }
        self.navigationController?.pushViewController(obj, animated: true)
        
    }
    
    @objc func btnFilterAction(){
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "FilterVC") as! FilterVC
        obj.arrFilter = self.arrFilter
        obj.modalPresentationStyle = .overFullScreen
        obj.modalTransitionStyle = .coverVertical
        obj.handlerData = { [weak self] arr in
            self?.arrFilter = arr
            for i in 0..<self!.arrFilter.count {
                let dict = self?.arrFilter[i]
                if dict!["is_selected"].stringValue == "1" {
                    self?.strFilterName = dict!["key"].stringValue
                    self?.isDataOver = false
                    self?.nextOffset = 0
                    self?.getOppoertunityList(isRefreshing: true, name: dict!["key"].stringValue)
                    break
                }
            }
        }
        self.present(obj, animated: true, completion: nil)
    }
    
    @objc func btnAddLoggedCalledForOpportunity(sender:UIButton){
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "AddEditLoggedCallVc") as! AddEditLoggedCallVc
        obj.selectedController = .add
        obj.isComeFromOpportunity = true
        obj.customerData = self.dictCusotmerData
        obj.dictContact = dictContact
        obj.dictOpportunityDetail = self.arrayOpportunity[sender.tag]
        obj.handlerForAddLoggedCalls = { [weak self] in
            
        }
        self.navigationController?.pushViewController(obj, animated: true)
        
    }
}

//MARK; - TextField Delegate

extension OpportunityListVc:UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        self.isDataOver = false
        self.nextOffset = 0
        searchOppoertunityCalls(isRefreshing: true,name: strFilterName)
        return true
        
    }
    
}


//MARK: - TableView Delegate
extension OpportunityListVc: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.arrayOpportunity.count == 0{
            let lbl = UILabel()
            lbl.text = "No record found."
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = UIColor.black
            lbl.center = tableView.center
            tableView.backgroundView = lbl
            return 0
        }else{
            tableView.backgroundView = nil
            return self.arrayOpportunity.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomerTblCell") as! CustomerTblCell
        
        if selectedController == .home{
            cell.vwAddLoggedCall.isHidden = true
        }else
        {
            cell.vwAddLoggedCall.isHidden = false
        }
        
        cell.viewOpportunity.isHidden = false
        
        let dict = self.arrayOpportunity[indexPath.row]
        
        cell.lblStageOpportunity.backgroundColor =  UIColor.hexStringToUIColor(hex: dict["color"].stringValue)
        cell.lblStageOpportunity.text = " \(dict["stage_id"][1].stringValue) "
        cell.lblStageOpportunity.layer.cornerRadius = 5
        cell.lblStageOpportunity.clipsToBounds = true
        cell.lblStageOpportunity.textColor = .white
        cell.lblStageOpportunity.font = themeFont(size: 15, fontname: .regular)
        
        cell.btnAddLoggedCall.tag = indexPath.row
        cell.btnAddLoggedCall.addTarget(self, action: #selector(btnAddLoggedCalledForOpportunity), for: .touchUpInside)
        
        cell.contentView.backgroundColor = UIColor.white
        cell.lblHeaader.attributedText = setColorAndFontString(str1:"\(dict["name"].stringValue)\n" ,str2: "Customer Name : \(dict["partner_id"][1].stringValue)")
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let dict = self.arrayOpportunity[indexPath.row]
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "OppertunityDetailsVc") as! OppertunityDetailsVc
        obj.dictData = dict
        obj.dictCustomerData = self.dictCusotmerData
        obj.dictContact = dictContact
        if selectedController == .customer{
            obj.selectedController = .customer
        }else if selectedController == .home{
            obj.selectedController = .home
        }
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if selectedController == .home{
            let lastSectionIndex = tableView.numberOfSections - 1
            
            let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
            
            if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && arrayOpportunity != [] && arrayOpportunity.count >= 10 && self.isDataOver == false{
                
                let spinner = UIActivityIndicatorView(style: .gray)
                spinner.color = UIColor.appThemeGreenColor
                spinner.startAnimating()
                spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
                tableView.tableFooterView = spinner
                tableView.tableFooterView?.isHidden = false
                
                self.nextOffset += 10
                
                if (txtSearch.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
                {
                    self.getOppoertunityList(name: strFilterName)
                }else{
                    self.searchOppoertunityCalls(name: strFilterName)
                }
                
            } else {
                tableView.tableFooterView?.isHidden = true
                tableView.tableFooterView = nil
            }
        }else{
            print("Nothing")
        }
        
    }
}

//MARK: - API calling

extension OpportunityListVc{
    
    func getOppoertunityList(isRefreshing:Bool = false,name:String = ""){
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            var url = ""
            
            if selectedController == .customer{
                url = "\(GlobalVariable.baseURL)getOpportunities?UserId=\(getUserDetail("user_id"))&Username=\(getUserDetail("username"))&Password=\(getUserDetail("password"))&Company=\(getUserDetail("company_id"))&CustomerId=\(dictCusotmerData["id"].stringValue)&filter=\(name)"
            }
            else if selectedController == .home{
                url = "\(GlobalVariable.baseURL)getOpportunities?UserId=\(getUserDetail("user_id"))&Username=\(getUserDetail("username"))&Password=\(getUserDetail("password"))&Company=\(getUserDetail("company_id"))&CustomerId=0&offset=\(nextOffset)&pazeSize=10&filter=\(name)"
            }
            
            print("URL: \(url)")
            
            url = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            
            if isRefreshing{
                self.showLoader()
            }
            
            
            CommonService().ServiceGETMethod(url: url, param: [:]) { (respones) in
                
                if isRefreshing{
                    self.hideLoader()
                }
                
                if let json = respones.value{
                    print("JSON : \(json)")
                    
                   
                    if json["isError"].boolValue == true{
                        makeToast(strMessage: "Something went wrong")
                        return
                    }
                    if json.arrayValue.count == 0  {
                        self.isDataOver = true
                        if self.nextOffset == 0{
                            self.arrayOpportunity = []
                            self.tblOppertunity.reloadData()
                        }
                        return
                    }
                    
                    if self.selectedController == .customer{
                        self.arrayOpportunity = []
                        self.arrayOpportunity = json.arrayValue
                    }
                    else if self.selectedController == .home{
                        if self.nextOffset == 0{
                            self.arrayOpportunity = []
                        }
                        
                        let aryData = json.arrayValue
                        self.arrayOpportunity = self.arrayOpportunity + aryData

                    }
                    
                    self.tblOppertunity.reloadData()
                    
                }
                else
                {
                    makeToast(strMessage: GlobalVariable.serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: GlobalVariable.interNotWorking)
        }
    }
    
    func getOppoertunityFilterList(isRefreshing:Bool = false){
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url =  "\(GlobalVariable.baseURL)getOpportunitiesFilter"
            
            CommonService().ServiceGETMethod(url: url, param: [:]) { (respones) in
                
                if isRefreshing{
                    self.hideLoader()
                }
                
                if let json = respones.value{
                    print("JSON : \(json)")
                    
                   if json["isError"].boolValue == true{
                        makeToast(strMessage: "Something went wrong")
                        return
                    }
                    
                    self.arrFilter = json.arrayValue
                    for i in 0..<self.arrFilter.count {
                        var dict = self.arrFilter[i]
                        dict["is_selected"] = "0"
                        self.arrFilter[i] = dict
                    }
                    
                }
                else
                {
                    makeToast(strMessage: GlobalVariable.serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: GlobalVariable.interNotWorking)
        }
    }
    
    
    func searchOppoertunityCalls(isRefreshing:Bool = false,name:String = ""){
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            print("nextOffset:\(nextOffset)")
            
            var strSearch = (txtSearch.text?.trimmingCharacters(in: .whitespaces)) ?? ""
            
            strSearch = strSearch.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
            
            let url = "\(GlobalVariable.baseURL)getopportunities?UserId=\(getUserDetail("user_id"))&Username=\(getUserDetail("username"))&Password=\(getUserDetail("password"))&Company=\(getUserDetail("company_id"))&CustomerId=0&offset=\(nextOffset)&pazeSize=10&opportunityName=\(strSearch)&filter=\(name)"
            
            print("URL: \(url)")
            
            if isRefreshing{
                self.showLoader()
            }
            
            CommonService().ServiceGETMethod(url: url, param: [:]) { (respones) in
                if isRefreshing{
                    self.hideLoader()
                }
                
                if let json = respones.value{
                    print("JSON : \(json)")
                    
                    if json.arrayValue.count == 0  {
                        self.isDataOver = true
                        if self.nextOffset == 0{
                            self.arrayOpportunity = []
                            self.tblOppertunity.reloadData()
                        }
                        return
                    }  
                    
                    if self.nextOffset == 0{
                        self.arrayOpportunity = []
                    }
                    
                    let aryData = json.arrayValue
                    self.arrayOpportunity = self.arrayOpportunity + aryData
                    self.tblOppertunity.reloadData()
                    
                }
                else
                {
                    makeToast(strMessage: GlobalVariable.serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: GlobalVariable.interNotWorking)
        }
        
    }

    
}

