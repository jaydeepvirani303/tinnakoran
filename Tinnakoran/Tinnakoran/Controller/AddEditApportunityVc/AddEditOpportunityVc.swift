//
//  AddEditOpportunityVc.swift
//  Tinnakoran
//
//  Created by YASH on 10/13/19.
//  Copyright © 2019 Yash. All rights reserved.
//

import UIKit
import AlamofireSwiftyJSON
import Alamofire
import SwiftyJSON
import DropDown

class AddEditOpportunityVc: UIViewController {

    @IBOutlet weak var txtOpportunintesname: CustomTextField!
    @IBOutlet weak var txtContactName: CustomTextField!
    @IBOutlet weak var txtCustomerName: CustomTextField!
    @IBOutlet weak var txtAddress1: CustomTextField!
    @IBOutlet weak var txtAddress2: CustomTextField!
    @IBOutlet weak var txtCity: CustomTextField!
    @IBOutlet weak var txtZip: CustomTextField!
    @IBOutlet weak var txtPhone: CustomTextField!
    @IBOutlet weak var txtMobile: CustomTextField!
    @IBOutlet weak var txtPosition: CustomTextField!
    @IBOutlet weak var txtCountry: CustomTextField!
    @IBOutlet weak var txtEmail: CustomTextField!
    @IBOutlet weak var txtFax: CustomTextField!
    @IBOutlet weak var txtPriority: CustomTextField!
    @IBOutlet weak var txtRevenue: CustomTextField!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var txtDescription: UITextView!
    @IBOutlet weak var txtProbability: CustomTextField!
    @IBOutlet weak var btnAddEdit: CustomButton!
    @IBOutlet weak var txtStage: CustomTextField!
    
    var dictData = JSON()
    var dictCustomerData = JSON()
    var dictContact = JSON()
    
    var selectedController = isForAddOrEdit.add
    var handlerForAddOpportunity: ()->Void = {}
    var handlerForEditOpportunity: (JSON) -> Void = {_ in}
    
    var countryDD = DropDown()
    var selectedCountryFromList = ""
    var jsonCountryList : [JSON] = []
    var strCountryList = [String]()
    
    var priorityDD = DropDown()
    var selectedpriorityFromList = ""
    var jsonPriorityList : [JSON] = []
    var strPriorityList = [String]()
    
    var stageDD = DropDown()
    var selectedStageFromList = ""
    var jsonStageList : [JSON] = []
    var strStageList = [String]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        
        self.configDD(dropdown: self.countryDD, sender: self.txtCountry)
        self.configDD(dropdown: self.priorityDD, sender: self.txtPriority)
        self.configDD(dropdown: self.stageDD, sender: self.txtStage)
        
        selectionIndex()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        if selectedController == .add{
            self.setupNavigationbar(titleText: "Add Opportunity")
            btnAddEdit.setTitle("Add", for: .normal)
        }else if selectedController == .edit{
            self.setupNavigationbar(titleText: "Edit Opportunity")
            btnAddEdit.setTitle("Edit", for: .normal)
            self.setDataForEdit(dict: dictData)
        }
        
    }

    func setupUI(){
        
        [txtCustomerName,txtAddress1,txtAddress2,txtCity,txtZip,txtPosition,txtPhone,txtMobile,txtEmail,txtFax,txtOpportunintesname,txtContactName,txtPriority,txtRevenue,txtProbability,txtCountry,txtStage].forEach { (txt) in
            txt?.placeHolderColor = UIColor.lightGray
            txt?.delegate = self
            txt?.font = themeFont(size: 13, fontname: .regular)
        }
        
        txtDescription.font = themeFont(size: 13, fontname: .regular)
        txtDescription.delegate = self
        
        btnAddEdit.backgroundColor = UIColor.appThemeLightGrayColor
        
        self.countryList()
        
        self.priorityList()
        
        self.stageList()
    }
    
    func selectionIndex()
    {
        
        self.countryDD.selectionAction = { (index, item) in
            self.txtCountry.text = item
            self.view.endEditing(true)
            
            self.selectedCountryFromList = self.jsonCountryList[index]["id"].stringValue
            
            //            self.txtSelectCarType.text =
            //            self.txtSelectSeat.text = ""
            self.countryDD.hide()
        }
        
        self.priorityDD.selectionAction = { (index, item) in
            self.txtPriority.text = item
            self.view.endEditing(true)
            
            self.selectedpriorityFromList = self.jsonPriorityList[index]["key"].stringValue
            
            //            self.txtSelectCarType.text =
            //            self.txtSelectSeat.text = ""
            self.priorityDD.hide()
        }
        
        self.stageDD.selectionAction = { (index, item) in
            self.txtStage.text = item
            self.view.endEditing(true)
            
            self.selectedStageFromList = self.jsonStageList[index]["id"].stringValue
            
            //            self.txtSelectCarType.text =
            //            self.txtSelectSeat.text = ""
            self.stageDD.hide()
        }
    }
    
    //MARK: - DropDown setup
    
    func configDD(dropdown: DropDown, sender: UIView)
    {
        dropdown.anchorView = sender
        dropdown.direction = .any
        dropdown.dismissMode = .onTap
        //  dropdown.topOffset = CGPoint(x: 0, y: self.view.bounds.origin.y)
        dropdown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        // print("textfield Frame \(txtPurpose.frame)")
        dropdown.width = sender.bounds.width
        dropdown.cellHeight = 40.0
        dropdown.backgroundColor = UIColor.white
        dropdown.textColor = UIColor.black
        dropdown.selectionBackgroundColor = UIColor.clear
        
    }
    @IBAction func btnCountryTapped(_ sender: UIButton) {
        countryDD.show()
    }
    @IBAction func priorityTapped(_ sender: UIButton) {
        priorityDD.show()
    }
    
    @IBAction func btnStageTapped(_ sender: UIButton) {
        stageDD.show()
    }
    
    @IBAction func btnAddEditTapped(_ sender: UIButton) {
        
        if (txtOpportunintesname.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: "Please enter opportunity name")
        }
        else
        {
            AddEditOpportuntiyAPICalled()
        }
        
    }
    
    func setDataForEdit(dict:JSON){
        
        txtOpportunintesname.text = dict["name"].stringValue
        txtContactName.text = dict["contact_name"].stringValue
        txtCustomerName.text = dict["partner_id"][1].stringValue
        txtAddress1.text = dict["street"].stringValue
        txtAddress2.text = dict["street2"].stringValue
        txtCity.text = dict["city"].stringValue
        txtZip.text = dict["zip"].stringValue
        txtPosition.text = dict["function"].stringValue
        txtPhone.text = dict["phone"].stringValue
        txtMobile.text = dict["mobile"].stringValue
        txtEmail.text = dict["user_email"].stringValue
        txtFax.text = dict["fax"].stringValue
        txtPriority.text = dict["priority"].stringValue
        txtRevenue.text = dict["planned_revenue"].stringValue
        txtProbability.text = dict["probability"].stringValue
        txtStage.text = dict["stage_id"][1].stringValue
        
        self.selectedpriorityFromList = dict["priority"].stringValue
        
        if dict["description"].stringValue == ""{
            self.lblDescription.isHidden = false
        }else{
            self.lblDescription.isHidden = true
            txtDescription.text = dict["description"].stringValue
        }
        
        
        if dict["country_id"].arrayValue.count == 0{
            txtCountry.text = ""
        }
        else
        {
            self.selectedCountryFromList = dict["country_id"][0].stringValue
            txtCountry.text = dict["country_id"][1].stringValue
        }
        
        if dict["stage_id"].arrayValue.count == 0{
            txtStage.text = ""
        }
        else
        {
            self.selectedStageFromList = dict["stage_id"][0].stringValue
            txtStage.text = dict["stage_id"][1].stringValue
        }
        
    }

}


//MARK: - TextField Delegate

extension AddEditOpportunityVc: UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtRevenue || textField == txtProbability
        {
            let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            return string == numberFiltered
        }
        return true
        
    }
}

//MARK:- Textview Delegate

extension AddEditOpportunityVc:UITextViewDelegate
{
    func textViewDidChange(_ textView: UITextView) {
        if textView.text == ""
        {
            self.lblDescription.isHidden = false
        }
        else
        {
            self.lblDescription.isHidden = true
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn shouldChangeTextInRange: NSRange, replacementText text: String) -> Bool
    {
        if text == "\n"  // Recognizes enter key in keyboard
        {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}

//MARK: - API calling

extension AddEditOpportunityVc{
    
    func AddEditOpportuntiyAPICalled()
    {
        self.view.endEditing(true)
        
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
             let url = "\(GlobalVariable.baseURL)postOpportunities"
            
            let userID = Int(getUserDetail("user_id"))
            var id = Int()
            if dictContact.count != 0 {
                id = self.dictContact["id"].intValue
            } else {
                id = self.dictCustomerData["id"].intValue
            }
            var param = [
                "UserId": Int(userID!),
                "UserName":getUserDetail("username"),
                "Password":getUserDetail("password"),
                "Company":getUserDetail("company_id"),
                "CustomerId" : id,
                "OpportunitiesName" : self.txtOpportunintesname.text ?? "",
                "ContactName" : txtContactName.text ?? "",
                "CustomerName":txtCustomerName.text ?? "",
                "Address1": txtAddress1.text ?? "",
                "Address2":txtAddress2.text ?? ""
                
                ] as [String : Any]
            
            param["PlannedRevenue"] = Int(txtRevenue.text ?? "0")
            param["Probability"] = Int(txtProbability.text ?? "0")
            param["City"] = txtCity.text ?? ""
            param["Zip"] = txtZip.text ?? ""
            param["Position"] = txtPosition.text ?? ""
            param["Phone"] = txtPhone.text ?? ""
            param["Mobile"] = txtMobile.text ?? ""
            param["Email"] = txtEmail.text ?? ""
            param["Fax"] = txtFax.text ?? ""
            param["Description"] = txtDescription.text ?? ""
            param["Priority"] = self.selectedpriorityFromList
            param["CountryId"] = Int(self.selectedCountryFromList)
            param["StageId"] = Int(self.selectedStageFromList)
            
            if selectedController == .add{
                param["OpportunitiesId"] = 0
            }else if selectedController == .edit{
                param["OpportunitiesId"] = self.dictData["id"].intValue
            }
            
            print("param : \(param)")
            
            self.showLoader()
            
            CommonService().ServicePostMethod(url: url, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value{
                    print("JSON : \(json)")
                    
                    if self.selectedController == .add{
                        self.handlerForAddOpportunity()
                        self.navigationController?.popViewController(animated: true)
                    }else if self.selectedController == .edit{
                       // self.handlerForEditOpportunity(json)
                        self.redirectToCustomerDetailsScreen()
                    }
                    
                }
                else
                {
                    makeToast(strMessage: GlobalVariable.serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: GlobalVariable.interNotWorking)
        }
 
    }
    
    
    func countryList(){
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(GlobalVariable.baseURL)getCountries?UserId=\(getUserDetail("user_id"))&Username=\(getUserDetail("username"))&Password=\(getUserDetail("password"))&Company=\(getUserDetail("company_id"))"
            
            print("URL: \(url)")
            
            self.showLoader()
            
            CommonService().ServiceGETMethod(url: url, param: [:]) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value{
                    print("JSON : \(json)")
                    
                    self.jsonCountryList = json.arrayValue
                    
                    for i in 0..<self.jsonCountryList.count
                    {
                        let value = self.jsonCountryList[i]["name"].stringValue
                        self.strCountryList.append(value)
                    }
                    
                    self.countryDD.dataSource = self.strCountryList
                }
                else
                {
                    makeToast(strMessage: GlobalVariable.serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: GlobalVariable.interNotWorking)
        }
        
    }

    
//    \(GlobalVariable.baseURL)getPriorities
    
    
    func priorityList(){
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(GlobalVariable.baseURL)getPriorities"
            
//            ?UserId=\(getUserDetail("user_id"))&Username=\(getUserDetail("username"))&Password=\(getUserDetail("password"))&Company=\(getUserDetail("company_id"))"
            
            print("URL: \(url)")
            
            self.showLoader()
            
            CommonService().ServiceGETMethod(url: url, param: [:]) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value{
                    print("JSON : \(json)")
                    
                    self.jsonPriorityList = json.arrayValue
                    
                    for i in 0..<self.jsonPriorityList.count
                    {
                        let value = self.jsonPriorityList[i]["value"].stringValue
                        self.strPriorityList.append(value)
                    }
                    
                    self.priorityDD.dataSource = self.strPriorityList
                }
                else
                {
                    makeToast(strMessage: GlobalVariable.serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: GlobalVariable.interNotWorking)
        }
        
    }
    
    
    
    func stageList(){
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(GlobalVariable.baseURL)getStages?UserId=\(getUserDetail("user_id"))&Username=\(getUserDetail("username"))&Password=\(getUserDetail("password"))&Company=\(getUserDetail("company_id"))"
            
            print("URL: \(url)")
            
            self.showLoader()
            
            CommonService().ServiceGETMethod(url: url, param: [:]) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value{
                    print("JSON : \(json)")
                    
                    self.jsonStageList = json.arrayValue
                    
                    for i in 0..<self.jsonStageList.count
                    {
                        let value = self.jsonStageList[i]["name"].stringValue
                        self.strStageList.append(value)
                    }
                    
                    self.stageDD.dataSource = self.strStageList
                }
                else
                {
                    makeToast(strMessage: GlobalVariable.serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: GlobalVariable.interNotWorking)
        }
        
    }
    
}

