//
//  SelectOptionsVc.swift
//  Tinnakoran
//
//  Created by YASH on 10/6/19.
//  Copyright © 2019 Yash. All rights reserved.
//

import UIKit
import AlamofireSwiftyJSON
import Alamofire
import SwiftyJSON

class SelectOptionsVc: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var lblRepeatSelling: UILabel!
    @IBOutlet weak var lblCrossSelling: UILabel!
    @IBOutlet weak var lblNewCustomer: UILabel!
    @IBOutlet weak var lblLoggedVisit: UILabel!
    @IBOutlet weak var lblOngoingProject: UILabel!
    
    
    @IBOutlet weak var btnDashboard: CustomButton!
    @IBOutlet weak var btnProduct: CustomButton!
    @IBOutlet weak var btnCustomer: CustomButton!
    @IBOutlet weak var btnLogout: CustomButton!
    
    @IBOutlet weak var btnRefresh: UIButton!
    //MARK - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        getData()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    func setupUI(){
       
//        [btnDashboard,btnProduct,btnCustomer,btnLogout].forEach { (btn) in
//            btn?.backgroundColor = UIColor.appThemeLightGrayColor
//            btn?.titleLabel?.font = themeFont(size: 15, fontname: .bold)
//        }
        
        let image = UIImage(named: "refresh_icon")?.withRenderingMode(.alwaysTemplate)
        btnRefresh.setImage(image, for: .normal)
        btnRefresh.tintColor = UIColor.white

    }
    
    @IBAction func btnAllOpportunityTapped(_ sender: UIButton) {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "OpportunityListVc") as! OpportunityListVc
        obj.selectedController = .home
        self.navigationController?.pushViewController(obj, animated: true)

        
    }
    
    @IBAction func btnRefreshTapped(_ sender: UIButton) {
        getData()
    }
    @IBAction func btnAllPriceRequestListTapped(_ sender: UIButton) {
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "SalePriceRequestListVc") as! SalePriceRequestListVc
        obj.selectedController = .home
        self.navigationController?.pushViewController(obj, animated: true)
        
    }
    @IBAction func btnLoggedCallsTapped(_ sender: UIButton) {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "LoggedCallListVc") as! LoggedCallListVc
        obj.selectedController = .home
        self.navigationController?.pushViewController(obj, animated: true)
        
    }
    
    @IBAction func btnAllVisitPlanTapped(_ sender: UIButton) {
    }
    
}

//MARK: -IBAction
extension SelectOptionsVc{
    
    @IBAction func btnDashboardTapped(_ sender: UIButton) {

        let obj = self.storyboard?.instantiateViewController(withIdentifier: "DashboardVc") as! DashboardVc
        self.navigationController?.pushViewController(obj, animated: true)
        
    }
    
    @IBAction func btnCustomer(_ sender: UIButton) {
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "CustomerListVc") as! CustomerListVc
        self.navigationController?.pushViewController(obj, animated: true)
        
    }
    
    @IBAction func btnMessagesAction(_ sender: UIButton) {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "CreateMessageVC") as! CreateMessageVC
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnLogoutTapped(_ sender: UIButton) {
        
        let alertController = UIAlertController(title: "", message: "Are you sure you want to logout?", preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            
            self.navigateToLogin()
            
        }
        let cancelAction = UIAlertAction(title: "No", style: UIAlertAction.Style.cancel) { (result : UIAlertAction) -> Void in
            print("Cancel")
        }
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    @IBAction func btnProdutTapped(_ sender: UIButton) {
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "ProductListVc") as! ProductListVc
        obj.selectedController = .productList
        self.navigationController?.pushViewController(obj, animated: true)
        
    }
    
    
    
    func navigateToLogin(){
    GlobalVariable.Defaults.removeObject(forKey: "userDetails")
        
        let vc  = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        let rearNavigation = UINavigationController(rootViewController: vc)
        AppDelegate.shared.window?.rootViewController = rearNavigation
    }
}

extension SelectOptionsVc{
   
    func getData(){
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(GlobalVariable.baseURL)getHomeScreenData?UserId=\(getUserDetail("user_id"))&Username=\(getUserDetail("username"))&Password=\(getUserDetail("password"))&Company=\(getUserDetail("company_id"))"
            
            print("URL: \(url)")
            
            self.showLoader()
            
            CommonService().ServiceGETMethod(url: url, param: [:]) { (respones) in

                self.hideLoader()
                
                if let dict = respones.value{
                    print("JSON : \(dict)")
                    
//                    {"customerVisitThisWeek":1,"ongoingOpportunities":41,"repeateSellingActual":164000,"repeateSellingTarget":413,"crossSellingActual":0,"crossSellingTarget":423,"newCustomerActual":0,"newCustomerTarget":433}
                    
                    var strRepeatSelling = ""
                    
                    if dict["repeateSellingTarget"].intValue == 0{
                        strRepeatSelling = "\(dict["repeateSellingActual"].intValue.formattedWithSeparator)/\(dict["repeateSellingTarget"].intValue.formattedWithSeparator) (0%)"
                    }
                    else{
                        let perRepeatSelling = String(format: "%.0f", (dict["repeateSellingActual"].floatValue/dict["repeateSellingTarget"].floatValue)*100)
                        
                        print("PreRepeating : \(perRepeatSelling)")
                        
                        strRepeatSelling = "\(dict["repeateSellingActual"].intValue)/\(dict["repeateSellingTarget"].intValue) \((perRepeatSelling))%" == "" ? "-" : "\(dict["repeateSellingActual"].intValue.formattedWithSeparator)/\(dict["repeateSellingTarget"].intValue.formattedWithSeparator) (\(perRepeatSelling)%)"
                    }
                    
                    self.lblRepeatSelling.text = "REPEAT SELLING |    \(strRepeatSelling)"

                    var strCrossSelling = ""
                    
                    if dict["crossSellingTarget"].intValue == 0{
                        strCrossSelling = "\(dict["crossSellingActual"].intValue.formattedWithSeparator)/\(dict["crossSellingTarget"].intValue.formattedWithSeparator) (0%)"
                    }
                    else{
                        let perRepeatSelling = String(format: "%.0f", (dict["crossSellingActual"].floatValue/dict["crossSellingTarget"].floatValue)*100)
                        
                        print("PreRepeating : \(perRepeatSelling)")
                        
                        strCrossSelling = "\(dict["crossSellingActual"].intValue)/\(dict["crossSellingTarget"].intValue) \((perRepeatSelling))%" == "" ? "-" : "\(dict["crossSellingActual"].intValue.formattedWithSeparator)/\(dict["crossSellingTarget"].intValue.formattedWithSeparator) (\((perRepeatSelling))%)"
                    }
                    
                    self.lblCrossSelling.text = "CROSS SELLING |     \(strCrossSelling)"
                    
                    var strNewCustomer = ""
                    
                    if dict["newCustomerTarget"].intValue == 0{
                        strNewCustomer = "\(dict["newCustomerActual"].intValue.formattedWithSeparator)/\(dict["newCustomerTarget"].intValue.formattedWithSeparator) (0%)"
                    }
                    else{
                        let perRepeatSelling = String(format: "%.0f", (dict["newCustomerActual"].floatValue/dict["newCustomerTarget"].floatValue)*100)
                        
                        print("PreRepeating : \(perRepeatSelling)")
                        
                        strNewCustomer = "\(dict["newCustomerActual"].intValue)/\(dict["newCustomerTarget"].intValue) \((perRepeatSelling))%" == "" ? "-" : "\(dict["newCustomerActual"].intValue.formattedWithSeparator)/\(dict["newCustomerTarget"].intValue.formattedWithSeparator) (\((perRepeatSelling))%)"
                    }

                    self.lblNewCustomer.text = "NEW CUSTOMER |     \(strNewCustomer)"
                    
                    self.lblLoggedVisit.text = "Logged-Visit | \t\t \(dict["customerVisitThisWeek"].intValue.formattedWithSeparator)"
                    
                    self.lblOngoingProject.text = "Ongoing Project | \t \(dict["ongoingOpportunities"].intValue.formattedWithSeparator)"
                    
                }
                else
                {
                    makeToast(strMessage: GlobalVariable.serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: GlobalVariable.interNotWorking)
        }
        
    }

    
}
