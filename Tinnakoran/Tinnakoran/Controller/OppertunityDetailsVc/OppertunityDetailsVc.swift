//
//  OppertunityDetailsVc.swift
//  Tinnakoran
//
//  Created by YASH on 10/13/19.
//  Copyright © 2019 Yash. All rights reserved.
//

import UIKit
import SwiftyJSON

class OppertunityDetailsVc: UIViewController {

    @IBOutlet weak var btnAddLoggedCall: CustomButton!
    @IBOutlet weak var consantAddloggedCallHeight: NSLayoutConstraint!
    @IBOutlet weak var lbHeaderName: UILabel!
    @IBOutlet weak var lblCustomerName: UILabel!
    @IBOutlet weak var lblPosition: UILabel!
    @IBOutlet weak var lblPartnerName: UILabel!
    @IBOutlet weak var lblContactName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var lblMobile: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblFax: UILabel!
    @IBOutlet weak var lblPriority: UILabel!
    @IBOutlet weak var lblRevenue: UILabel!
    @IBOutlet weak var lblProbability: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    
    @IBOutlet weak var txtCustomerName: UITextView!
    @IBOutlet weak var lblPositionValue: UILabel!
    @IBOutlet weak var txtPartnerName: UITextView!
    @IBOutlet weak var txtContactName: UITextView!
    @IBOutlet weak var lblEmailValue: UILabel!
    @IBOutlet weak var lblPhoneValue: UILabel!
    @IBOutlet weak var lblMobileValue: UILabel!
    @IBOutlet weak var txtAddressValue: UITextView!
    @IBOutlet weak var lblFaxValue: UILabel!
    @IBOutlet weak var lblPriorityValue: UILabel!
    @IBOutlet weak var lblRevenueValue: UILabel!
    @IBOutlet weak var lblProbabilitylValue: UILabel!
    @IBOutlet weak var txtDescription: UITextView!
    @IBOutlet weak var lblStage: UILabel!
    @IBOutlet weak var lblStageValue: UILabel!
    
    var dictData = JSON()
    var dictCustomerData = JSON()
    var selectedController = fromHomeOrCustomer.customer
    var dictContact = JSON()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        print("DictCustimerData:\(dictData)")
        
        setupUI()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupNavigationbar(titleText: "Details")
        
        if selectedController == .customer{
            let rightButton = UIBarButtonItem(image: UIImage(named:"edit-button") , style: .plain, target: self, action: #selector(btnEditOpportunityTapped))
            rightButton.tintColor = .white
            self.navigationItem.rightBarButtonItem = rightButton
        }else{
            self.btnAddLoggedCall.isHidden = true
            self.consantAddloggedCallHeight.constant = 0
        }
        
    }
    
    @objc func btnEditOpportunityTapped(){
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "AddEditOpportunityVc") as! AddEditOpportunityVc
        obj.selectedController = .edit
        obj.dictCustomerData = self.dictCustomerData
        obj.dictData = self.dictData
        obj.dictContact = dictContact
        obj.handlerForEditOpportunity = { [weak self] value in
            self?.setData(dict: value)
        }
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    func setupUI(){
        
        lbHeaderName.font = themeFont(size: 17, fontname: .medium)
        lbHeaderName.textColor = UIColor.appThemeGreenColor
        [lblCustomerName,lblPosition,lblPartnerName,lblContactName,lblEmail,lblPhone,lblMobile,lblAddress,lblFax,lblPriority,lblRevenue,lblProbability,lblDescription,lblStage].forEach { (lbl) in
            lbl?.font = themeFont(size: 16, fontname: .regular)
            lbl?.textColor = UIColor.appThemeGreenColor
        }
        [lblPositionValue,lblEmailValue,lblPhoneValue,lblMobileValue,lblFaxValue,lblPriorityValue,lblRevenueValue,lblProbabilitylValue,lblStageValue].forEach { (lbl) in
            lbl?.font = themeFont(size: 16, fontname: .regular)
            lbl?.textColor = UIColor.appThemeLightGrayColor
        }
        
        [txtCustomerName,txtPartnerName,txtContactName,txtAddressValue,txtDescription].forEach { (txt) in
            txt?.font = themeFont(size: 16, fontname: .regular)
            txt?.textColor = UIColor.appThemeLightGrayColor
        }
        
        btnAddLoggedCall.backgroundColor = UIColor.appThemeLightGrayColor
        
        setData(dict: dictData)
    }

    func setData(dict:JSON)
    {
        lbHeaderName.text = dict["name"].stringValue
        
        txtCustomerName.text = dict["partner_id"][1].stringValue == "" ? "-" : dict["partner_id"][1].stringValue
        
        txtPartnerName.text = dict["partner_name"].stringValue == "" ? "-" : dict["partner_name"].stringValue
        
        txtContactName.text = dict["contact_name"].stringValue == "" ? "-" : dict["contact_name"].stringValue
        
        lblPositionValue.text = dict["function"].stringValue == "" ? "-" : dict["function"].stringValue
        
        lblEmailValue.text = dict["user_email"].stringValue == "" ? "-" : dict["email_print"].stringValue
        
        lblPhoneValue.text = dict["phone"].stringValue == "" ? "-" : dict["phone"].stringValue
        
        lblMobileValue.text = dict["mobile"].stringValue == "" ? "-" : dict["mobile"].stringValue
        
        txtAddressValue.text = "\(dict["street"].stringValue) \(dict["street2"].stringValue), \(dict["city"].stringValue), \(dict["zip"].stringValue)" == "" ? "-" : "\(dict["street"].stringValue) \(dict["street2"].stringValue), \(dict["city"].stringValue), \(dict["zip"].stringValue)"
        
        lblFaxValue.text = dict["fax"].stringValue == "" ? "-" : dict["fax"].stringValue
        
        lblPriorityValue.text = dict["priority"].stringValue == "" ? "-" : dict["priority"].stringValue
        
        lblRevenueValue.text = dict["planned_revenue"].stringValue == "" ? "-" : dict["planned_revenue"].stringValue
        
        lblProbabilitylValue.text = dict["probability"].stringValue == "" ? "-" : dict["probability"].stringValue
        
        txtDescription.text = dict["description"].stringValue == "" ? "-" : dict["description"].stringValue
        
        lblStageValue.text = dict["stage_id"][1].stringValue == "" ? "-" : dict["stage_id"][1].stringValue
        
        lblStageValue.text = " \(lblStageValue.text ?? "app") "
        
        lblStageValue.textColor = .white
        lblStageValue.backgroundColor = UIColor.hexStringToUIColor(hex: dict["color"].stringValue)
        lblStageValue.layer.cornerRadius = 5
        lblStageValue.clipsToBounds = true
        
    }
   
    @IBAction func btnAddLoggedCalledTapped(_ sender: UIButton) {
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "AddEditLoggedCallVc") as! AddEditLoggedCallVc
        obj.selectedController = .add
        obj.isComeFromOpportunity = true
        obj.customerData = self.dictCustomerData
        obj.dictOpportunityDetail = self.dictData
        obj.handlerForAddLoggedCalls = { [weak self] in
            
        }
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    
}
