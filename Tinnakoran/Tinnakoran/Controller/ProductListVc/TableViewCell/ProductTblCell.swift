//
//  ProductTblCell.swift
//  Tinnakoran
//
//  Created by YASH on 02/10/19.
//  Copyright © 2019 Yash. All rights reserved.
//

import UIKit

class ProductTblCell: UITableViewCell {

    //MARK:- Outlet
    
    @IBOutlet weak var constantLeadingLabel: NSLayoutConstraint!
    @IBOutlet weak var vwBack: UIView!
    @IBOutlet weak var lblMainHeader: UILabel!
    @IBOutlet weak var lblSubdetails: UILabel!
    @IBOutlet weak var imgBack: UIImageView!
    @IBOutlet weak var imgCheck: UIImageView!
    
    //MARK: - View lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        lblMainHeader.font = themeFont(size: 15, fontname: .medium)
        lblSubdetails.font = themeFont(size: 13, fontname: .light)
        
        lblMainHeader.textColor = UIColor.appThemeGreenColor
        lblSubdetails.textColor = UIColor.appThemeLightGrayColor
        
        let image = imgBack.image?.withRenderingMode(.alwaysTemplate)
        imgBack.image = image
        imgBack.tintColor = UIColor.black
        
        imgCheck.isHidden = true
        self.constantLeadingLabel.constant = 12
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
