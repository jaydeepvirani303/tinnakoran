//
//  ProductListVc.swift
//  Tinnakoran
//
//  Created by YASH on 02/10/19.
//  Copyright © 2019 Yash. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON


enum isFromProductOrSalePriceRequest{
    case productList
    case salePriceRequest
}

class ProductListVc: UIViewController {

    //MARK: - Outlet
    @IBOutlet weak var txtSearch: CustomTextField!
    @IBOutlet weak var tblProduct: UITableView!
    
    //MARK: - Variable
    
    var nextOffset = 0
    var arrayProduct : [JSON] = []
    
    var selectedController =  isFromProductOrSalePriceRequest.productList
    
    var handlerProductName : (String,Int) -> Void = {_,_ in}
    
    //MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        self.setupNavigationbar(titleText: "Products")
        
        if selectedController == .salePriceRequest{
            let rightButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(btnDoneTapped))
            rightButton.tintColor = .white
            self.navigationItem.rightBarButtonItem = rightButton
        }
        
    }

    @objc func btnDoneTapped(){
        
        var selectedJSON = JSON()
        
        if self.arrayProduct.contains(where: { (json) -> Bool in
            if json["selected"].stringValue == "1"
            {
                selectedJSON = json
                return true
            }
            return false
        }){
            handlerProductName(selectedJSON["name"].stringValue,selectedJSON["id"].intValue)
            
        }else{
            
        }
        
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK: - Setup UI

extension ProductListVc{
   
    func setupUI(){
        
        tblProduct.register(UINib(nibName: "ProductTblCell", bundle: nil), forCellReuseIdentifier: "ProductTblCell")
        
        txtSearch.placeHolderColor = UIColor.darkGray
        
       // printFonts()
        
        self.getProductList(isRefreshing: true)
    }

}

//MARK: - TableView Delegate and DataSource

extension ProductListVc: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.arrayProduct.count == 0{
            let lbl = UILabel()
            lbl.text = "No record found."
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = UIColor.black
            lbl.center = tableView.center
            tableView.backgroundView = lbl
            return 0
        }else{
            tableView.backgroundView = nil
            return self.arrayProduct.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProductTblCell") as! ProductTblCell
        
        let dict = self.arrayProduct[indexPath.row]
        
        cell.lblMainHeader.text = dict["name"].stringValue
        cell.lblSubdetails.text = "Onhand \(dict["qty_available"].stringValue) | ATP \(dict["virtual_available"].intValue - dict["incoming_qty"].intValue) | Incoming \(dict["incoming_qty"].intValue)"
        
        if selectedController == .productList{
            
        }else{
            cell.imgCheck.isHidden = false
            cell.constantLeadingLabel.constant = 40
            
            cell.imgCheck.tag = indexPath.row
            
            if dict["selected"].stringValue == "0"{
                cell.imgCheck.image = UIImage(named: "ic_uncheck")
            }else{
                cell.imgCheck.image = UIImage(named: "ic_check")
            }
            
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if selectedController == .productList{
            let dict = self.arrayProduct[indexPath.row]
            
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "ProductDetailsVC") as! ProductDetailsVC
            obj.dictData = dict
            self.navigationController?.pushViewController(obj, animated: true)

        }else{
            
            for i in 0..<arrayProduct.count{
                self.arrayProduct[i]["selected"].stringValue = "0"
            }
            
            self.arrayProduct[indexPath.row]["selected"].stringValue = "1"
            
            self.tblProduct.reloadData()
        }
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let lastSectionIndex = tableView.numberOfSections - 1
        
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && arrayProduct != [] && arrayProduct.count >= 10 {
            
            let spinner = UIActivityIndicatorView(style: .gray)
            spinner.color = UIColor.appThemeGreenColor
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
            tableView.tableFooterView = spinner
            tableView.tableFooterView?.isHidden = false
            
            self.nextOffset += 10
            
            if (txtSearch.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
            {
                self.getProductList()
            }else{
                self.searchProduct()
            }
            
        } else {
            tableView.tableFooterView?.isHidden = true
            tableView.tableFooterView = nil
        }
        
    }
    
}

//MARK; - TextField Delegate

extension ProductListVc:UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        self.nextOffset = 0
        searchProduct(isRefreshing: true)
        return true
        
    }
    
}

//MARK: - API calling

extension ProductListVc{
    
    func getProductList(isRefreshing:Bool = false){
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            print("nextOffset:\(nextOffset)")
            
            let url = "\(GlobalVariable.baseURL)getProducts?offset=\(nextOffset)&pazeSize=10&UserId=\(getUserDetail("user_id"))&Username=\(getUserDetail("username"))&Password=\(getUserDetail("password"))&Company=\(getUserDetail("company_id"))"
            
            print("URL: \(url)")
            
            if isRefreshing{
                self.showLoader()
            }
            
            CommonService().ServiceGETMethod(url: url, param: [:]) { (respones) in
                if isRefreshing{
                    self.hideLoader()
                }
                
                if let json = respones.value{
                    print("JSON : \(json)")
                    
                        if self.nextOffset == 0{
                            self.arrayProduct = []
                        }
                        
                      let aryData = json.arrayValue
                        self.arrayProduct = self.arrayProduct + aryData
                    
                    if self.selectedController == .salePriceRequest{
                        
                        for i in 0..<self.arrayProduct.count{
                            var dict = self.arrayProduct[i]
                            dict["selected"].stringValue = "0"
                            self.arrayProduct[i] = dict
                        }
                        
                    }
                    
                    
                    self.tblProduct.reloadData()
                        
                }
                else
                {
                    makeToast(strMessage: GlobalVariable.serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: GlobalVariable.interNotWorking)
        }
        
    }
    
    
    func searchProduct(isRefreshing:Bool = false){
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            print("nextOffset:\(nextOffset)")
            
            var strSearch = (txtSearch.text?.trimmingCharacters(in: .whitespaces)) ?? ""
            
            strSearch = strSearch.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
            
            let url = "\(GlobalVariable.baseURL)getProducts?offset=\(nextOffset)&pazeSize=10&UserId=\(getUserDetail("user_id"))&Username=\(getUserDetail("username"))&Password=\(getUserDetail("password"))&Company=\(getUserDetail("company_id"))&productName=\(strSearch.lowercased())"
            
            print("URL: \(url)")
            
            if isRefreshing{
                self.showLoader()
            }
            
            CommonService().ServiceGETMethod(url: url, param: [:]) { (respones) in
                if isRefreshing{
                    self.hideLoader()
                }
                
                if let json = respones.value{
                    print("JSON : \(json)")
                    
                    if self.nextOffset == 0{
                        self.arrayProduct = []
                    }
                    
                    let aryData = json.arrayValue
                    self.arrayProduct = self.arrayProduct + aryData
                    self.tblProduct.reloadData()
                    
                }
                else
                {
                    makeToast(strMessage: GlobalVariable.serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: GlobalVariable.interNotWorking)
        }
        
    }
    
}
