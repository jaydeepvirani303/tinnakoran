//
//  ContactDetail.swift
//  Tinnakoran
//
//  Created by Jaydeep on 31/03/20.
//  Copyright © 2020 Yash. All rights reserved.
//

import UIKit
import SwiftyJSON

class ContactDetail: UIViewController {
    
    //MARK:- Outlet zone
    
    @IBOutlet weak var lblDetail: UILabel!
    @IBOutlet weak var btnPending: CustomButton!
    @IBOutlet weak var btnSOAll: CustomButton!
    @IBOutlet weak var btnLoggedCall: CustomButton!
    @IBOutlet weak var btOpertunity: CustomButton!
    @IBOutlet weak var btnPriceRequest: CustomButton!
    
    //MARK:- Variable Declaration
    
    var dictContact = JSON()
    var dictCustomerData = JSON()
    
     //MARK:- View Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        [btnPending,btnSOAll,btnLoggedCall,btOpertunity,btnPriceRequest].forEach { (btn) in
            btn?.backgroundColor = UIColor.appThemeLightGrayColor
            btn?.titleLabel?.font = themeFont(size: 15, fontname: .bold)
        }

       let strName = dictContact["name"].stringValue == "" ? "-" : dictContact["name"].stringValue
        let strPhoneNUmber = dictContact["phone"].stringValue == "" ? "-" : dictContact["phone"].stringValue
        let strEmail = dictContact["email_print"].stringValue == "" ? "-" : dictContact["email_print"].stringValue
        let strPosition = dictContact["function"].stringValue == "" ? "-" : dictContact["function"].stringValue
        let strStreet = dictContact["street"].stringValue == "" ? "-" : dictContact["street"].stringValue
        let strCity = dictContact["city"].stringValue == "" ? "-" : dictContact["city"].stringValue
        let strComment = dictContact["comment"].stringValue == "" ? "-" : dictContact["comment"].stringValue
        
       
        let name = setColorAndFontString(str1: "Name : ", str1Color: .black, str1Font: themeFont(size: 17, fontname: .regular), str2: strName+"\n", str2Color: .black, str2Font: themeFont(size: 17, fontname: .regular))
        let phonenumber = setColorAndFontString(str1: "Phone : ", str1Color: .black, str1Font: themeFont(size: 17, fontname: .regular), str2: strPhoneNUmber+"\n", str2Color: .black, str2Font: themeFont(size: 17, fontname: .regular))
        let email = setColorAndFontString(str1: "Email : ", str1Color: .black, str1Font: themeFont(size: 17, fontname: .regular), str2: strEmail+"\n", str2Color: .black, str2Font: themeFont(size: 17, fontname: .regular))
        let position = setColorAndFontString(str1: "Position : ", str1Color: .black, str1Font: themeFont(size: 17, fontname: .regular), str2: strPosition+"\n", str2Color: .black, str2Font: themeFont(size: 17, fontname: .regular))
        let street = setColorAndFontString(str1: "Street : ", str1Color: .black, str1Font: themeFont(size: 17, fontname: .regular), str2: strStreet+"\n", str2Color: .black, str2Font: themeFont(size: 17, fontname: .regular))
        let city = setColorAndFontString(str1: "City : ", str1Color: .black, str1Font: themeFont(size: 17, fontname: .regular), str2: strCity+"\n", str2Color: .black, str2Font: themeFont(size: 17, fontname: .regular))
        let comment = setColorAndFontString(str1: "Comment : ", str1Color: .black, str1Font: themeFont(size: 17, fontname: .regular), str2: strComment+"\n", str2Color: .black, str2Font: themeFont(size: 17, fontname: .regular))
        
        let attributedString = NSMutableAttributedString()
        attributedString.append(name)
        attributedString.append(phonenumber)
        attributedString.append(email)
        attributedString.append(position)
        attributedString.append(street)
        attributedString.append(city)
        attributedString.append(comment)
        
        lblDetail.attributedText = attributedString
    }
    

    override func viewWillAppear(_ animated: Bool) {
        setupNavigationbar(titleText: "Contact Detail")
    }
    
    //MARK: - IBAction
    @IBAction func btnSoPendingTapped(_ sender: UIButton) {
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "SaleOrderListVc") as! SaleOrderListVc
        obj.selectedCotroller = .SOPending
        obj.dictCusotmerData = dictCustomerData
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnSOAllTapped(_ sender: UIButton) {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "SaleOrderListVc") as! SaleOrderListVc
        obj.selectedCotroller = .SOAll
        obj.dictCusotmerData = dictCustomerData
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnLoggedCallsTapped(_ sender: UIButton) {
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "LoggedCallListVc") as! LoggedCallListVc
        obj.dictCusotmerData = dictCustomerData
        obj.selectedController = .customer
        obj.dictContact = dictContact
        self.navigationController?.pushViewController(obj, animated: true)
        
    }
    
    @IBAction func btnOpertunityTapped(_ sender: UIButton) {
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "OpportunityListVc") as! OpportunityListVc
        obj.dictCusotmerData = dictCustomerData
        obj.selectedController = .customer
        obj.dictContact = dictContact
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnSalesPriceRequestTapped(_ sender: UIButton) {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "SalePriceRequestListVc") as! SalePriceRequestListVc
        obj.dictCusotmerData = dictCustomerData
        obj.selectedController = .customer
        self.navigationController?.pushViewController(obj, animated: true)
    }
}
