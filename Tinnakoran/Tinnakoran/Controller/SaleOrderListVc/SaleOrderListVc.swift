//
//  SaleOrderListVc.swift
//  Tinnakoran
//
//  Created by YASH on 10/13/19.
//  Copyright © 2019 Yash. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON


class SaleOrderListVc: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var tblSOlList: UITableView!
    
    var dictCusotmerData = JSON()
    var selectedCotroller = CheckSOParentViewController.SOPending
    
    var arraySOList : [JSON] = []

    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        tblSOlList.register(UINib(nibName: "ContactTblCell", bundle: nil), forCellReuseIdentifier: "ContactTblCell")
        
        tblSOlList.register(UINib(nibName: "PendingSaleOrderTblCell", bundle: nil), forCellReuseIdentifier: "PendingSaleOrderTblCell")

        tblSOlList.tableFooterView = UIView()
        
         getSaleOrdersList()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        
        if selectedCotroller == .SOPending{
            self.setupNavigationbar(titleText: "Pending Sale Order")
        }else if selectedCotroller == .SOAll{
            self.setupNavigationbar(titleText: "All Sale Order")
        }
        
    }
    
    @objc func btnUrgentTapped(sender:UIButton){
        
        print("sender:\(sender.tag)")
        var dict = self.arraySOList[sender.tag]
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "SaleOrderUrgentVc") as! SaleOrderUrgentVc
        obj.dictData = dict
        obj.handlerRefreshPendingList = { [weak self] in
            self?.getSaleOrdersList()
        }
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @objc func btnChangeDateTapped(sender:UIButton){
        print("sender:\(sender.tag)")
        var dict = self.arraySOList[sender.tag]
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "SaleOrderUrgentVc") as! SaleOrderUrgentVc
        obj.dictData = dict
        obj.isChangeDateButtonClick = true
        obj.handlerRefreshPendingList = { [weak self] in
            self?.getSaleOrdersList()
        }
        self.navigationController?.pushViewController(obj, animated: true)
    }
}

//MARK: - TableView Delegate
extension SaleOrderListVc: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.arraySOList.count == 0{
            let lbl = UILabel()
            lbl.text = "No record found."
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = UIColor.black
            lbl.center = tableView.center
            tableView.backgroundView = lbl
            return 0
        }else{
            tableView.backgroundView = nil
            return self.arraySOList.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let dict = self.arraySOList[indexPath.row]
        
        if selectedCotroller == .SOPending{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "PendingSaleOrderTblCell") as! PendingSaleOrderTblCell
            
            cell.btnUrgent.isHidden = false
            cell.btnUrgent.tag = indexPath.row
            cell.btnUrgent.addTarget(self, action: #selector(btnUrgentTapped), for: .touchUpInside)
            
            cell.btnChangeDate.tag = indexPath.row
            cell.btnChangeDate.addTarget(self, action: #selector(btnChangeDateTapped), for: .touchUpInside)
            
            let strName = dict["name"].stringValue == "" ? "-" : dict["name"].stringValue
            let strCustomerName = dict["partner_id"][1].stringValue == "" ? "-" : dict["partner_id"][1].stringValue
            let strOrderDate = dict["date_order"].stringValue == "" ? "-" : dict["date_order"].stringValue
            let strRequestedDate = dict["requested_date"].stringValue == "" ? "-" : dict["requested_date"].stringValue
            
            let strState = dict["state"].stringValue == "" ? "-" : dict["state"].stringValue
            let strOrderRef = dict["client_order_ref"].stringValue == "" ? "-" : dict["client_order_ref"].stringValue
            
            let name = setColorAndFontString(str1:"Name: " ,str2: strName)
            let customerName = setColorAndFontString(str1:"Customer Name: " ,str2: strCustomerName+"\n")
            let orderDate = setColorAndFontString(str1:"Order Date: " ,str2: strOrderDate+"\n")
            let requestedDate = setColorAndFontString(str1:"Requested Date: " ,str2: strRequestedDate+"\n")
            let state = setColorAndFontString(str1:"State: " ,str2: strState+"\n")
            let orderRef = setColorAndFontString(str1:"Order Ref.: " ,str2: strOrderRef)
            
            let attributedName = NSMutableAttributedString()
            attributedName.append(name)
            
            let attributedString = NSMutableAttributedString()
            attributedString.append(customerName)
            attributedString.append(orderDate)
            attributedString.append(requestedDate)
            attributedString.append(state)
            attributedString.append(orderRef)
            
            cell.lblName.attributedText = attributedName
            cell.lblSubDetails.attributedText = attributedString
            
            return cell
            
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactTblCell") as! ContactTblCell
        
        cell.btnUrgent.isHidden = true
        cell.constantWidthbtnUrgent.constant = 45.0
        
        let strName = dict["name"].stringValue == "" ? "-" : dict["name"].stringValue
        let strCustomerName = dict["partner_id"][1].stringValue == "" ? "-" : dict["partner_id"][1].stringValue
        let strOrderDate = dict["date_order"].stringValue == "" ? "-" : dict["date_order"].stringValue
        let strRequestedDate = dict["requested_date"].stringValue == "" ? "-" : dict["requested_date"].stringValue
        
        let strState = dict["state"].stringValue == "" ? "-" : dict["state"].stringValue
        let strOrderRef = dict["client_order_ref"].stringValue == "" ? "-" : dict["client_order_ref"].stringValue
        
        let name = setColorAndFontString(str1:"Name: " ,str2: strName+"\n")
        let customerName = setColorAndFontString(str1:"Customer Name: " ,str2: strCustomerName+"\n")
        let orderDate = setColorAndFontString(str1:"Order Date: " ,str2: strOrderDate+"\n")
        let requestedDate = setColorAndFontString(str1:"Requested Date: " ,str2: strRequestedDate+"\n")
        let state = setColorAndFontString(str1:"State: " ,str2: strState+"\n")
        let orderRef = setColorAndFontString(str1:"Order Ref.: " ,str2: strOrderRef)
        
        let attributedString = NSMutableAttributedString()
        attributedString.append(name)
        attributedString.append(customerName)
        attributedString.append(orderDate)
        attributedString.append(requestedDate)
        attributedString.append(state)
        attributedString.append(orderRef)
        
        cell.lblMain.attributedText = attributedString
        
        return cell
        
    }

    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let dict = self.arraySOList[indexPath.row]
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "SaleOrderDetailsVc") as! SaleOrderDetailsVc
        obj.dictCusotmerData = dict
        
        if selectedCotroller == .SOAll{
            obj.selectedCotroller = .SOAll
        }else if selectedCotroller == .SOPending{
            obj.selectedCotroller = .SOPending
        }
        
        self.navigationController?.pushViewController(obj, animated: true)
        
    }
    
    /*
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if self.arraySOList.count != 0{
            return "Sales Orders"
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if self.arraySOList.count != 0{
            return 50
        }
        return 0
    }*/
}



//MARK: - API calling

extension SaleOrderListVc{
    
    func getSaleOrdersList(){
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            var url = ""
            
            url = "\(GlobalVariable.baseURL)getSalesOrders?UserId=\(getUserDetail("user_id"))&Username=\(getUserDetail("username"))&Password=\(getUserDetail("password"))&Company=\(getUserDetail("company_id"))&CustomerId=\(dictCusotmerData["id"].stringValue)"
            
            if selectedCotroller == .SOPending{
                url += "&flgIsPending=true"
            }else if selectedCotroller == .SOAll{
                
            }
            
            print("URL: \(url)")
            
            self.showLoader()
            
            CommonService().ServiceGETMethod(url: url, param: [:]) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value{
                    print("JSON : \(json)")
                    
                    if json["isError"].boolValue == true{
                        makeToast(strMessage: "Something went wrong")
                        return
                    }
                    
                    
              //      self.setData(dict: json[0])
                    
                    self.arraySOList = []
                    self.arraySOList = json.arrayValue
                    
                    print("self.arraSoList:\(self.arraySOList)")
                    
                    self.tblSOlList.reloadData()
                    
                }
                else
                {
                    makeToast(strMessage: GlobalVariable.serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: GlobalVariable.interNotWorking)
        }
    }
    
}

