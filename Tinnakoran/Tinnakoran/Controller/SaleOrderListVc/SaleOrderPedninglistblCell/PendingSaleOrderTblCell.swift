//
//  PendingSaleOrderTblCell.swift
//  Tinnakoran
//
//  Created by YASH on 11/2/19.
//  Copyright © 2019 Yash. All rights reserved.
//

import UIKit

class PendingSaleOrderTblCell: UITableViewCell {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var btnChangeDate: CustomButton!
    @IBOutlet weak var btnUrgent: CustomButton!
    @IBOutlet weak var lblSubDetails: UILabel!
    @IBOutlet weak var imgArrow: UIImageView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        
        lblName.font = themeFont(size: 15, fontname: .medium)
        btnChangeDate.backgroundColor = UIColor.appThemeLightGrayColor
        
        let image = imgArrow.image?.withRenderingMode(.alwaysTemplate)
        imgArrow.image = image
        imgArrow.tintColor = UIColor.black
        
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
