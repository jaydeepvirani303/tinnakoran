//
//  LoggedCallDetailsVc.swift
//  Tinnakoran
//
//  Created by YASH on 10/13/19.
//  Copyright © 2019 Yash. All rights reserved.
//

import UIKit
import SwiftyJSON

class LoggedCallDetailsVc: UIViewController {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblCustomerName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var txtCustomerName: UITextView!
    @IBOutlet weak var txtDate: UITextView!
    @IBOutlet weak var txtDescription: UITextView!
    @IBOutlet weak var txtCategoryName: UITextView!
    
    var dictData = JSON()
    var dictCustomerData = JSON()
    var dictContact = JSON()
    var selectedController = fromHomeOrCustomer.customer
    var handlerAPICalled:() -> Void = {}
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupNavigationbar(titleText: "Details")
        
        if selectedController == .customer{
            let rightButton = UIBarButtonItem(image: UIImage(named:"edit-button") , style: .plain, target: self, action: #selector(btnEditLoggedCallsTapped))
            rightButton.tintColor = .white
            self.navigationItem.rightBarButtonItem = rightButton
        } else {
            let rightButton = UIBarButtonItem(image: UIImage(named:"edit-button") , style: .plain, target: self, action: #selector(btnEditLoggedCallsTapped))
            rightButton.tintColor = .white
            self.navigationItem.rightBarButtonItem = rightButton
        }
    }
    
    @objc func btnEditLoggedCallsTapped(){
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "AddEditLoggedCallVc") as! AddEditLoggedCallVc
        obj.selectedParentController = selectedController
        obj.selectedController = .edit
        obj.dictData = dictData
        obj.dictContact = dictContact
        obj.customerData = self.dictCustomerData
        obj.handlerForEditLoggedCalls = { [weak self] value in           
            self?.handlerAPICalled()
            if self?.selectedController == .home {
                self?.navigationController?.popViewController(animated: false)
            } else {
                 self?.setData(dict: value)
            }
        }
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    func setupUI(){
        
        lblName.font = themeFont(size: 17, fontname: .medium)
        lblName.textColor = UIColor.appThemeGreenColor
        
        [lblCustomerName,lblDescription,lblDate,lblCategory].forEach { (lbl) in
            lbl?.font = themeFont(size: 16, fontname: .regular)
            lbl?.textColor = UIColor.appThemeGreenColor
        }
        
        [txtCustomerName,txtDate,txtDescription,txtCategoryName].forEach { (txt) in
            txt?.font = themeFont(size: 16, fontname: .regular)
            txt?.textColor = UIColor.appThemeLightGrayColor
        }
        
        setData(dict:dictData)
    }

    func setData(dict:JSON){
        
        lblName.text = dict["name"].stringValue
        
        txtCustomerName.text = dict["partner_id"][1].stringValue == "" ? "-" : dict["partner_id"][1].stringValue
        
        txtDescription.text = dict["description"].stringValue == "" ? "-" : dict["description"].stringValue
        
        txtDate.text = dict["date"].stringValue == "" ? "-" : dict["date"].stringValue
        
        txtCategoryName.text = dict["categ_id"][1].stringValue == "" ? "-" : dict["categ_id"][1].stringValue
    }
}
