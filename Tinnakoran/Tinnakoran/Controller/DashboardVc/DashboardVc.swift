//
//  DashboardVc.swift
//  Tinnakoran
//
//  Created by YASH on 11/27/19.
//  Copyright © 2019 Yash. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON
import LinearProgressBar

class DashboardVc: UIViewController {

    @IBOutlet weak var lblLeadingDetails: UILabel!
    @IBOutlet weak var lblThisMonth: UILabel!
    @IBOutlet weak var lblTotalThisMonth: UILabel!
    @IBOutlet weak var lblThisYear: UILabel!
    @IBOutlet weak var lblTotalThisYear: UILabel!
    @IBOutlet weak var lblBindingThisMoth: UILabel!
    @IBOutlet weak var lblBindingThisYear: UILabel!
    
    @IBOutlet weak var lblProgressRepeatSellingThisMonth: UILabel!
    @IBOutlet weak var lblProgressCrossSellingThisMonth: UILabel!
    @IBOutlet weak var lblProgressNewCustomerThisMonth: UILabel!
    @IBOutlet weak var lblProgressBiddingThisMonth: UILabel!
    
    @IBOutlet weak var progressRepeatSellingThisMonth: LinearProgressBar!
    @IBOutlet weak var progressCrossSellingThisMonth: LinearProgressBar!
    @IBOutlet weak var progressNewCustomerThisMonth: LinearProgressBar!
    @IBOutlet weak var progessBidding: LinearProgressBar!
    
    
    @IBOutlet weak var lblProgressRepeatSellingThisYear: UILabel!
    @IBOutlet weak var lblProgressCrossSellingThisYear: UILabel!
    @IBOutlet weak var lblProgressNewCustomerThisYear: UILabel!
    @IBOutlet weak var lblProgressBiddingThisYear: UILabel!
    @IBOutlet weak var progressRepeatSellingThisYear: LinearProgressBar!
    @IBOutlet weak var progressCrossSellingThisYear: LinearProgressBar!
    @IBOutlet weak var progressNewCustomerThisYear: LinearProgressBar!
    @IBOutlet weak var progessBiddingYear: LinearProgressBar!

    
    var dictLeadingData = JSON()
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        [progressRepeatSellingThisMonth,progressCrossSellingThisMonth,progressNewCustomerThisMonth,progessBidding,progressRepeatSellingThisYear,progressCrossSellingThisYear,progressNewCustomerThisYear,progessBiddingYear].forEach { (bar) in
            bar?.trackColor = UIColor.lightGray
            bar?.barColor = UIColor.red
            bar?.barPadding = 0
            bar?.trackPadding = 0
            bar?.barThickness = 5
        }
        
        
        dashboardDetails()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setupNavigationbar(titleText: "Dashboard")
    }
    
}


extension DashboardVc{
    
    func dashboardDetails(){
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(GlobalVariable.baseURL)getDashboardDetails?UserId=\(getUserDetail("user_id"))&Username=\(getUserDetail("username"))&Password=\(getUserDetail("password"))&Company=\(getUserDetail("company_id"))"
            
            print("URL: \(url)")
            
            self.showLoader()
            
            CommonService().ServiceGETMethod(url: url, param: [:]) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value{
                    print("JSON : \(json)")
                    
                    self.setLeadingData(dict: json["leadingIndicator"])
                    self.setThisMonthData(dict: json["laggingIndicator"]["thisMonth"],thisMonth:true)
                    self.setThisMonthData(dict: json["laggingIndicator"]["thisYear"],thisMonth:false)
                    
                }
                else
                {
                    makeToast(strMessage: GlobalVariable.serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: GlobalVariable.interNotWorking)
        }
        
    }

    
    func setLeadingData(dict:JSON){
        
        let strCustomerVisitThisWeek = dict["customerVisitThisWeek"].intValue.formattedWithSeparator == "" ? "-" : dict["customerVisitThisWeek"].intValue.formattedWithSeparator
        let strCustomerAcquired = "\(dict["newCustomerAcquiredThisMonth"].intValue.formattedWithSeparator)/\(dict["newCustomerAcquiredThisYear"].intValue.formattedWithSeparator)" == "" ? "-" : "\(dict["newCustomerAcquiredThisMonth"].intValue.formattedWithSeparator)/\(dict["newCustomerAcquiredThisYear"].intValue.formattedWithSeparator)"
        let strnewOpotunity = "\(dict["newOpportunitiesThisMonth"].intValue.formattedWithSeparator)/\(dict["newOpportunitiesThisYear"].intValue.formattedWithSeparator)" == "" ? "-" : "\(dict["newOpportunitiesThisMonth"].intValue.formattedWithSeparator)/\(dict["newOpportunitiesThisYear"].intValue.formattedWithSeparator)"
        let strOngoningOpportunity = dict["ongoingOpportunities"].intValue.formattedWithSeparator == "" ? "-" : dict["ongoingOpportunities"].intValue.formattedWithSeparator
        
        let visitthisweek = setColorAndFontString(str1:"Customer visit (this week): " ,str2: strCustomerVisitThisWeek+"\n")
        let customerAcquired = setColorAndFontString(str1:"New customer acquired (this month/Year): " ,str2: strCustomerAcquired+"\n")
        let newoportunity = setColorAndFontString(str1:"New Opportunity (this month/Year): " ,str2: strnewOpotunity+"\n")
        let onggoingoportunity = setColorAndFontString(str1:"Ongoing Opportunity : " ,str2: strOngoningOpportunity)
        
        let attributedString = NSMutableAttributedString()
        attributedString.append(visitthisweek)
        attributedString.append(customerAcquired)
        attributedString.append(newoportunity)
        attributedString.append(onggoingoportunity)
        
        self.lblLeadingDetails.attributedText = attributedString
        
    }
    
    func setThisMonthData(dict:JSON,thisMonth:Bool){
        
        var strRepeatSelling = ""
        
        if dict["repeateSellingTarget"].intValue == 0{
             strRepeatSelling = "\(dict["repeateSellingActual"].intValue.formattedWithSeparator)/\(dict["repeateSellingTarget"].intValue.formattedWithSeparator) (100%)"
            
            if thisMonth{
                self.lblProgressRepeatSellingThisMonth.text = "(100%)"
                progressRepeatSellingThisMonth.progressValue = 100.0
            }else{
                self.lblProgressRepeatSellingThisYear.text = "(100%)"
                progressRepeatSellingThisYear.progressValue = 100.0
            }
            
        }
        else{
            let perRepeatSelling = String(format: "%.0f", (dict["repeateSellingActual"].floatValue/dict["repeateSellingTarget"].floatValue)*100)
            
            print("PreRepeating : \(perRepeatSelling)")
            
            if thisMonth{
                self.lblProgressRepeatSellingThisMonth.text = "(\(perRepeatSelling)%)"
                progressRepeatSellingThisMonth.progressValue = perRepeatSelling.CGFloatValue() ?? 0.0
            }else{
                self.lblProgressRepeatSellingThisYear.text = "(\(perRepeatSelling)%)"
                progressRepeatSellingThisYear.progressValue = perRepeatSelling.CGFloatValue() ?? 0.0
            }

            
            strRepeatSelling = "\(dict["repeateSellingActual"].intValue)/\(dict["repeateSellingTarget"].intValue) \((perRepeatSelling))%" == "" ? "-" : "\(dict["repeateSellingActual"].intValue.formattedWithSeparator)/\(dict["repeateSellingTarget"].intValue.formattedWithSeparator) (\(perRepeatSelling)%)"
        }
        
        var strCrossSelling = ""
        
        if dict["crossSellingTarget"].intValue == 0{
            strCrossSelling = "\(dict["crossSellingActual"].intValue.formattedWithSeparator)/\(dict["crossSellingTarget"].intValue.formattedWithSeparator) (100%)"
            
            if thisMonth{
                self.lblProgressCrossSellingThisMonth.text = "(100%)"
                progressCrossSellingThisMonth.progressValue = 100.0
            }else{
                self.lblProgressCrossSellingThisYear.text = "(100%)"
                progressCrossSellingThisYear.progressValue = 100.0
            }
            
        }
        else{
            let perRepeatSelling = String(format: "%.0f", (dict["crossSellingActual"].floatValue/dict["crossSellingTarget"].floatValue)*100)
            
            print("PreRepeating : \(perRepeatSelling)")
            
            if thisMonth{
                self.lblProgressCrossSellingThisMonth.text = "(\(perRepeatSelling)%)"
                progressCrossSellingThisMonth.progressValue = perRepeatSelling.CGFloatValue() ?? 0.0
            }else{
                self.lblProgressCrossSellingThisYear.text = "(\(perRepeatSelling)%)"
                progressCrossSellingThisYear.progressValue = perRepeatSelling.CGFloatValue() ?? 0.0
            }
            
            strCrossSelling = "\(dict["crossSellingActual"].intValue)/\(dict["crossSellingTarget"].intValue) \((perRepeatSelling))%" == "" ? "-" : "\(dict["crossSellingActual"].intValue.formattedWithSeparator)/\(dict["crossSellingTarget"].intValue.formattedWithSeparator) (\((perRepeatSelling))%)"
        }
        
        
        var strNewCustomer = ""
        
        if dict["newCustomerTarget"].intValue == 0{
            strNewCustomer = "\(dict["newCustomerActual"].intValue.formattedWithSeparator)/\(dict["newCustomerTarget"].intValue.formattedWithSeparator) (100%)"
            
            if thisMonth{
                self.lblProgressNewCustomerThisMonth.text = "(100%)"
                progressNewCustomerThisMonth.progressValue = 100.0
            }else{
                self.lblProgressNewCustomerThisYear.text = "(100%)"
                progressNewCustomerThisYear.progressValue = 100.0
            }
            
        }
        else{
            let perRepeatSelling = String(format: "%.0f", (dict["newCustomerActual"].floatValue/dict["newCustomerTarget"].floatValue)*100)
            
            print("PreRepeating : \(perRepeatSelling)")
            
            if thisMonth{
                self.lblProgressNewCustomerThisMonth.text = "(\(perRepeatSelling)%)"
                progressNewCustomerThisMonth.progressValue = perRepeatSelling.CGFloatValue() ?? 0.0
            }else{
                self.lblProgressNewCustomerThisYear.text = "(\(perRepeatSelling)%)"
                progressNewCustomerThisYear.progressValue = perRepeatSelling.CGFloatValue() ?? 0.0
            }
            
            
            strNewCustomer = "\(dict["newCustomerActual"].intValue)/\(dict["newCustomerTarget"].intValue) \((perRepeatSelling))%" == "" ? "-" : "\(dict["newCustomerActual"].intValue.formattedWithSeparator)/\(dict["newCustomerTarget"].intValue.formattedWithSeparator) (\((perRepeatSelling))%)"
        }
        
        var strBidding = ""
        
        if dict["biddingTarget"].intValue == 0{
            strBidding = "\(dict["biddingActual"].intValue.formattedWithSeparator)/\(dict["biddingTarget"].intValue.formattedWithSeparator) (100%)"
            
            if thisMonth{
                self.lblProgressBiddingThisMonth.text = "(100%)"
                progessBidding.progressValue = 100.0
            }else{
                self.lblProgressBiddingThisYear.text = "(100%)"
                progessBiddingYear.progressValue = 100.0
            }
            
        }
        else{
            let perRepeatSelling = String(format: "%.0f", (dict["biddingActual"].floatValue/dict["biddingTarget"].floatValue)*100)
            
            print("PreRepeating : \(perRepeatSelling)")
            
            if thisMonth{
                self.lblProgressBiddingThisMonth.text = "(\(perRepeatSelling)%)"
                progessBidding.progressValue = perRepeatSelling.CGFloatValue() ?? 0.0
            }else{
                self.lblProgressBiddingThisYear.text = "(\(perRepeatSelling)%)"
                progessBiddingYear.progressValue = perRepeatSelling.CGFloatValue() ?? 0.0
            }
            
            
            strBidding = "\(dict["biddingActual"].intValue)/\(dict["biddingTarget"].intValue) \((perRepeatSelling))%" == "" ? "-" : "\(dict["biddingActual"].intValue.formattedWithSeparator)/\(dict["biddingTarget"].intValue.formattedWithSeparator) (\((perRepeatSelling))%)"
        }
        
        let repeatselling = setColorAndFontString(str1:"Repeating Selling (THB): " ,str2: strRepeatSelling+"\n")
        let crossselling = setColorAndFontString(str1:"Cross Selling (THB): " ,str2: strCrossSelling+"\n")
        let newcustomer = setColorAndFontString(str1:"New Customer (THB): " ,str2: strNewCustomer)
        let bidding = setColorAndFontString(str1:"Bidding (THB) : " ,str2: strBidding)
        
        let attributedString = NSMutableAttributedString()
        attributedString.append(repeatselling)
        attributedString.append(crossselling)
        attributedString.append(newcustomer)
    //    attributedString.append(bidding)
        
        let attributedString3 = NSMutableAttributedString()
        attributedString3.append(bidding)
        
        
        if thisMonth{
            self.lblThisMonth.attributedText = attributedString
            self.lblBindingThisMoth.attributedText = attributedString3
        }else{
            self.lblThisYear.attributedText = attributedString
            self.lblBindingThisYear.attributedText = attributedString3
        }
        
        let totalThisMonthActual = dict["repeateSellingActual"].floatValue + dict["crossSellingActual"].floatValue + dict["newCustomerActual"].floatValue
//            + dict["biddingActual"].floatValue
        
        let totalThisMonthTarget = dict["repeateSellingTarget"].floatValue + dict["crossSellingTarget"].floatValue + dict["newCustomerTarget"].floatValue
//            + dict["biddingTarget"].floatValue
        
        var strTotal = ""
        
        if totalThisMonthTarget == 0.0{
            strTotal = "\(Int(totalThisMonthActual).formattedWithSeparator)/\(Int(totalThisMonthTarget).formattedWithSeparator) (100%)"
        }
        else{
            let perRepeatSelling = String(format: "%.0f", (totalThisMonthActual/totalThisMonthTarget)*100)
            
            print("PreRepeating : \(perRepeatSelling)")
            
            strTotal = "\(totalThisMonthActual)/\(totalThisMonthTarget) \((perRepeatSelling))%" == "" ? "-" : "\(Int(totalThisMonthActual).formattedWithSeparator)/\(Int(totalThisMonthTarget).formattedWithSeparator) (\(perRepeatSelling)%)"
        }
        
        let totalThisMonth = setColorAndFontString(str1:"Total Sales (THB): " ,str2: strTotal+"\n")
        
        let attributedString2 = NSMutableAttributedString()
        attributedString2.append(totalThisMonth)
        
        if thisMonth{
            self.lblTotalThisMonth.attributedText = attributedString2
        }else{
            self.lblTotalThisYear.attributedText = attributedString2
        }
        
    }
}


extension Formatter {
    static let withSeparator: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.groupingSeparator = ","
        formatter.numberStyle = .decimal
        return formatter
    }()
}

extension Int{
    var formattedWithSeparator: String {
        return Formatter.withSeparator.string(for: self) ?? ""
    }
}

extension String {
    
    func CGFloatValue() -> CGFloat? {
        guard let doubleValue = Double(self) else {
            return nil
        }
        
        return CGFloat(doubleValue)
    }
}
