//
//  AddEditCustomerVc.swift
//  Tinnakoran
//
//  Created by YASH on 10/13/19.
//  Copyright © 2019 Yash. All rights reserved.
//

import UIKit
import AlamofireSwiftyJSON
import SwiftyJSON
import Alamofire
import DropDown

enum isForAddOrEdit{
    case edit
    case add
}


class AddEditCustomerVc: UIViewController {

    @IBOutlet weak var txtCustomerName: CustomTextField!
    @IBOutlet weak var txtAddress1: CustomTextField!
    @IBOutlet weak var txtAddress2: CustomTextField!
    @IBOutlet weak var txtCity: CustomTextField!
    @IBOutlet weak var txtZip: CustomTextField!
    @IBOutlet weak var txtPosition: CustomTextField!
    @IBOutlet weak var txtPhone: CustomTextField!
    @IBOutlet weak var txtMobile: CustomTextField!
    @IBOutlet weak var txtEmail: CustomTextField!
    @IBOutlet weak var txtFax: CustomTextField!
    @IBOutlet weak var txtCountry: CustomTextField!
    @IBOutlet weak var txtVwDescription: UITextView!
    @IBOutlet weak var btnIsCompmany: UIButton!
    @IBOutlet weak var btnAddEdit: CustomButton!
    @IBOutlet weak var lblPlaceHolderDescription: UILabel!
    
    var selectedController = isForAddOrEdit.add
    
    var countryDD = DropDown()
    var selectedCountryFromList = ""

    var jsonCountryList : [JSON] = []
    var strCountryList = [String]()

    var isCompany = "0"
    
    var handlerForAddCustomer: ()->Void = {}
    var handlerForEditCustomer: (JSON) -> Void = {_ in}
    
    var dictData = JSON()
    var dictCustomerData = JSON()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        countryList()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        if selectedController == .add{
            self.setupNavigationbar(titleText: "Add Customers")
            btnAddEdit.setTitle("Add", for: .normal)
        }else if selectedController == .edit{
            self.setupNavigationbar(titleText: "Edit Customers")
            btnAddEdit.setTitle("Submit change".uppercased(), for: .normal)
            self.setDataForEdit(dict: dictData)
        }
        
    }
    
    override func viewDidLayoutSubviews() {
        
        self.configDD(dropdown: self.countryDD, sender: self.txtCountry)
        selectionIndex()
    }
    func setupUI(){
        
        [txtCustomerName,txtAddress1,txtAddress2,txtCity,txtZip,txtPosition,txtPhone,txtMobile,txtEmail,txtFax,txtCountry].forEach { (txt) in
            txt?.placeHolderColor = UIColor.lightGray
            txt?.delegate = self
            txt?.font = themeFont(size: 13, fontname: .regular)
        }
        
        txtVwDescription.font = themeFont(size: 13, fontname: .regular)
        txtVwDescription.delegate = self
        
        btnAddEdit.backgroundColor = UIColor.appThemeLightGrayColor
    }

    func selectionIndex()
    {
        
        self.countryDD.selectionAction = { (index, item) in
            self.txtCountry.text = item
            self.view.endEditing(true)
            
            self.selectedCountryFromList = self.jsonCountryList[index]["id"].stringValue
            
            //            self.txtSelectCarType.text =
            //            self.txtSelectSeat.text = ""
            self.countryDD.hide()
        }
    }
    
    //MARK: - DropDown setup
    
    func configDD(dropdown: DropDown, sender: UIView)
    {
        dropdown.anchorView = sender
        dropdown.direction = .any
        dropdown.dismissMode = .onTap
        //  dropdown.topOffset = CGPoint(x: 0, y: self.view.bounds.origin.y)
        dropdown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        // print("textfield Frame \(txtPurpose.frame)")
        dropdown.width = sender.bounds.width
        dropdown.cellHeight = 40.0
        dropdown.backgroundColor = UIColor.white
        dropdown.textColor = UIColor.black
        dropdown.selectionBackgroundColor = UIColor.clear
        
    }
    
    @IBAction func btnIsCompanySelected(_ sender: Any) {
        if btnIsCompmany.isSelected{
            self.isCompany = "0"
            btnIsCompmany.isSelected = false
        }else{
            self.isCompany = "1"
            btnIsCompmany.isSelected = true
        }
    }
    
    @IBAction func btnSelectCountryTapped(_ sender: UIButton) {
        countryDD.show()
    }
    
    
    @IBAction func btnAddEditTapped(_ sender: CustomButton) {
        
        if (txtCustomerName.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: "Please enter customer name")
        }
        else
        {
            AddEditCustomerAPICalled()
        }
    }
    
    func setDataForEdit(dict:JSON){
        
        print("dict:\(dict)")
        txtCustomerName.text = dict["name"].stringValue
        txtAddress1.text = dict["street"].stringValue
        txtAddress2.text = dict["street2"].stringValue
        txtCity.text = dict["city"].stringValue
        txtZip.text = dict["zip"].stringValue
        txtPosition.text = dict["function"].stringValue
        txtPhone.text = dict["phone"].stringValue
        txtMobile.text = dict["mobile"].stringValue
        txtEmail.text = dict["email_print"].stringValue
        txtFax.text = dict["fax"].stringValue
        if dict["comment"].stringValue == ""{
            self.lblPlaceHolderDescription.isHidden = false
        }else{
            self.lblPlaceHolderDescription.isHidden = true
            txtVwDescription.text = dict["comment"].stringValue
        }
        
        if dict["is_company"].boolValue == true{
            btnIsCompmany.isSelected = true
        }else{
            btnIsCompmany.isSelected = false
        }
        
        if dict["country_id"].arrayValue.count == 0{
            txtCountry.text = ""
        }
        else
        {
            self.selectedCountryFromList = dict["country_id"][0].stringValue
            txtCountry.text = dict["country_id"][1].stringValue
        }
        
        /*
        if dict["CountryId"].boolValue{
            txtCountry.text = ""
        }
        
        if dict["CountryId"].stringValue != ""{
           
            let country = dict["CountryId"].stringValue
            var index = 0
            if jsonCountryList.contains(where: { (json) -> Bool in
                if json["id"].stringValue == country{
                    index = jsonCountryList.firstIndex(of: json) ?? 0
                    return true
                }
                return false
            })
            {
                txtCountry.text = self.jsonCountryList[index]["name"].stringValue
            }
            else
            {
                txtCountry.text = ""
            }
            
        }*/

    }
}

//MARK: - TextField Delegate

extension AddEditCustomerVc: UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
}

//MARK:- Textview Delegate

extension AddEditCustomerVc:UITextViewDelegate
{
    func textViewDidChange(_ textView: UITextView) {
        if textView.text == ""
        {
            self.lblPlaceHolderDescription.isHidden = false
        }
        else
        {
            self.lblPlaceHolderDescription.isHidden = true
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn shouldChangeTextInRange: NSRange, replacementText text: String) -> Bool
    {
        if text == "\n"  // Recognizes enter key in keyboard
        {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}


//MARK: - API calling

extension AddEditCustomerVc{
    
    func AddEditCustomerAPICalled()
    {
        self.view.endEditing(true)
        
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(GlobalVariable.baseURL)postCustomers"
            
            print("URL: \(url)")
            
            let userID = Int(getUserDetail("user_id"))
            var param = [
                         "UserId": Int(userID!),
                         "UserName":getUserDetail("username"),
                         "Password":getUserDetail("password"),
                         "Company": getUserDetail("company_id"),
                         "CustomerName":txtCustomerName.text ?? "",
                         "Address1": txtAddress1.text ?? "",
                         "Address2":txtAddress2.text ?? ""
                ] as [String : Any]
            
            param["City"] = txtCity.text ?? ""
            param["Zip"] = txtZip.text ?? ""
            param["Customertype"] = txtPosition.text ?? ""
            param["Phone"] = txtPhone.text ?? ""
            param["Mobile"] = txtMobile.text ?? ""
            param["EmailPrint"] = txtEmail.text ?? ""
            param["Fax"] = txtFax.text ?? ""
            param["Notes"] = txtVwDescription.text ?? ""
            param["CountryId"] = Int(self.selectedCountryFromList)
            param["IsCompany"] = Int(self.isCompany)
            
            if selectedController == .add{
                param["CustomerId"] = 0
            }else if selectedController == .edit{
                param["CustomerId"] = self.dictData["id"].intValue
            }
            
            print("param : \(param)")
            
            self.showLoader()
            
            CommonService().ServicePostMethod(url: url, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value{
                    print("JSON : \(json)")
                    
                    if self.selectedController == .add{
                        self.handlerForAddCustomer()
                        self.navigationController?.popViewController(animated: true)
                    }else if self.selectedController == .edit{
                        self.navigationController?.popViewController(animated: true)
                        self.handlerForEditCustomer(json)
//                        self.handlerForEditCustomer(json)
                        /*for controller in self.navigationController!.viewControllers as Array {
                            if controller.isKind(of: CustomerListVc.self) {
                                self.navigationController!.popToViewController(controller, animated: true)
                                break
                            }
                        }*/
                    }
                    
                }
                else
                {
                    makeToast(strMessage: GlobalVariable.serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: GlobalVariable.interNotWorking)
        }
        
    }

    
    func countryList(){
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(GlobalVariable.baseURL)getCountries?UserId=\(getUserDetail("user_id"))&Username=\(getUserDetail("username"))&Password=\(getUserDetail("password"))&Company=\(getUserDetail("company_id"))"
            
            print("URL: \(url)")
            
            self.showLoader()
            
            CommonService().ServiceGETMethod(url: url, param: [:]) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value{
                    print("JSON : \(json)")
                    
                    self.jsonCountryList = json.arrayValue
                    
                    for i in 0..<self.jsonCountryList.count
                    {
                        let value = self.jsonCountryList[i]["name"].stringValue
                        self.strCountryList.append(value)
                    }
                    
                    self.countryDD.dataSource = self.strCountryList
                }
                else
                {
                    makeToast(strMessage: GlobalVariable.serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: GlobalVariable.interNotWorking)
        }
        
    }
}
