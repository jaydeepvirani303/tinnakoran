//
//  AddEditLoggedCallVc.swift
//  Tinnakoran
//
//  Created by YASH on 10/13/19.
//  Copyright © 2019 Yash. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON
import DropDown

class AddEditLoggedCallVc: UIViewController {

    @IBOutlet weak var txtName: CustomTextField!
    @IBOutlet weak var txtDate: CustomTextField!
    @IBOutlet weak var txtCategory: CustomTextField!
    @IBOutlet weak var txtDescription: UITextView!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var btnAddEdit: CustomButton!
    
    var selectedController = isForAddOrEdit.add
    var selectedParentController = fromHomeOrCustomer.customer
    
    var categoryDD = DropDown()
    var selectedCategoryFomList = 0
    
    var jsonCategoryList : [JSON] = []
    var strCategoryList = [String]()
    var dictData = JSON()
    var customerData = JSON()
    var dictOpportunityDetail = JSON()
    var dictContact = JSON()
    
    var handlerForAddLoggedCalls: ()->Void = {}
    var handlerForEditLoggedCalls: (JSON) -> Void = {_ in}

    var isComeFromOpportunity = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        
        self.categoryList()
        // Do any additional setup after loading the view.
    }

    
    @IBAction func btnSelecteCategoryTapped(_ sender: UIButton) {
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        if selectedController == .add{
            self.setupNavigationbar(titleText: "Add Looged Calls")
            btnAddEdit.setTitle("Add", for: .normal)
        }else if selectedController == .edit{
            self.setupNavigationbar(titleText: "Edit Looged Calls")
            btnAddEdit.setTitle("Edit Logged call".uppercased(), for: .normal)
            self.setDataForEdit(dict: dictData)
        }
        
    }
    
    override func viewDidLayoutSubviews() {
        
        self.configDD(dropdown: self.categoryDD, sender: self.txtCategory)
        selectionIndex()
    }
    
    func setupUI(){
        
        [txtName,txtDate,txtCategory].forEach { (txt) in
            txt?.placeHolderColor = UIColor.lightGray
            txt?.delegate = self
            txt?.font = themeFont(size: 13, fontname: .regular)
        }
        
        txtDescription.font = themeFont(size: 13, fontname: .regular)
        txtDescription.delegate = self
        
        btnAddEdit.backgroundColor = UIColor.appThemeLightGrayColor
    }
    
    func selectionIndex()
    {
        
        self.categoryDD.selectionAction = { (index, item) in
            self.txtCategory.text = item
            self.view.endEditing(true)
            
            self.selectedCategoryFomList = self.jsonCategoryList[index]["id"].intValue
            
            //            self.txtSelectCarType.text =
            //            self.txtSelectSeat.text = ""
            self.categoryDD.hide()
        }
    }
    
    //MARK: - DropDown setup
    
    func configDD(dropdown: DropDown, sender: UIView)
    {
        dropdown.anchorView = sender
        dropdown.direction = .any
        dropdown.dismissMode = .onTap
        //  dropdown.topOffset = CGPoint(x: 0, y: self.view.bounds.origin.y)
        dropdown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        // print("textfield Frame \(txtPurpose.frame)")
        dropdown.width = sender.bounds.width
        dropdown.cellHeight = 40.0
        dropdown.backgroundColor = UIColor.white
        dropdown.textColor = UIColor.black
        dropdown.selectionBackgroundColor = UIColor.clear
        
    }
    
    @IBAction func btnSelectCountryTapped(_ sender: UIButton) {
        categoryDD.show()
    }
    
    
    @IBAction func btnAddEditTapped(_ sender: CustomButton) {
        
        if (txtName.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: "Please enter name")
        }else if (txtDate.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: "Please enter date")
        }
        else
        {
            AddEditLoggedCallAPICalled()
        }
    }
    
    func setDataForEdit(dict:JSON){
        
        print("dict:\(dict)")
        
        txtDate.text = dict["date"].stringValue
        txtName.text = dict["name"].stringValue
        
        if dict["categ_id"].arrayValue.count == 0{
            txtCategory.text = ""
        }
        else
        {
            self.selectedCategoryFomList = dict["categ_id"][0].intValue
            txtCategory.text = dict["categ_id"][1].stringValue
        }
        
        if dict["description"].stringValue == ""{
            self.lblDescription.isHidden = false
        }else{
            self.lblDescription.isHidden = true
            txtDescription.text = dict["description"].stringValue
        }
    }
}

//MARK: - TextField Delegate

extension AddEditLoggedCallVc: UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtDate
        {
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "CommonDateTimeVC") as! CommonDateTimeVC
            obj.pickerDelegate = self
            obj.controlType = 2
         //   obj.isSetMinimumDate = true
           // obj.setMinimumDate = Date()
            obj.modalPresentationStyle = .overCurrentContext
            obj.modalTransitionStyle = .crossDissolve
            self.present(obj, animated: true, completion: nil)
            return false
        }
        
        return true
        
    }
    
}

extension AddEditLoggedCallVc : DateTimePickerDelegate
{
    func setDateandTime(dateValue: Date, type: Int) {
        print("dateValue  - ",dateValue)
        
        self.view.endEditing(true)
        
        if type == 2
        {
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            dateFormatter.locale =  Locale(identifier: "en_IN")
            txtDate.text = dateFormatter.string(from: dateValue) + " 00:00:00"
        }
        
    }
}

//MARK:- Textview Delegate

extension AddEditLoggedCallVc:UITextViewDelegate
{
    func textViewDidChange(_ textView: UITextView) {
        if textView.text == ""
        {
            self.lblDescription.isHidden = false
        }
        else
        {
            self.lblDescription.isHidden = true
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn shouldChangeTextInRange: NSRange, replacementText text: String) -> Bool
    {
        if text == "\n"  // Recognizes enter key in keyboard
        {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}


//MARK: - API calling

extension AddEditLoggedCallVc{
    
    func AddEditLoggedCallAPICalled()
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(GlobalVariable.baseURL)postLoggedCalls"
            
            print("URL: \(url)")
            var id = Int()
            if dictContact.count != 0 {
                id = self.dictContact["id"].intValue
            } else {
                id = self.customerData["id"].intValue
            }
            let userID = Int(getUserDetail("user_id"))
            
            var param = [
                "UserId":Int(userID!),
                "UserName":getUserDetail("username"),
                "Password":getUserDetail("password"),
                "Company":getUserDetail("company_id"),
                "CustomerId": id ,
                "Date": txtDate.text ?? "",
                "LoggedName":txtName.text ?? ""
                ] as [String : Any]
            
            
            param["Description"] = txtDescription.text ?? ""
            param["CategoryId"] = Int(selectedCategoryFomList)
        
            if selectedController == .add{
                param["LoggedCallId"] = 0
                if self.isComeFromOpportunity{
                    param["OpportunityId"] = self.dictOpportunityDetail["id"].intValue
                }
                
            }else if selectedController == .edit{
                param["LoggedCallId"] = self.dictData["id"].intValue
            }
            
            print("param : \(param)")
            
            self.showLoader()
            
            CommonService().ServicePostMethod(url: url, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value{
                    print("JSON : \(json)")
                    
                    if self.selectedController == .add{
                        self.handlerForAddLoggedCalls()
                        self.navigationController?.popViewController(animated: false)
                    }else if self.selectedController == .edit{                        
                        if self.selectedParentController == .home {
//                             self.handlerForEditLoggedCalls(json)
                            self.navigationController?.popViewController(animated: false)
                            self.handlerForEditLoggedCalls(json)
                        } else {
                            self.redirectToCustomerDetailsScreen()
                        }
                        
                    }
                    
                }
                else
                {
                    makeToast(strMessage: GlobalVariable.serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: GlobalVariable.interNotWorking)
        }
        
    }
    
    
    func categoryList(){
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(GlobalVariable.baseURL)getCategories?UserId=\(getUserDetail("user_id"))&Username=\(getUserDetail("username"))&Password=\(getUserDetail("password"))&Company=\(getUserDetail("company_id"))"
            
            print("URL: \(url)")
            
            self.showLoader()
            
            CommonService().ServiceGETMethod(url: url, param: [:]) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value{
                    print("JSON : \(json)")
                    
                    self.jsonCategoryList = json.arrayValue
                    
                    for i in 0..<self.jsonCategoryList.count
                    {
                        let value = self.jsonCategoryList[i]["name"].stringValue
                        self.strCategoryList.append(value)
                    }
                    
                    self.categoryDD.dataSource = self.strCategoryList
                }
                else
                {
                    makeToast(strMessage: GlobalVariable.serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: GlobalVariable.interNotWorking)
        }
        
    }
}
