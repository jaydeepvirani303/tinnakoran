//
//  ProductDetailsVC.swift
//  Tinnakoran
//
//  Created by YASH on 10/6/19.
//  Copyright © 2019 Yash. All rights reserved.
//

import UIKit
import SwiftyJSON
import AlamofireSwiftyJSON
import Alamofire

class ProductDetailsVC: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var lblMainHeader: UILabel!
    
    @IBOutlet weak var lblOnHandQty: UILabel!
    @IBOutlet weak var lblIncomigQty: UILabel!
    @IBOutlet weak var lblForcatedQty: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblCustomerLeadTime: UILabel!
    @IBOutlet weak var lblManufacture: UILabel!
    @IBOutlet weak var lblOriginCountry: UILabel!
    @IBOutlet weak var lblProductFunction: UILabel!
    @IBOutlet weak var lblFinishedCategory: UILabel!
    
    
    
    var dictData = JSON()
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        lblMainHeader.font = themeFont(size: 17, fontname: .medium)
        lblMainHeader.textColor = UIColor.appThemeGreenColor
        
      //  getProductDetails()
        
        setData(dict: dictData)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupNavigationbar(titleText: "Details")
    }
    
    func setData(dict:JSON)
    {
        lblMainHeader.text = dict["name"].stringValue
        
        lblCategory.attributedText = setColorAndFontString(str1: "= ATP : ", str1Color: UIColor.black, str1Font: themeFont(size: 16, fontname: .regular), str2: String(dict["virtual_available"].intValue - dict["incoming_qty"].intValue), str2Color: UIColor.appThemeGreenColor, str2Font: themeFont(size: 16, fontname: .medium))
        
        lblOnHandQty.attributedText = setColorAndFontString(str1: " On Hand : ", str1Color: UIColor.black, str1Font: themeFont(size: 16, fontname: .regular), str2: dict["qty_available"].stringValue, str2Color: UIColor.appThemeGreenColor, str2Font: themeFont(size: 16, fontname: .medium))
        
        lblIncomigQty.attributedText = setColorAndFontString(str1: "Incoming  : ", str1Color: UIColor.black, str1Font: themeFont(size: 16, fontname: .regular), str2: dict["incoming_qty"].stringValue, str2Color: UIColor.appThemeGreenColor, str2Font: themeFont(size: 16, fontname: .medium))
        
        let calculation = dict["qty_available"].intValue - dict["virtual_available"].intValue + dict["incoming_qty"].intValue
        
        lblForcatedQty.attributedText = setColorAndFontString(str1: "- Sale Order(Booked) : ", str1Color: UIColor.black, str1Font: themeFont(size: 16, fontname: .regular), str2: "\(calculation)", str2Color: UIColor.appThemeGreenColor, str2Font: themeFont(size: 16, fontname: .medium))
        
        lblCustomerLeadTime.attributedText = setColorAndFontString(str1: "Customer Lead Time : ", str1Color: UIColor.black, str1Font: themeFont(size: 16, fontname: .regular), str2: dict["sale_delay"].stringValue, str2Color: UIColor.appThemeGreenColor, str2Font: themeFont(size: 16, fontname: .medium))
        
        lblManufacture.attributedText = setColorAndFontString(str1: "Manufacture : ", str1Color: UIColor.black, str1Font: themeFont(size: 16, fontname: .regular), str2: dict["manu_short"].stringValue, str2Color: UIColor.appThemeGreenColor, str2Font: themeFont(size: 16, fontname: .medium))
        
        lblOriginCountry.attributedText = setColorAndFontString(str1: "Origin Country : ", str1Color: UIColor.black, str1Font: themeFont(size: 16, fontname: .regular), str2: dict["origin_country"].stringValue, str2Color: UIColor.appThemeGreenColor, str2Font: themeFont(size: 16, fontname: .medium))
        
        lblProductFunction.attributedText = setColorAndFontString(str1: "Product Function : ", str1Color: UIColor.black, str1Font: themeFont(size: 16, fontname: .regular), str2: dict["product_function"].stringValue == "" ? "-" : dict["product_function"].stringValue, str2Color: UIColor.appThemeGreenColor, str2Font: themeFont(size: 16, fontname: .medium))
        
        var jsonCategory = dict["finishedCategoryList"].arrayValue
        var arrayStrCategory : [String] = []
        for i in 0..<jsonCategory.count{
            arrayStrCategory.append(jsonCategory[i]["name"].stringValue)
        }
        
        let categoryName = arrayStrCategory.count == 0 ? "-" : arrayStrCategory.joined(separator: ", ")
        
        lblFinishedCategory.attributedText = setColorAndFontString(str1: "Finished Category : ", str1Color: UIColor.black, str1Font: themeFont(size: 16, fontname: .regular), str2: categoryName, str2Color: UIColor.appThemeGreenColor, str2Font: themeFont(size: 16, fontname: .medium))
        
    }

}

//MARK: - API calling

extension ProductDetailsVC
{
    
    func getProductDetails(){
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(GlobalVariable.baseURL)getProducts?UserId=\(getUserDetail("user_id"))&Username=\(getUserDetail("username"))&Password=\(getUserDetail("password"))&Company=\(getUserDetail("company_id"))&productId=\(self.dictData["id"].stringValue)"
            
            print("URL: \(url)")
            
            self.showLoader()
            
            CommonService().ServiceGETMethod(url: url, param: [:]) { (respones) in

                self.hideLoader()
                
                if let json = respones.value{
                    print("JSON : \(json)")
                    
                    self.setData(dict: json[0])
                    
                    
                }
                else
                {
                    makeToast(strMessage: GlobalVariable.serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: GlobalVariable.interNotWorking)
        }
        
    }
    
}
