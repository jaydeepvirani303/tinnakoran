//
//  ContactTblCell.swift
//  Tinnakoran
//
//  Created by YASH on 10/8/19.
//  Copyright © 2019 Yash. All rights reserved.
//

import UIKit

class ContactTblCell: UITableViewCell {

    @IBOutlet weak var btnUrgent: UIButton!
    @IBOutlet weak var imgBack: UIImageView!
    @IBOutlet weak var lblMain: UILabel!
    @IBOutlet weak var lblCompititor: UILabel!
    @IBOutlet weak var lblApprovePrice: UILabel!
    @IBOutlet weak var vwUper: UIView!
    @IBOutlet weak var vwBottom: UIView!
    
    @IBOutlet weak var constantWidthbtnUrgent: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.constantWidthbtnUrgent.constant = 70
//        self.btnUrgent.isHidden = true
        
        let image = imgBack.image?.withRenderingMode(.alwaysTemplate)
        imgBack.image = image
        imgBack.tintColor = UIColor.black

        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
