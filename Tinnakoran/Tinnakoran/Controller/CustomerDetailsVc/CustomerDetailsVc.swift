//
//  CustomerDetailsVc.swift
//  Tinnakoran
//
//  Created by YASH on 10/6/19.
//  Copyright © 2019 Yash. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON

class CustomerDetailsVc: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var lblHeaderTitle: UILabel!
    
    @IBOutlet weak var lblCompany: UILabel!
    @IBOutlet weak var lblCompanyValue: UILabel!
    
    @IBOutlet weak var lblCustomerType: UILabel!
    @IBOutlet weak var lblCustomerTypeValue: UILabel!
    
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblEmailValue: UILabel!
    
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var lblPhoneValue: UILabel!
    
    @IBOutlet weak var lblMobile: UILabel!
    @IBOutlet weak var lblMobileValue: UILabel!
    
    @IBOutlet weak var lblAddress: UILabel!
    
    @IBOutlet weak var txtAddressValue: UITextView!
    
    
//    @IBOutlet weak var lblAddressValue: UILabel!
    
    @IBOutlet weak var lblFax: UILabel!
    @IBOutlet weak var lblFaxValue: UILabel!
    
    @IBOutlet weak var lblNotes: UILabel!
    @IBOutlet weak var txtNotesValue: UITextView!
    
    @IBOutlet weak var tblContactList: UITableView!
    @IBOutlet weak var constantHeightTbl: NSLayoutConstraint!
    
    @IBOutlet weak var btnPending: CustomButton!
    @IBOutlet weak var btnSOAll: CustomButton!
    @IBOutlet weak var btnLoggedCall: CustomButton!
    @IBOutlet weak var btOpertunity: CustomButton!
    @IBOutlet weak var btnPriceRequest: CustomButton!
    
    //MARK: - Variable
    
    var dictData = JSON()
    var arrayContactList : [JSON] = []
    var handlerUpdateAPI:() -> Void = {}
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        
      //  getCustomerDetails()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupNavigationbar(titleText: "Details")
        
        let rightButton = UIBarButtonItem(image: UIImage(named:"edit-button") , style: .plain, target: self, action: #selector(btnEditCustomerTapped))
        rightButton.tintColor = .white
        self.navigationItem.rightBarButtonItem = rightButton
        
         tblContactList.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
        tblContactList.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        tblContactList.removeObserver(self, forKeyPath: "contentSize")
    }
    
    override func viewDidLayoutSubviews() {
        
    }
    
    @objc func btnEditCustomerTapped(){
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "AddEditCustomerVc") as! AddEditCustomerVc
         obj.selectedController = .edit
         obj.dictData = dictData
         obj.handlerForEditCustomer = { [weak self] value in
            self?.navigationController?.popViewController(animated: false)
            self?.handlerUpdateAPI()
            self?.setData(dict: value)
        }
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    func setupUI(){
        
        lblHeaderTitle.font = themeFont(size: 17, fontname: .medium)
        lblHeaderTitle.textColor = UIColor.appThemeGreenColor
        [lblCompany,lblCustomerType,lblEmail,lblPhone,lblMobile,lblAddress,lblFax,lblNotes].forEach { (lbl) in
            lbl?.font = themeFont(size: 16, fontname: .regular)
            lbl?.textColor = UIColor.appThemeGreenColor
        }
        [lblCompanyValue,lblCustomerTypeValue,lblEmailValue,lblPhoneValue,lblMobileValue,lblFaxValue].forEach { (lbl) in
            lbl?.font = themeFont(size: 16, fontname: .regular)
            lbl?.textColor = UIColor.appThemeLightGrayColor
        }
        
        [txtAddressValue,txtNotesValue].forEach { (txt) in
            txt?.font = themeFont(size: 16, fontname: .regular)
            txt?.textColor = UIColor.appThemeLightGrayColor
        }
        
        [btnPending,btnSOAll,btnLoggedCall,btOpertunity,btnPriceRequest].forEach { (btn) in
            btn?.backgroundColor = UIColor.appThemeLightGrayColor
            btn?.titleLabel?.font = themeFont(size: 15, fontname: .bold)
        }
        
         tblContactList.register(UINib(nibName: "ContactTblCell", bundle: nil), forCellReuseIdentifier: "ContactTblCell")
        
        setData(dict: dictData)
    }
    
    func setData(dict:JSON)
    {
        lblHeaderTitle.text = dict["name"].stringValue
        
        lblCompanyValue.text = dict["company_id"][1].stringValue == "" ? "-" : dict["company_id"][1].stringValue
        
        lblCustomerTypeValue.text = dict["function"].stringValue == "" ? "-" : dict["function"].stringValue
        
        lblEmailValue.text = dict["email_print"].stringValue == "" ? "-" : dict["email_print"].stringValue
        
        lblPhoneValue.text = dict["phone"].stringValue == "" ? "-" : dict["phone"].stringValue
        
        lblMobileValue.text = dict["mobile"].stringValue == "" ? "-" : dict["mobile"].stringValue

//        txtAddressValue.text = "\(dict["street"].stringValue) \(dict["street2"].stringValue), \(dict["city"].stringValue), \(dict["zip"].stringValue)" == "" ? "-" : "\(dict["street"].stringValue) \(dict["street2"].stringValue), \(dict["city"].stringValue), \(dict["zip"].stringValue)"
        
        txtAddressValue.text = dict["fullAddress"].stringValue

        lblFaxValue.text = dict["fax"].stringValue == "" ? "-" : dict["fax"].stringValue
        
        txtNotesValue.text = dict["comment"].stringValue == "" ? "-" : dict["comment"].stringValue
        
        self.arrayContactList = dict["contactList"].arrayValue
        self.tblContactList.reloadData()
    }
    
    
    //MARK:- Overide Method
    
        override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
            if object is UITableView {
                
                self.constantHeightTbl.constant = tblContactList.contentSize.height
                if self.constantHeightTbl.constant == 0 {
                    self.constantHeightTbl.constant = 100
                }
                self.view.layoutIfNeeded()
                self.view.layoutSubviews()
            }
        }
    
    
    //MARK: - IBAction
    @IBAction func btnSoPendingTapped(_ sender: UIButton) {
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "SaleOrderListVc") as! SaleOrderListVc
        obj.selectedCotroller = .SOPending
        obj.dictCusotmerData = dictData
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnSOAllTapped(_ sender: UIButton) {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "SaleOrderListVc") as! SaleOrderListVc
        obj.selectedCotroller = .SOAll
        obj.dictCusotmerData = dictData
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnLoggedCallsTapped(_ sender: UIButton) {
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "LoggedCallListVc") as! LoggedCallListVc
        obj.dictCusotmerData = dictData
        obj.selectedController = .customer
        self.navigationController?.pushViewController(obj, animated: true)
        
    }
    
    @IBAction func btnOpertunityTapped(_ sender: UIButton) {
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "OpportunityListVc") as! OpportunityListVc
        obj.dictCusotmerData = dictData
        obj.selectedController = .customer
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnSalesPriceRequestTapped(_ sender: UIButton) {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "SalePriceRequestListVc") as! SalePriceRequestListVc
        obj.dictCusotmerData = dictData
        obj.selectedController = .customer
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    
}


//MARK: - API calling
/*
extension CustomerDetailsVc
{
    
    func getCustomerDetails(){
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(GlobalVariable.baseURL)getCustomers?UserId=\(getUserDetail("user_id"))&Username=\(getUserDetail("username"))&Password=\(getUserDetail("password"))&Company=\(getUserDetail("company_id"))&customerId=\(self.dictData["id"].stringValue)"
            
            print("URL: \(url)")
            
            self.showLoader()
            
            CommonService().ServiceGETMethod(url: url, param: [:]) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value{
                    print("JSON : \(json)")
                    
                    self.setData(dict: json[0])
                    
                    
                }
                else
                {
                    makeToast(strMessage: GlobalVariable.serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: GlobalVariable.interNotWorking)
        }
        
    }
    
}
*/

extension CustomerDetailsVc: UITableViewDelegate,UITableViewDataSource{
    
//    func numberOfSections(in tableView: UITableView) -> Int {
//        if self.arrayContactList.count != 0{
//            return 1
//        }
//    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
            return self.arrayContactList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactTblCell") as! ContactTblCell
        
        let dict = self.arrayContactList[indexPath.row]
        
        let strName = dict["name"].stringValue == "" ? "-" : dict["name"].stringValue
        let strPhoneNUmber = dict["phone"].stringValue == "" ? "-" : dict["phone"].stringValue
        let strEmail = dict["email_print"].stringValue == "" ? "-" : dict["email_print"].stringValue
        let strPosition = dict["function"].stringValue == "" ? "-" : dict["function"].stringValue
        
        let name = setColorAndFontString(str1:"Name: " ,str2: strName+"\n")
        let phonenumber = setColorAndFontString(str1:"Phone: " ,str2: strPhoneNUmber+"\n")
        let email = setColorAndFontString(str1:"Email: " ,str2: strEmail+"\n")
        let position = setColorAndFontString(str1:"Position: " ,str2: strPosition)
        
        let attributedString = NSMutableAttributedString()
        attributedString.append(name)
        attributedString.append(phonenumber)
        attributedString.append(email)
        attributedString.append(position)
        
        cell.lblMain.attributedText = attributedString
        
        if dict["color"].stringValue != "" || dict["type"].stringValue != "" {
            cell.btnUrgent.isHidden = false
            cell.btnUrgent.backgroundColor = UIColor.hexStringToUIColor(hex: dict["color"].stringValue)
            cell.btnUrgent.setTitle(dict["type"].stringValue, for: .normal)
            cell.btnUrgent.titleLabel?.font = themeFont(size: 14, fontname: .regular)
        } else {
            cell.btnUrgent.isHidden = true
        }
       
        
//        cell.lblMain.text = " Name: \(strName) \n Phone: \(strPhoneNUmber) \n Email: \(strEmail) \n Position: \(strPosition)"
        
//        cell.lblMainHeader.text = dict["name"].stringValue
//        cell.lblSubdetails.text = "\(dict["categ_id"][1].stringValue)\n\(dict["company_id"][1].stringValue)"d
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
       /* if self.arrayContactList.count == 0{
            return nil
        }*/
        let view:CustomerDetailHeaderView = UINib(nibName: "CustomerDetailHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CustomerDetailHeaderView
        
        view.backgroundColor = .lightGray
        
        view.lblTitle.textColor = UIColor.black
        view.lblTitle.font = themeFont(size: 15, fontname: .bold)
        view.lblTitle.text = "Contacts"
        
        view.btnAddNewContactOutlet.setTitle(" Add new contacts ", for: .normal)
        view.btnAddNewContactOutlet.titleLabel?.font = themeFont(size: 15, fontname: .bold)
        view.btnAddNewContactOutlet.setTitleColor(.white, for: .normal)
        view.btnAddNewContactOutlet.clipsToBounds = true
        view.btnAddNewContactOutlet.layer.cornerRadius = 5
        view.btnAddNewContactOutlet.backgroundColor = UIColor.darkGray
        view.btnAddNewContactOutlet.addTarget(self, action: #selector(btnAddContactAction(_:)), for: .touchUpInside)
        return view
    }
    
    /*func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if self.arrayContactList.count != 0{
            return "Contacts"
        }
        return nil
    }*/
    
   /* func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view:CustomerDetailHeaderView = .fromNib()
        view.lblTitle.text = "Contact"
        view.lblTitle.font = themeFont(size: 15, fontname: .bold)
        view.lblTitle.textColor = .black
        
        view.btnAddNewContactOutlet.setTitle("Add New Contact", for: .normal)
        view.btnAddNewContactOutlet.setTitleColor(.black, for: .normal)
        view.btnAddNewContactOutlet.titleLabel!.font = themeFont(size: 15, fontname: .bold)
        view.btnAddNewContactOutlet.backgroundColor  = .darkGray
        view.btnAddNewContactOutlet.layer.cornerRadius = 5
        view.btnAddNewContactOutlet.clipsToBounds = true
        
        view.layoutIfNeeded()
        view.layoutSubviews()
        return view
    }*/
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "ContactDetail") as! ContactDetail
        obj.dictContact = arrayContactList[indexPath.row]
        obj.dictCustomerData = dictData
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        if self.arrayContactList.count != 0{
            return 60
//        }
//        return 0
    }
    
    
    @IBAction func btnAddContactAction(_ sender:UIButton) {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "AddContactsVC") as! AddContactsVC
        obj.customerData = dictData
        obj.handlerData = {[weak self] in
            self?.navigationController?.popViewController(animated: false)
            self?.handlerUpdateAPI()
        }
        self.navigationController?.pushViewController(obj, animated: true)
    }
}
