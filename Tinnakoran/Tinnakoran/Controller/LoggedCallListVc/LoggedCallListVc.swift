//
//  LoggedCallListVc.swift
//  Tinnakoran
//
//  Created by YASH on 10/11/19.
//  Copyright © 2019 Yash. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON

enum fromHomeOrCustomer{
    
    case customer
    case home
    case contact
}


class LoggedCallListVc: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var vwSearch: UIView!
    @IBOutlet weak var txtSearch: CustomTextField!
    @IBOutlet weak var tblLoggedList: UITableView!
    
    //MARK: - Variable
    
    var arrayLoogedList : [JSON] = []
    var dictCusotmerData = JSON()
    var dictContact = JSON()
    
    var selectedController = fromHomeOrCustomer.customer
    var nextOffset = 0
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tblLoggedList.register(UINib(nibName: "ProductTblCell", bundle: nil), forCellReuseIdentifier: "ProductTblCell")
        
        tblLoggedList.tableFooterView = UIView()
        
        getLoggedCallList(isRefreshing:true)
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        self.setupNavigationbar(titleText: "Logged Calls")
        
        if selectedController == .customer{
            let rightButton = UIBarButtonItem(image: UIImage(named:"add-plus-button") , style: .plain, target: self, action: #selector(btnAddLoggedCallsTapped))
            rightButton.tintColor = .white
            self.navigationItem.rightBarButtonItem = rightButton
            
            self.vwSearch.isHidden = true
        }else{
            print("Nothing")
            self.vwSearch.isHidden = false
        }
        
    }
    
    @objc func btnAddLoggedCallsTapped(){
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "AddEditLoggedCallVc") as! AddEditLoggedCallVc
        obj.selectedController = .add
        obj.customerData = self.dictCusotmerData
        obj.dictContact = dictContact
        obj.handlerForAddLoggedCalls = { [weak self] in
                self?.getLoggedCallList()
        }
        self.navigationController?.pushViewController(obj, animated: true)
        
    }
}

//MARK; - TextField Delegate

extension LoggedCallListVc:UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        self.nextOffset = 0
        searchLoggedCalls(isRefreshing: true)
        return true
        
    }
    
}



//MARK: - TableView Delegate
extension LoggedCallListVc: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.arrayLoogedList.count == 0{
            let lbl = UILabel()
            lbl.text = "No record found."
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = UIColor.black
            lbl.center = tableView.center
            tableView.backgroundView = lbl
            return 0
        }else{
            tableView.backgroundView = nil
            return self.arrayLoogedList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProductTblCell") as! ProductTblCell
        
        let dict = self.arrayLoogedList[indexPath.row]
        
        let categoryName = dict["categ_id"][1].stringValue == "" ? "-" : dict["categ_id"][1].stringValue

        cell.lblMainHeader.text = dict["name"].stringValue
        cell.lblSubdetails.text = "\(dict["partner_id"][1].stringValue) \n\(dict["date"].stringValue)\n\(categoryName)"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let dict = self.arrayLoogedList[indexPath.row]
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "LoggedCallDetailsVc") as! LoggedCallDetailsVc
        obj.dictCustomerData = self.dictCusotmerData
        obj.dictData = dict
        obj.dictContact = dictContact
        if selectedController == .customer{
            obj.selectedController = .customer
        }else if selectedController == .home{
            obj.selectedController = .home
        }
        obj.handlerAPICalled = {[weak self] in
            self?.nextOffset = 0
            self?.txtSearch.text = ""
            self?.getLoggedCallList()
        }
        
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if selectedController == .home{
            let lastSectionIndex = tableView.numberOfSections - 1
            
            let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
            
            if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && arrayLoogedList != [] && arrayLoogedList.count >= 10 {
                
                let spinner = UIActivityIndicatorView(style: .gray)
                spinner.color = UIColor.appThemeGreenColor
                spinner.startAnimating()
                spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
                tableView.tableFooterView = spinner
                tableView.tableFooterView?.isHidden = false
                
                self.nextOffset += 10
                
                if (txtSearch.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
                {
                    self.getLoggedCallList()
                }else{
                    self.searchLoggedCalls()
                }
                
            } else {
                tableView.tableFooterView?.isHidden = true
                tableView.tableFooterView = nil
            }
        }else{
            print("Nothing")
        }
        
    }
}

//MARK: - API calling

extension LoggedCallListVc{
    
    func getLoggedCallList(isRefreshing:Bool = false){
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            var url = ""
            
            if selectedController == .customer{
                url = "\(GlobalVariable.baseURL)getLoggedCalls?UserId=\(getUserDetail("user_id"))&Username=\(getUserDetail("username"))&Password=\(getUserDetail("password"))&Company=\(getUserDetail("company_id"))&CustomerId=\(dictCusotmerData["id"].stringValue)"
            }
            else if selectedController == .home{
                 url = "\(GlobalVariable.baseURL)getLoggedCalls?UserId=\(getUserDetail("user_id"))&Username=\(getUserDetail("username"))&Password=\(getUserDetail("password"))&Company=\(getUserDetail("company_id"))&CustomerId=0&offset=\(nextOffset)&pazeSize=10"
            }
           
            print("URL: \(url)")
            
            if isRefreshing{
                self.showLoader()
            }
            
            CommonService().ServiceGETMethod(url: url, param: [:]) { (respones) in
                
                if isRefreshing{
                    self.hideLoader()
                }
                
                if let json = respones.value{
                    print("JSON : \(json)")
                    
                    if json["isError"].boolValue == true{
                        makeToast(strMessage: "Something went wrong")
                        return
                    }
                    
                    
                    if self.selectedController == .customer{
                        self.arrayLoogedList = []
                        self.arrayLoogedList = json.arrayValue
                    }
                    else if self.selectedController == .home{
                        if self.nextOffset == 0{
                            self.arrayLoogedList = []
                        }
                        
                        let aryData = json.arrayValue
                        self.arrayLoogedList = self.arrayLoogedList + aryData
                        
                    }
                    
                    self.tblLoggedList.reloadData()
                    
                }
                else
                {
                    makeToast(strMessage: GlobalVariable.serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: GlobalVariable.interNotWorking)
        }
    }
    
    
    func searchLoggedCalls(isRefreshing:Bool = false){
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            print("nextOffset:\(nextOffset)")
            
            var strSearch = (txtSearch.text?.trimmingCharacters(in: .whitespaces)) ?? ""
            
            strSearch = strSearch.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
            
            let url = "\(GlobalVariable.baseURL)getLoggedCalls?UserId=\(getUserDetail("user_id"))&Username=\(getUserDetail("username"))&Password=\(getUserDetail("password"))&Company=\(getUserDetail("company_id"))&CustomerId=0&offset=\(nextOffset)&pazeSize=10&loggedCallName=\(strSearch)"
            
            print("URL: \(url)")
            
            if isRefreshing{
                self.showLoader()
            }
            
            CommonService().ServiceGETMethod(url: url, param: [:]) { (respones) in
                if isRefreshing{
                    self.hideLoader()
                }
                
                if let json = respones.value{
                    print("JSON : \(json)")
                    
                    if self.nextOffset == 0{
                        self.arrayLoogedList = []
                    }
                    
                    let aryData = json.arrayValue
                    self.arrayLoogedList = self.arrayLoogedList + aryData
                    self.tblLoggedList.reloadData()
                    
                }
                else
                {
                    makeToast(strMessage: GlobalVariable.serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: GlobalVariable.interNotWorking)
        }
        
    }

}



