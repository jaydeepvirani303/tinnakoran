//
//  ViewController.swift
//  Tinnakoran
//
//  Created by YASH on 02/10/19.
//  Copyright © 2019 Yash. All rights reserved.
//

import UIKit
import DropDown
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON

class ViewController: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var txtUserName: CustomTextField!
    @IBOutlet weak var txtPassword: CustomTextField!
    @IBOutlet weak var txtDropDown: CustomTextField!
    @IBOutlet weak var btnLogin: CustomButton!
    @IBOutlet weak var btnPassword: UIButton!
    
    //MARK: - Varaiable
    
    var companyDD = DropDown()
    var companySelected = ""
    var selectedCompanyFromList = ""
    
    var jsonCompanyList : [JSON] = []
    var strCompanyList = [String]()
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if (GlobalVariable.Defaults.object(forKey:"userDetails") != nil)
        {
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "SelectOptionsVc") as! SelectOptionsVc
            self.navigationController?.pushViewController(obj, animated: false)
        }
        else{
            setupUI()
        }
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewDidLayoutSubviews() {
        
        self.configDD(dropdown: self.companyDD, sender: self.txtDropDown)
        selectionIndex()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    func setupUI(){
        
        [txtUserName,txtPassword,txtDropDown].forEach { (txt) in
            txt?.delegate = self
            txt?.font = themeFont(size: 13, fontname: .regular)
        }
        txtDropDown.isUserInteractionEnabled = false
        txtPassword.isSecureTextEntry = true
        btnLogin.backgroundColor = UIColor.appThemeLightGrayColor
        btnLogin.titleLabel?.font = themeFont(size: 15, fontname: .bold)
        
        companyList()
    }
    
    func selectionIndex()
    {
        
        self.companyDD.selectionAction = { (index, item) in
            self.txtDropDown.text = item
            self.view.endEditing(true)
            
            self.selectedCompanyFromList = self.jsonCompanyList[index]["key"].stringValue
            self.companySelected = "1"
            
//            self.txtSelectCarType.text =
            //            self.txtSelectSeat.text = ""
            self.companyDD.hide()
        }
    }
    
    //MARK: - DropDown setup
    
    func configDD(dropdown: DropDown, sender: UIView)
    {
        dropdown.anchorView = sender
        dropdown.direction = .any
        dropdown.dismissMode = .onTap
        //  dropdown.topOffset = CGPoint(x: 0, y: self.view.bounds.origin.y)
        dropdown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        // print("textfield Frame \(txtPurpose.frame)")
        dropdown.width = sender.bounds.width
        dropdown.cellHeight = 40.0
        dropdown.backgroundColor = UIColor.white
        dropdown.textColor = UIColor.black
        dropdown.selectionBackgroundColor = UIColor.clear
        
    }
    
    @IBAction func btnDropDownTapped(_ sender: UIButton) {
        
        companyDD.show()
    }
    
    @IBAction func btnPasswordTapped(_ sender: UIButton) {
        
        if btnPassword.isSelected{
            txtPassword.isSecureTextEntry = true
            btnPassword.isSelected = false
        }else{
             btnPassword.isSelected = true
            txtPassword.isSecureTextEntry = false
        }
        
    }
}

//MARK: - IBAction method

extension ViewController{
    
    @IBAction func btnLoginTapped(_ sender: UIButton) {
        
        if (txtUserName.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: "Please enter username")
        }else if (txtPassword.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: "Please enter your password")
        }else if self.companySelected != "1"
        {
            makeToast(strMessage: "Please select company")
        }
        else
        {
            loginApiCalled()
        }
        
    }
    
}


//MARK: - TextField Delegate

extension ViewController: UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
}


//MARK: - API calling

extension ViewController{
    
    func loginApiCalled()
    {
        self.view.endEditing(true)
        
        let strUserName = (txtUserName.text?.trimmingCharacters(in: .whitespaces)) ?? ""
        let strPassword = (txtPassword.text?.trimmingCharacters(in: .whitespaces)) ?? ""

        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(GlobalVariable.baseURL)Login?Username=\(strUserName)&Password=\(strPassword)&Company=\(selectedCompanyFromList)"
            
            print("URL: \(url)")
            
            self.showLoader()
            
            CommonService().ServiceGETMethod(url: url, param: [:]) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value{
                    print("JSON : \(json)")
                    
                    if json["userId"].intValue == 0{
                        makeToast(strMessage: "Invalid Username/Password")
                        return
                    }
                    
                    var dict = JSON()
                    dict["username"].stringValue = strUserName
                    dict["password"].stringValue = strPassword
                    dict["company_id"].stringValue = self.selectedCompanyFromList
                    dict["user_id"].stringValue = json["userId"].stringValue
                    
                    guard let rowdata = try? dict.rawData() else {return}
                    GlobalVariable.Defaults.setValue(rowdata, forKey: "userDetails")
                    GlobalVariable.Defaults.synchronize()
                    
                    let obj = self.storyboard?.instantiateViewController(withIdentifier: "SelectOptionsVc") as! SelectOptionsVc
                    self.navigationController?.pushViewController(obj, animated: true)
                    
                }
                else
                {
                    makeToast(strMessage: GlobalVariable.serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: GlobalVariable.interNotWorking)
        }
        
    }
    
    func companyList(){
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(GlobalVariable.baseURL)getCompanies"
            
            print("URL: \(url)")
            
            self.showLoader()
            
            CommonService().ServiceGETMethod(url: url, param: [:]) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value{
                    print("JSON : \(json)")
                    
                    self.jsonCompanyList = json.arrayValue
                    
                    for i in 0..<self.jsonCompanyList.count
                    {
                        let value = self.jsonCompanyList[i]["value"].stringValue
                        self.strCompanyList.append(value)
                    }
                    
                    self.companyDD.dataSource = self.strCompanyList
                }
                else
                {
                    makeToast(strMessage: GlobalVariable.serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: GlobalVariable.interNotWorking)
        }
        
    }
    
}
