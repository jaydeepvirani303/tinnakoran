//
//  AddContactsVC.swift
//  Tinnakoran
//
//  Created by Jaydeep on 25/03/20.
//  Copyright © 2020 Yash. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON
import DropDown

class AddContactsVC: UIViewController {
    
    //MARK:- Outlet zone
    
    @IBOutlet weak var txtContactName: CustomTextField!
    @IBOutlet weak var txtJobPosition: CustomTextField!
    @IBOutlet weak var txtPhone: CustomTextField!
    @IBOutlet weak var txtEmailPrint: CustomTextField!
    @IBOutlet weak var txtCategoryName: CustomTextField!
    @IBOutlet weak var txtMobile: CustomTextField!
    @IBOutlet weak var btnAddOutlet: CustomButton!
    
    //MARK:- Variable Declaration
    
    var categoryDD = DropDown()
    var selectedCategoryFomList = String()
    var arrCategory:[JSON] = []
    var strCategoryList = [String]()
    var customerData = JSON()
    var handlerData:() -> Void = {}
    
    //MARK:- View Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        
        categoryList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupNavigationbar(titleText: "Add Contact")
    }
    
    func setupUI(){
        
        [txtContactName,txtJobPosition,txtPhone,txtEmailPrint,txtMobile,txtCategoryName].forEach { (txt) in
            txt?.placeHolderColor = UIColor.lightGray
            txt?.delegate = self
            txt?.font = themeFont(size: 13, fontname: .regular)
        }
        
        [txtPhone,txtMobile,].forEach { (txt) in
           addDoneButtonOnKeyboard(textfield: txt)
        }
        
        btnAddOutlet.setTitleColor(.white, for: .normal)
        btnAddOutlet.titleLabel?.font = themeFont(size: 20, fontname: .bold)
        btnAddOutlet.backgroundColor = UIColor.lightGray
        btnAddOutlet.setTitle("Add", for: .normal)
    }
    
    func selectionIndex() {
        
        self.categoryDD.selectionAction = { (index, item) in
            self.txtCategoryName.text = item
            self.view.endEditing(true)
            
            self.selectedCategoryFomList = self.arrCategory[index]["key"].stringValue
            
            self.categoryDD.hide()
        }
    }
    
    override func viewDidLayoutSubviews() {
        
        self.configDD(dropdown: self.categoryDD, sender: self.txtCategoryName)
        selectionIndex()
    }
    
    //MARK: - DropDown setup
    
    func configDD(dropdown: DropDown, sender: UIView)
    {
        dropdown.anchorView = sender
        dropdown.direction = .any
        dropdown.dismissMode = .onTap
        //  dropdown.topOffset = CGPoint(x: 0, y: self.view.bounds.origin.y)
        dropdown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        // print("textfield Frame \(txtPurpose.frame)")
        dropdown.width = sender.bounds.width
        dropdown.cellHeight = 40.0
        dropdown.backgroundColor = UIColor.white
        dropdown.textColor = UIColor.black
        dropdown.selectionBackgroundColor = UIColor.clear
        
    }
    
    @IBAction func btnSelectCategoryTapped(_ sender: UIButton) {
        categoryDD.show()
    }
    
    
    @IBAction func btnAddTapped(_ sender: CustomButton) {
        
        if (txtContactName.text?.trimmingCharacters(in: .whitespaces))!.isEmpty {
            makeToast(strMessage: "Please enter contact name")
        } /*else if (txtJobPosition.text?.trimmingCharacters(in: .whitespaces))!.isEmpty {
            makeToast(strMessage: "Please enter job position")
        } else if (txtPhone.text?.trimmingCharacters(in: .whitespaces))!.isEmpty {
            makeToast(strMessage: "Please enter phone number")
        } else if (txtMobile.text?.trimmingCharacters(in: .whitespaces))!.isEmpty {
            makeToast(strMessage: "Please enter mobile number")
        } else if (txtEmailPrint.text?.trimmingCharacters(in: .whitespaces))!.isEmpty {
            makeToast(strMessage: "Please enter email address")
        } else if txtEmailPrint.text?.isValidEmail() == false {
            makeToast(strMessage: "Please enter valid email address")
        }  else if (txtCategoryName.text?.trimmingCharacters(in: .whitespaces))!.isEmpty {
            makeToast(strMessage: "Please enter category")
        } */else {
            AddContactAPICalled()
        }
    }

}

//MARK: - TextField Delegate

extension AddContactsVC: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
}


//MARK: - API calling

extension AddContactsVC{
    
    func AddContactAPICalled()
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(GlobalVariable.baseURL)postCustomerContact"
            
            print("URL: \(url)")
            
            let userID = Int(getUserDetail("user_id"))
            
            let param = [
                "UserId":Int(userID!),
                "UserName":getUserDetail("username"),
                "Password":getUserDetail("password"),
                "Company":getUserDetail("company_id"),
                "ContactName":self.txtContactName.text ?? "",
                "JobPosition":self.txtJobPosition.text ?? "",
                "Phone":self.txtPhone.text ?? "",
                "Mobile":self.txtMobile.text ?? "",
                "EmailPrint":self.txtEmailPrint.text ?? "",
                "CustomerId":self.customerData["id"].intValue,
                "Contacttype":selectedCategoryFomList
                ] as [String : Any]
            
           
            
            print("param : \(param)")
            
            self.showLoader()
            
            CommonService().ServicePostMethod(url: url, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value{
                    print("JSON : \(json)")
                    self.navigationController?.popViewController(animated: false)
                    self.handlerData()
                }
                else
                {
                    makeToast(strMessage: GlobalVariable.serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: GlobalVariable.interNotWorking)
        }
        
    }
    
    
    func categoryList(){
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(GlobalVariable.baseURL)getContactType?UserId=\(getUserDetail("user_id"))&Username=\(getUserDetail("username"))&Password=\(getUserDetail("password"))&Company=\(getUserDetail("company_id"))"
            
            print("URL: \(url)")
            
            self.showLoader()
            
            CommonService().ServiceGETMethod(url: url, param: [:]) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value{
                    print("JSON : \(json)")
                    
                    self.arrCategory = json.arrayValue
                    
                    for i in 0..<self.arrCategory.count
                    {
                        let value = self.arrCategory[i]["value"].stringValue
                        self.strCategoryList.append(value)
                    }
                    
                    self.categoryDD.dataSource = self.strCategoryList
                    self.txtCategoryName.text = self.strCategoryList[0]
                    self.selectedCategoryFomList = self.arrCategory[0]["key"].stringValue
                }
                else
                {
                    makeToast(strMessage: GlobalVariable.serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: GlobalVariable.interNotWorking)
        }
        
    }
}
