//
//  CreateMessageVC.swift
//  Tinnakoran
//
//  Created by Jaydeep on 29/03/20.
//  Copyright © 2020 Yash. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON
import DropDown
import Photos

class CreateMessageVC: UIViewController {
    
    //MARK:- Variable Declaration
       
    var categoryDD = DropDown()
    var arrMessage:[JSON] = []
    var arrCategory:[JSON] = []
    var strCategoryList = [String]()
    let picker = UIImagePickerController()
    var image : UIImage?
    var nextOffset = 0
    var selectedCategoryFomList = String()
    
    //MARK:- Outlet zone
    
    @IBOutlet weak var txtCategory:UITextField!
    @IBOutlet weak var btnSelectImage:UIButton!
    @IBOutlet weak var btnSendOutlet:UIButton!
    @IBOutlet weak var btnClearText:UIButton!
    @IBOutlet weak var txtviewMessage:UITextView!
    @IBOutlet weak var lblMessageTitle:UILabel!
    @IBOutlet weak var lblLastOrderTitle:UILabel!
    @IBOutlet weak var tblMessage:UITableView!
    @IBOutlet weak var constantHeightTbl: NSLayoutConstraint!
    
    //MARK:- View Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        
        categoryList()
        getMessageList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupNavigationbar(titleText: "Messages")
         tblMessage.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        tblMessage.removeObserver(self, forKeyPath: "contentSize")
    }
    
    //MARK:- Overide Method
       
           override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
               if object is UITableView {

                 //  self.constantHeightTbl.constant = tblMessage.contentSize.height
               }
           }
     
    func setupUI() {
        txtCategory.textColor = .black
        txtCategory.font = themeFont(size: 15, fontname: .regular)
        
        btnSelectImage.titleLabel?.font = themeFont(size: 15, fontname: .regular)
        btnSelectImage.setTitleColor(.white, for: .normal)
        btnSelectImage.setTitle("Add Document", for: .normal)
        btnSelectImage.backgroundColor = .appThemeGreenColor
        btnSelectImage.clipsToBounds = true
        btnSelectImage.layer.cornerRadius = 5
        
        lblMessageTitle.textColor = .black
        lblMessageTitle.font = themeFont(size: 15, fontname: .regular)
        lblMessageTitle.text = "Message"
        
        txtviewMessage.textColor = .black
        txtviewMessage.font = themeFont(size: 15, fontname: .regular)
        
        btnSendOutlet.titleLabel?.font = themeFont(size: 15, fontname: .regular)
        btnSendOutlet.setTitleColor(.white, for: .normal)
        btnSendOutlet.setTitle("Send", for: .normal)
        btnSendOutlet.backgroundColor = .appThemeGreenColor
        btnSendOutlet.clipsToBounds = true
        btnSendOutlet.layer.cornerRadius = 5
        
        btnClearText.titleLabel?.font = themeFont(size: 15, fontname: .regular)
        btnClearText.setTitleColor(.white, for: .normal)
        btnClearText.backgroundColor = .systemPink
        btnClearText.setTitle("Clear Text", for: .normal)
        btnClearText.clipsToBounds = true
        btnClearText.layer.cornerRadius = 5
        
        lblLastOrderTitle.textColor = .black
        lblLastOrderTitle.font = themeFont(size: 15, fontname: .bold)
        lblLastOrderTitle.text = "Last Order (Click to copy)"
    }
    
    @IBAction func selectCategoryAction(_ sender:UIButton) {
        categoryDD.show()
    }
    
    @IBAction func btnSendAction(_ sender:UIButton) {
        /*if image == nil {
            makeToast(strMessage: "Please select image")
            return
        } else if self.txtviewMessage.text.trimmingCharacters(in: .whitespaces).isEmpty {
            makeToast(strMessage: "Please enter description")
            return
        }*/
        sendMessage()
    }
    
    @IBAction func btnClearTextAction(_ sender:UIButton) {
        txtviewMessage.text = ""
    }
    
    @IBAction func btnSelectImageAction(_ sender:UIButton) {
        alertActionForCamara()
    }
    
    @IBAction func btnCopyAction(_ sender:UIButton) {
        let dict = arrMessage[sender.tag]
        self.txtviewMessage.text = dict["notes"].stringValue
    }
    
    func selectionIndex() {
        
        self.categoryDD.selectionAction = { (index, item) in
            self.txtCategory.text = item
            self.view.endEditing(true)
            self.selectedCategoryFomList = self.arrCategory[index]["key"].stringValue
            self.categoryDD.hide()
        }
    }
    
    override func viewDidLayoutSubviews() {
        
        self.configDD(dropdown: self.categoryDD, sender: self.txtCategory)
        selectionIndex()
    }
    
    //MARK: - DropDown setup
    
    func configDD(dropdown: DropDown, sender: UIView)
    {
        dropdown.anchorView = sender
        dropdown.direction = .any
        dropdown.dismissMode = .onTap
        //  dropdown.topOffset = CGPoint(x: 0, y: self.view.bounds.origin.y)
        dropdown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        // print("textfield Frame \(txtPurpose.frame)")
        dropdown.width = sender.bounds.width
        dropdown.cellHeight = 40.0
        dropdown.backgroundColor = UIColor.white
        dropdown.textColor = UIColor.black
        dropdown.selectionBackgroundColor = UIColor.clear
        
    }
}

extension CreateMessageVC : UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}

//MAR:- TableView

extension CreateMessageVC:UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMessage.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CreateMessageCell") as! CreateMessageCell
        let dict = arrMessage[indexPath.row]
        let df = DateFormatter()
        df.dateFormat = "dd-MM-yyyy HH:mm"
        let date = df.date(from: dict["create_date"].stringValue)
        df.dateFormat = "dd-MM-yyyy"
        cell.lblDate.text = df.string(from: date!)
        cell.lblName.text = dict["name"].stringValue
        cell.lblDescription.text = dict["notes"].stringValue
        cell.btnDoneOutlet.setTitle(dict["state"].stringValue, for: .normal)
        cell.btnCopyOutlet.addTarget(self, action: #selector(btnCopyAction(_:)), for: .touchUpInside)
        cell.btnCopyOutlet.tag = indexPath.row
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let lastSectionIndex = tableView.numberOfSections - 1
        
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && arrMessage != [] && arrMessage.count >= 10 {
            
            let spinner = UIActivityIndicatorView(style: .gray)
            spinner.color = UIColor.appThemeGreenColor
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
            tableView.tableFooterView = spinner
            tableView.tableFooterView?.isHidden = false
            
            self.nextOffset += 10
            
            self.getMessageList(isRefreshing: false)
            
        } else {
            tableView.tableFooterView?.isHidden = true
            tableView.tableFooterView = nil
        }
        
    }
    
}

//MARK: - UIImagePickerController

extension CreateMessageVC:UIImagePickerControllerDelegate,
UINavigationControllerDelegate {
    func alertActionForCamara()  {
        
        // Create the alert controller
        let alertController = UIAlertController(title: "Doctor Portal", message: "Please choose any one", preferredStyle: .actionSheet)
        
        // Create the actions
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default) {
            UIAlertAction in
            
            self.picker.allowsEditing = true
            self.picker.delegate = self
            self.picker.sourceType = .photoLibrary
            self.picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
            self.picker.modalPresentationStyle = .popover
            self.present(self.picker, animated: true, completion: nil)
            
        }
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default) {
            UIAlertAction in
            
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                self.picker.allowsEditing = true
                self.picker.delegate = self
                self.picker.sourceType = UIImagePickerController.SourceType.camera
                self.picker.cameraCaptureMode = .photo
                self.picker.modalPresentationStyle = .fullScreen
                self.present(self.picker,animated: true,completion: nil)
            } else {
                makeToast(strMessage: "Sorry, this device has not camera.")
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        
        // Add the actions
        alertController.addAction(gallaryAction)
        alertController.addAction(cameraAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
//        isSelectedImage = true
        if #available(iOS 11.0, *) {
            if picker.sourceType == .camera {
                self.btnSelectImage.setTitle("Document.png", for: .normal)
            } else {
                guard let fileUrl = info[UIImagePickerController.InfoKey.imageURL] as? URL else { return }
                self.btnSelectImage.setTitle(fileUrl.lastPathComponent, for: .normal)
                print(fileUrl.lastPathComponent)
            }
        } else {
            // Fallback on earlier versions
        }
        
        
        if let editedImage = info[.editedImage] as? UIImage {
            image = editedImage
//            self.imgProfile.image = editedImage
            picker.dismiss(animated: true, completion: nil)
        } else if let originalImage = info[.originalImage] as? UIImage {
            image = originalImage
//            self.imgProfile.image = originalImage
            picker.dismiss(animated: true, completion: nil)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}

//MARK: - API calling

extension CreateMessageVC{
    
    func categoryList(){
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(GlobalVariable.baseURL)getWebTaskType?UserId=\(getUserDetail("user_id"))&Username=\(getUserDetail("username"))&Password=\(getUserDetail("password"))&Company=\(getUserDetail("company_id"))"
            
            print("URL: \(url)")
            
            self.showLoader()
            
            CommonService().ServiceGETMethod(url: url, param: [:]) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value{
                    print("JSON : \(json)")
                    
                    self.arrCategory = json.arrayValue
                    
                    for i in 0..<self.arrCategory.count
                    {
                        let value = self.arrCategory[i]["value"].stringValue
                        self.strCategoryList.append(value)
                    }
                    
                    self.categoryDD.dataSource = self.strCategoryList
                    self.txtCategory.text = self.strCategoryList[0]
                    self.selectedCategoryFomList = self.arrCategory[0]["key"].stringValue
                }
                else
                {
                    makeToast(strMessage: GlobalVariable.serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: GlobalVariable.interNotWorking)
        }
        
    }
    
    func getMessageList(isRefreshing:Bool = false){
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            print("nextOffset:\(nextOffset)")
            
            let url = "\(GlobalVariable.baseURL)getTaskMessage?offset=\(nextOffset)&pazeSize=10&UserId=\(getUserDetail("user_id"))&Username=\(getUserDetail("username"))&Password=\(getUserDetail("password"))&Company=\(getUserDetail("company_id"))"
            
            print("URL: \(url)")
            
            if isRefreshing{
                self.showLoader()
            }
            
            CommonService().ServiceGETMethod(url: url, param: [:]) { (respones) in
                if isRefreshing{
                    self.hideLoader()
                }
                
                if let json = respones.value{
                    print("JSON : \(json)")
                    
                    if self.nextOffset == 0{
                        self.arrMessage = []
                    }
                    
                    let aryData = json.arrayValue
                    self.arrMessage = self.arrMessage + aryData
                    
                    self.tblMessage.reloadData()
                    
                }
                else
                {
                    makeToast(strMessage: GlobalVariable.serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: GlobalVariable.interNotWorking)
        }
    }
    
    func sendMessage()
    {
        self.view.endEditing(true)
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(GlobalVariable.baseURL)postTaskMessage"
            print("URL: \(url)")
            
            let userID = Int(getUserDetail("user_id"))
            
            let param =  [
            "UserId":Int(userID!),
            "UserName":getUserDetail("username"),
            "Password":getUserDetail("password"),
            "Company":getUserDetail("company_id"),
            "MessageType":selectedCategoryFomList,
            "Message":self.txtviewMessage.text ?? ""
            
            ] as [String : Any]
          
            
            print("Param : \(param)")
            
           self.showLoader() 
            
            CommonService().makeReqeusrWithMultipartData(with: url, method: .post, parameter: param, image: image, success: { (respones) in
                
                self.stopAnimating()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    if json["webTaskId"].exists() {
                        self.nextOffset = 0
                        self.getMessageList()
                        self.image = nil
                        self.txtviewMessage.text = ""
                        self.btnSelectImage.setTitle("Add Document", for: .normal)
                    }
                }
                else
                {
                    makeToast(strMessage: GlobalVariable.serverNotResponding)
                }
            }, failure: {(error,code) in
                makeToast(strMessage: error)
            })
            
        }
        else
        {
            
            makeToast(strMessage: GlobalVariable.interNotWorking)
        }
    }
}

