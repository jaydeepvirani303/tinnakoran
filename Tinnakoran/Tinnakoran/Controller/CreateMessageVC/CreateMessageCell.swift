//
//  CreateMessageCell.swift
//  Tinnakoran
//
//  Created by Jaydeep on 29/03/20.
//  Copyright © 2020 Yash. All rights reserved.
//

import UIKit

class CreateMessageCell: UITableViewCell {
    
    @IBOutlet weak var lblDate:UILabel!
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var lblDescription:UILabel!
    @IBOutlet weak var btnDoneOutlet:UIButton!
    @IBOutlet weak var btnCopyOutlet:UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        [lblDate,lblName,lblDescription].forEach { (lbl) in
            lbl?.textColor = .black
            lbl?.font = themeFont(size: 13, fontname: .regular)
        }
        lblDate.textColor = .appThemeGreenColor
        
        btnDoneOutlet.backgroundColor = .appThemeGreenColor
        btnDoneOutlet.titleLabel?.font = themeFont(size: 13, fontname: .regular)
        btnDoneOutlet.setTitleColor(.white, for: .normal)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
