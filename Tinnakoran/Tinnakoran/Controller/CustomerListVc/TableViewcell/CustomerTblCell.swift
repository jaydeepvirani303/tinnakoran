//
//  CustomerTblCell.swift
//  Tinnakoran
//
//  Created by YASH on 10/6/19.
//  Copyright © 2019 Yash. All rights reserved.
//

import UIKit

class CustomerTblCell: UITableViewCell {

    @IBOutlet weak var lblStageOpportunity: UILabel!
    @IBOutlet weak var viewOpportunity: UIView!
    @IBOutlet weak var lblHeaader: UILabel!
    @IBOutlet weak var imgBack: UIImageView!
    @IBOutlet weak var vwAddLoggedCall: UIView!
    @IBOutlet weak var btnAddLoggedCall: CustomButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        btnAddLoggedCall.backgroundColor = UIColor.appThemeLightGrayColor
        
        lblHeaader.font = themeFont(size: 13, fontname: .medium)
        lblHeaader.textColor = UIColor.appThemeGreenColor
        
        let image = imgBack.image?.withRenderingMode(.alwaysTemplate)
        imgBack.image = image
        imgBack.tintColor = UIColor.black
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
