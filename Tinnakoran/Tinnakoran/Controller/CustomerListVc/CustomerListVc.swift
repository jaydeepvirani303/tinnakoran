//
//  CustomerListVc.swift
//  Tinnakoran
//
//  Created by YASH on 10/6/19.
//  Copyright © 2019 Yash. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON

class CustomerListVc: UIViewController {
    
    //MARK: - Outlet
    @IBOutlet weak var txtSearch: CustomTextField!
    @IBOutlet weak var tblCustomer: UITableView!
    
    //MARK: - Variable
    
    var nextOffset = 0
    var arrayCustomer : [JSON] = []
    var isComeFromSearch = false
    
    //MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        self.setupNavigationbar(titleText: "Customers")
        
        let rightButton = UIBarButtonItem(image: UIImage(named:"add-plus-button") , style: .plain, target: self, action: #selector(btnAddCustomerTapped))
        rightButton.tintColor = .white
        self.navigationItem.rightBarButtonItem = rightButton
    }
    
    @objc func btnAddCustomerTapped(){
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "AddEditCustomerVc") as! AddEditCustomerVc
        obj.selectedController = .add
        obj.handlerForAddCustomer = { [weak self] in
//            self?.txtSearch.text = ""
//            self?.nextOffset = 0
            self?.searchCustomer(isRefreshing: true)
//            self?.getCustomerList(isRefreshing : true)
        }
        self.navigationController?.pushViewController(obj, animated: true)
    }
}

//MARK: - Setup UI

extension CustomerListVc{
    
    func setupUI(){
        
        tblCustomer.register(UINib(nibName: "CustomerTblCell", bundle: nil), forCellReuseIdentifier: "CustomerTblCell")
        
        txtSearch.placeHolderColor = UIColor.darkGray
        
        // printFonts()
        
        self.getCustomerList(isRefreshing: true)
    }
    
}

//MARK: - TableView Delegate and DataSource

extension CustomerListVc: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.arrayCustomer.count == 0{
            let lbl = UILabel()
            lbl.text = "No record found."
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = UIColor.black
            lbl.center = tableView.center
            tableView.backgroundView = lbl
            return 0
        }else{
            tableView.backgroundView = nil
            return self.arrayCustomer.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomerTblCell") as! CustomerTblCell
        
        cell.lblHeaader.font = themeFont(size: 15, fontname: .medium)
        
        let dict = self.arrayCustomer[indexPath.row]
        
        cell.lblHeaader.text = dict["name"].stringValue
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let dict = self.arrayCustomer[indexPath.row]
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "CustomerDetailsVc") as! CustomerDetailsVc
        obj.dictData = dict
        obj.handlerUpdateAPI = {[weak self] in
            self?.nextOffset = 0
            if self!.isComeFromSearch {
                self?.searchCustomer(isRefreshing: true)
            } else {
                self?.searchCustomer(isRefreshing: true)
            }
        }
        self.navigationController?.pushViewController(obj, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let lastSectionIndex = tableView.numberOfSections - 1
        
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && arrayCustomer != [] && arrayCustomer.count >= 10 {
            
            let spinner = UIActivityIndicatorView(style: .gray)
            spinner.color = UIColor.appThemeGreenColor
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
            tableView.tableFooterView = spinner
            tableView.tableFooterView?.isHidden = false
            
            self.nextOffset += 10
            
            if (txtSearch.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
            {
                self.getCustomerList()
            }else{
                self.searchCustomer()
            }
            
        } else {
            tableView.tableFooterView?.isHidden = true
            tableView.tableFooterView = nil
        }
        
    }
    
}

//MARK; - TextField Delegate

extension CustomerListVc:UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        self.nextOffset = 0
        searchCustomer(isRefreshing: true)
        return true
        
    }
    
}

//MARK: - API calling

extension CustomerListVc{
    
    func getCustomerList(isRefreshing:Bool = false){
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            print("nextOffset:\(nextOffset)")
            
            let url = "\(GlobalVariable.baseURL)getCustomers?offset=\(nextOffset)&pazeSize=10&UserId=\(getUserDetail("user_id"))&Username=\(getUserDetail("username"))&Password=\(getUserDetail("password"))&Company=\(getUserDetail("company_id"))"
            
            print("URL: \(url)")
            
            if isRefreshing{
                self.showLoader()
            }
            
            CommonService().ServiceGETMethod(url: url, param: [:]) { (respones) in
                if isRefreshing{
                    self.hideLoader()
                }
                
                if let json = respones.value{
                    print("JSON : \(json)")
                    self.isComeFromSearch = false
                    if self.nextOffset == 0{
                        self.arrayCustomer = []
                    }
                    
                    let aryData = json.arrayValue
                    self.arrayCustomer = self.arrayCustomer + aryData
                    self.tblCustomer.reloadData()
                    
                }
                else
                {
                    makeToast(strMessage: GlobalVariable.serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: GlobalVariable.interNotWorking)
        }
        
    }
    
    
    func searchCustomer(isRefreshing:Bool = false){
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            print("nextOffset:\(nextOffset)")
            
            var strSearch = (txtSearch.text?.trimmingCharacters(in: .whitespaces)) ?? ""
            
            strSearch = strSearch.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
            
            let url = "\(GlobalVariable.baseURL)getCustomers?offset=\(nextOffset)&pazeSize=10&UserId=\(getUserDetail("user_id"))&Username=\(getUserDetail("username"))&Password=\(getUserDetail("password"))&Company=\(getUserDetail("company_id"))&customerName=\(strSearch.lowercased())"
            
            print("URL: \(url)")
            
            if isRefreshing{
                self.showLoader()
            }
            
            CommonService().ServiceGETMethod(url: url, param: [:]) { (respones) in
                if isRefreshing{
                    self.hideLoader()
                }
                
                if let json = respones.value{
                    print("JSON : \(json)")
                    self.isComeFromSearch = true
                    if self.nextOffset == 0{
                        self.arrayCustomer = []
                    }
                    
                    let aryData = json.arrayValue
                    self.arrayCustomer = self.arrayCustomer + aryData
                    self.tblCustomer.reloadData()
                    
                }
                else
                {
                    makeToast(strMessage: GlobalVariable.serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: GlobalVariable.interNotWorking)
        }
        
    }
    
}
