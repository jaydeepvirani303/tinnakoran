//
//  SalePriceRequestDetailsVc.swift
//  Tinnakoran
//
//  Created by YASH on 11/1/19.
//  Copyright © 2019 Yash. All rights reserved.
//

import UIKit
import AlamofireSwiftyJSON
import SwiftyJSON

class SalePriceRequestDetailsVc: UIViewController {
    
    //MARK: - Outlet
    
    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var lblCustomeName: UILabel!
    @IBOutlet weak var txtCustomerName: UITextView!
    
    @IBOutlet weak var lblCustomerProductDescription: UILabel!
    @IBOutlet weak var txtCustomerProductDescription: UITextView!
    
    @IBOutlet weak var lblApproverName: UILabel!
    @IBOutlet weak var txtvwApprovername: UITextView!
    
    @IBOutlet weak var lblRequestedDate: UILabel!
    @IBOutlet weak var txtRequestedDate: UITextView!
    
    @IBOutlet weak var lblOrderStatus: UILabel!
    @IBOutlet weak var txtOrderStatus: UITextView!
    
    @IBOutlet weak var tblSaleRequest: UITableView!
    
    @IBOutlet weak var constantTblHeight: NSLayoutConstraint!
    
    //MARK: - Check
    
    var dictCusotmerData = JSON()
    var dictData = JSON()
    var arraySaleRequestList : [JSON] = []
    
    var selectedController = fromHomeOrCustomer.customer

    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        self.setupNavigationbar(titleText: "Details")
        
        if selectedController == .customer{
            let rightButton = UIBarButtonItem(image: UIImage(named:"edit-button") , style: .plain, target: self, action: #selector(btnEditPriceRequestTapped))
            rightButton.tintColor = .white
            self.navigationItem.rightBarButtonItem = rightButton
        }

        tblSaleRequest.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        tblSaleRequest.removeObserver(self, forKeyPath: "contentSize")
    }
    
    @objc func btnEditPriceRequestTapped(sender:UIButton){
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "AddEditPriceRequestVc") as! AddEditPriceRequestVc
        obj.selectedCotroller = .edit
        obj.dictData = dictData
        obj.customerData = self.dictCusotmerData
        obj.handlerForEditPriceRequest = { [weak self] value in
           // self?.setData(dict: value)
        }
        self.navigationController?.pushViewController(obj, animated: true)
        
    }
    //MARK: - SetupUI
    
    func setupUI(){
        
        
        //        sale_header: name, parent_id = Customer name, invoice_id = Invoice Address, shipping_id = shiping Address, Order Date, Requested Date, client_order_ref = Order Reference, state = Status
        
        lblName.font = themeFont(size: 17, fontname: .medium)
        lblName.textColor = UIColor.appThemeGreenColor
        [lblCustomeName,lblCustomerProductDescription,lblApproverName,lblRequestedDate,lblOrderStatus].forEach { (lbl) in
            lbl?.font = themeFont(size: 16, fontname: .regular)
            lbl?.textColor = UIColor.appThemeGreenColor
        }
        [txtCustomerName,txtCustomerProductDescription,txtvwApprovername,txtRequestedDate,txtOrderStatus].forEach { (lbl) in
            lbl?.font = themeFont(size: 16, fontname: .regular)
            lbl?.textColor = UIColor.appThemeLightGrayColor
            lbl?.textContainerInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
        
        txtRequestedDate.textContainerInset = UIEdgeInsets(top: 5, left: 0, bottom: 0, right: 0)
        
        tblSaleRequest.register(UINib(nibName: "ContactTblCell", bundle: nil), forCellReuseIdentifier: "ContactTblCell")
        
        setData(dict:dictData)
        
    }
    //MARK:- Overide Method
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if object is UITableView {
            
            self.constantTblHeight.constant = tblSaleRequest.contentSize.height
        }
    }
    
    func setData(dict:JSON){
        
        print("dict:\(dict)")
        
        lblName.text = dict["name"].stringValue == "" ? "-" : dict["name"].stringValue
        
        txtCustomerName.text = dict["partner_id"][1].stringValue == "" ? "-" : dict["partner_id"][1].stringValue
        
        txtCustomerProductDescription.text = dict["partner_fin_product"].stringValue == "" ? "-" : dict["partner_fin_product"].stringValue
        
        txtvwApprovername.text = dict["approver_id"][1].stringValue == "" ? "-" : dict["approver_id"][1].stringValue
        
        txtRequestedDate.text = dict["request_date"].stringValue == "" ? "-" : dict["request_date"].stringValue
        
        txtOrderStatus.text = dict["state"].stringValue == "" ? "-" : dict["state"].stringValue
        
        lblOrderStatus.attributedText = setColorAndFontString(str1: "Status : ", str1Color: UIColor.appThemeGreenColor, str2: dict["state"].stringValue == "" ? "-" : dict["state"].stringValue.capitalized, str2Color: UIColor.appThemeLightGrayColor)
        
        self.arraySaleRequestList = dict["saleRequestLines"].arrayValue
        self.tblSaleRequest.reloadData()
    }
    
}


//MARK: - TableView Delegate
extension SalePriceRequestDetailsVc: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.arraySaleRequestList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactTblCell") as! ContactTblCell
        
        cell.imgBack.isHidden = true
        cell.lblCompititor.isHidden = false
        cell.vwUper.isHidden = false
        cell.vwBottom.isHidden = false
        cell.lblApprovePrice.isHidden = false
        
        let dict = self.arraySaleRequestList[indexPath.row]
        
        let strProductName = dict["product_id"][1].stringValue == "" ? "-" : dict["product_id"][1].stringValue
        let strIsNewProduct = dict["is_new_product"].boolValue
        let strQuantityPerOrder = dict["quantity_time"].stringValue == "" ? "-" : dict["quantity_time"].stringValue
        let strQuantityPerYear = dict["quantity_year"].stringValue == "0" ? "-" : dict["quantity_year"].stringValue
        let struom = dict["uom"].stringValue == "" ? "-" : dict["uom"].stringValue
        let strComment = dict["product_comment"].stringValue == "" ? "-" : dict["product_comment"].stringValue
        let strProductCountry = dict["product_comp_country"].stringValue == "" ? "-" : dict["product_comp_country"].stringValue
        let strProductCompPrice = dict["product_comp_price"].stringValue == "0.0" ? "-" : dict["product_comp_price"].stringValue
        let strProductCompUnit = dict["product_comp_unit"].stringValue == "" ? "-" : dict["product_comp_unit"].stringValue
        let strProductApprovedPrice = dict["product_app_price"].stringValue == "" ? "-" : dict["product_app_price"].stringValue
        let strProductApprovedUnit = dict["product_app_unit"].stringValue == "" ? "-" : dict["product_app_unit"].stringValue
        let strProductApprovedComment = dict["product_app_comment"].stringValue == "" ? "-" : dict["product_app_comment"].stringValue

        
        let productName = setColorAndFontString(str1:"Product Name: " ,str2: strProductName+"\n")
        let newProduct = setColorAndFontString(str1:"IsNewProduct: " ,str2: "\(strIsNewProduct)"+"\n")
        let quantityPerOrder = setColorAndFontString(str1:"Quantity Per Order: " ,str2: (strQuantityPerOrder + " \(struom)") + "\n")
        let quantityPerYear = setColorAndFontString(str1:"Quantity Per Year: " ,str2: strQuantityPerYear + " \(struom)")
       // let uom = setColorAndFontString(str1:"Unit Of Measure: " ,str2: struom+"")
      //  let productComment = setColorAndFontString(str1:"Remark: " ,str2: strComment+"\n")
        let productCountry = setColorAndFontString(str1:"Country Of Origin: " ,str2: strProductCountry+"\n")
        
           let productCompprice = setColorAndFontString(str1:"Product Competitor Price: " ,str2: (strProductCompPrice + " \(strProductCompUnit)") )
   //     let productCompUnit = setColorAndFontString(str1:"Product Competitor Unit: " ,str2: strProductCompUnit+"")
        let productApprPrice = setColorAndFontString(str1:"Product Approved Price: " ,str1Color: UIColor.red, str2: strProductApprovedPrice + " \(strProductApprovedUnit)"+"\n")
  //      let productApprUnit = setColorAndFontString(str1:"Product Approved Unit: " ,str2: strProductApprovedUnit+"\n")
        let productApprComm = setColorAndFontString(str1:"Product Approved Comment: " ,str1Color: UIColor.red,str2: strProductApprovedComment)
        
        let attributedString = NSMutableAttributedString()
        attributedString.append(productName)
        attributedString.append(newProduct)
        attributedString.append(quantityPerOrder)
        attributedString.append(quantityPerYear)
   //     attributedString.append(uom)
   //     attributedString.append(productComment)
        cell.lblMain.attributedText = attributedString
        
        let attributedString3 = NSMutableAttributedString()
        attributedString3.append(productCountry)
        attributedString3.append(productCompprice)
      //  attributedString3.append(productCompUnit)
        cell.lblCompititor.attributedText = attributedString3
        
        let attributedString2 = NSMutableAttributedString()
        
        attributedString2.append(productApprPrice)
      //  attributedString2.append(productApprUnit)
        attributedString2.append(productApprComm)
        
        cell.lblApprovePrice.attributedText = attributedString2
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if self.arraySaleRequestList.count != 0{
            let lbl = UILabel()
            lbl.text = "Sale Request lines"
            lbl.textColor = UIColor.black
            lbl.font = themeFont(size: 14, fontname: .regular)
            return lbl.text
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if self.arraySaleRequestList.count != 0{
            return 50
        }
        return 0
    }
}


