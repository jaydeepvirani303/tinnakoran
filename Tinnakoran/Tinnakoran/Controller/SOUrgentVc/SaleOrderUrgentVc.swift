
//
//  SaleOrderUrgentVc.swift
//  Tinnakoran
//
//  Created by YASH on 10/21/19.
//  Copyright © 2019 Yash. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON
import DropDown

class SaleOrderUrgentVc: UIViewController {

    
    @IBOutlet weak var txtWhenToDeliver: CustomTextField!
    @IBOutlet weak var txtWhyUrgent: CustomTextField!
    @IBOutlet weak var lblPlaceholder: UILabel!
    @IBOutlet weak var txtVwNote: UITextView!
    @IBOutlet weak var btnSubmit: CustomButton!
    
    
    var reasonDD = DropDown()
    var selectedReasonFomList = 0
    var jsonReasonList : [JSON] = []
    var strReasonList = [String]()

    
    var dictData = JSON()
    
    var handlerRefreshPendingList:() -> Void = {}
    
    var isChangeDateButtonClick = false
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        
        self.setupNavigationbar(titleText: "Pending Sale Order - Urgent")
        
    }
    
    override func viewDidLayoutSubviews() {
        
        self.configDD(dropdown: self.reasonDD, sender: self.txtWhyUrgent)
        selectionIndex()
    }
    
    func setupUI(){
        
        [txtWhenToDeliver,txtWhyUrgent].forEach { (txt) in
            txt?.placeHolderColor = UIColor.lightGray
            txt?.delegate = self
            txt?.font = themeFont(size: 13, fontname: .regular)
        }
        
        txtVwNote.font = themeFont(size: 13, fontname: .regular)
        txtVwNote.delegate = self
        
        btnSubmit.backgroundColor = UIColor.appThemeLightGrayColor
        
        reasonList()
    }

    func selectionIndex()
    {
        
        self.reasonDD.selectionAction = { (index, item) in
            self.txtWhyUrgent.text = item
            self.view.endEditing(true)
            
            self.selectedReasonFomList = self.jsonReasonList[index]["id"].intValue
            
            //            self.txtSelectCarType.text =
            //            self.txtSelectSeat.text = ""
            self.reasonDD.hide()
        }
    }
    
    //MARK: - DropDown setup
    
    func configDD(dropdown: DropDown, sender: UIView)
    {
        dropdown.anchorView = sender
        dropdown.direction = .any
        dropdown.dismissMode = .onTap
        //  dropdown.topOffset = CGPoint(x: 0, y: self.view.bounds.origin.y)
        dropdown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        // print("textfield Frame \(txtPurpose.frame)")
        dropdown.width = sender.bounds.width
        dropdown.cellHeight = 40.0
        dropdown.backgroundColor = UIColor.white
        dropdown.textColor = UIColor.black
        dropdown.selectionBackgroundColor = UIColor.clear
        
    }
    
    @IBAction func btnReasonTapped(_ sender: UIButton) {
        reasonDD.show()
    }
    
    @IBAction func btnSubmitTapped(_ sender: UIButton) {
        
        if (txtWhenToDeliver.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: "Please enter when to deliver")
        }
        else
        {
            alertShowForConfitmation()
        }
        
    }
    
    func alertShowForConfitmation(){
        
        var str = ""
        
        if isChangeDateButtonClick{
            str = "Are you sure to change the date?"
        }else{
            str = "Are you sure you want submit this sales order on urgent basis?"
        }
        
        let alertController = UIAlertController(title: "", message: str, preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            
            self.submitUrgentAPICalled()
            
        }
        let cancelAction = UIAlertAction(title: "No", style: UIAlertAction.Style.cancel) { (result : UIAlertAction) -> Void in
            print("Cancel")
        }
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
        
    }
    
}


extension SaleOrderUrgentVc:UITextViewDelegate
{
    func textViewDidChange(_ textView: UITextView) {
        if textView.text == ""
        {
            self.lblPlaceholder.isHidden = false
        }
        else
        {
            self.lblPlaceholder.isHidden = true
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn shouldChangeTextInRange: NSRange, replacementText text: String) -> Bool
    {
        if text == "\n"  // Recognizes enter key in keyboard
        {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}


//MARK: - TextField Delegate

extension SaleOrderUrgentVc: UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtWhenToDeliver
        {
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "CommonDateTimeVC") as! CommonDateTimeVC
            obj.pickerDelegate = self
            obj.controlType = 2
            //   obj.isSetMinimumDate = true
            // obj.setMinimumDate = Date()
            obj.modalPresentationStyle = .overCurrentContext
            obj.modalTransitionStyle = .crossDissolve
            self.present(obj, animated: true, completion: nil)
            return false
        }
        
        return true
        
    }

    
}


extension SaleOrderUrgentVc : DateTimePickerDelegate
{
    func setDateandTime(dateValue: Date, type: Int) {
        print("dateValue  - ",dateValue)
        
        self.view.endEditing(true)
        
        if type == 2
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            dateFormatter.locale =  Locale(identifier: "en_IN")
            txtWhenToDeliver.text = dateFormatter.string(from: dateValue)
        }
        
    }
}

extension SaleOrderUrgentVc{
    
    func submitUrgentAPICalled()
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(GlobalVariable.baseURL)postUrgentReport"
            
            print("URL: \(url)")
            
            
            let userID = Int(getUserDetail("user_id"))
            let param = [
                "UserId":Int(userID!),
                "UserName":getUserDetail("username"),
                "Password":getUserDetail("password"),
                "Company":getUserDetail("company_id"),
                "SaleOrderId": self.dictData["id"].intValue,
                "RequestedDate" : txtWhenToDeliver.text ?? "",
                "ReasonId": self.selectedReasonFomList,
                "Remark":txtVwNote.text ?? "",
                "IsChangeDateButtonClick" : isChangeDateButtonClick
                ] as [String : Any]
            
            print("param : \(param)")
            
            self.showLoader()
            
            CommonService().ServicePostMethod(url: url, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value{
                    print("JSON : \(json)")
                    
                    self.handlerRefreshPendingList()
                    
                    self.navigationController?.popViewController(animated: true)
                }
                else
                {
                    makeToast(strMessage: GlobalVariable.serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: GlobalVariable.interNotWorking)
        }
        
    }
    
    func reasonList(){
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(GlobalVariable.baseURL)getReasons?UserId=\(getUserDetail("user_id"))&Username=\(getUserDetail("username"))&Password=\(getUserDetail("password"))&Company=\(getUserDetail("company_id"))"
            
            print("URL: \(url)")
            
            self.showLoader()
            
            CommonService().ServiceGETMethod(url: url, param: [:]) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value{
                    print("JSON : \(json)")
                    
                    self.jsonReasonList = json.arrayValue
                    
                    for i in 0..<self.jsonReasonList.count
                    {
                        let value = self.jsonReasonList[i]["name"].stringValue
                        self.strReasonList.append(value)
                    }
                    
                    self.reasonDD.dataSource = self.strReasonList
                }
                else
                {
                    makeToast(strMessage: GlobalVariable.serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: GlobalVariable.interNotWorking)
        }
        
    }

}
