//
//  AddEditPriceRequesstTblCell.swift
//  Tinnakoran
//
//  Created by YASH on 11/2/19.
//  Copyright © 2019 Yash. All rights reserved.
//

import UIKit

class AddEditPriceRequesstTblCell: UITableViewCell {

    @IBOutlet weak var vwSelectProductName: UIView!
    @IBOutlet weak var vwEnterProductName: UIView!
    @IBOutlet weak var btnIsNewProduct: UIButton!
    @IBOutlet weak var imgProductDD: UIImageView!
    @IBOutlet weak var lblIsNewProduct: UILabel!
    
    @IBOutlet weak var txtEnterProductName: CustomTextField!
    @IBOutlet weak var txtSelectedProductName: CustomTextField!
    @IBOutlet weak var btnProductName: UIButton!
    
    @IBOutlet weak var txtQntyPerOrder: CustomTextField!
    @IBOutlet weak var txtQuantyPerYear: CustomTextField!
    @IBOutlet weak var txtUnitOfMeasurment: CustomTextField!
    @IBOutlet weak var txtCoountryOrigin: CustomTextField!
    @IBOutlet weak var txtProductCompetitorPrice: CustomTextField!
    @IBOutlet weak var txtProductCompetitorUnit: CustomTextField!
    @IBOutlet weak var txtProuctApprovedPrice: CustomTextField!
    @IBOutlet weak var txtProductApprovedUnit: CustomTextField!
    @IBOutlet weak var txtComment: CustomTextField!
    @IBOutlet weak var txtRemmark: CustomTextField!
    @IBOutlet weak var btnRemoveFromPriceList: UIButton!
    
    
    var handlerEnterproductName :(String) -> Void = {_ in}
    var handlerQntPerOrder : (Int) -> Void = {_ in}
    var handlerQntPerYear : (Int) -> Void = {_ in}
    var handlerUOM : (String) -> Void = {_ in}
    var handlerCountryOrigin : (String) -> Void = {_ in}
    var handlerCompPrice : (Int) -> Void = {_ in}
    var handlerCompUnit : (String) -> Void = {_ in}
    var handlerAppPrice : (Int) -> Void = {_ in}
    var handlerAppUnit : (String) -> Void = {_ in}
    var handlerComment : (String) -> Void = {_ in}
    var handlerRemark : (String) -> Void = {_ in}
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
        lblIsNewProduct.font = themeFont(size: 13, fontname: .regular)
        lblIsNewProduct.textColor = .black
        [txtEnterProductName,txtSelectedProductName,txtQntyPerOrder,txtQuantyPerYear,txtUnitOfMeasurment,txtCoountryOrigin,txtProductCompetitorPrice,txtProductCompetitorUnit,txtProuctApprovedPrice,txtProductApprovedUnit,txtComment,txtRemmark].forEach { (txt) in
            txt?.tag = self.tag
            txt?.delegate = self
            txt?.placeHolderColor = UIColor.lightGray
            txt?.font = themeFont(size: 13, fontname: .regular)
            txt?.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)

        }

        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension AddEditPriceRequesstTblCell:UITextFieldDelegate{
    
    
    @objc func textFieldDidChange(textField: UITextField) {
        
        let str = textField.text
        
        if textField == txtEnterProductName{
            print("str:\(str)")
            handlerEnterproductName(str ?? "")
        }else if textField == txtQntyPerOrder{
            handlerQntPerOrder(Int(str ?? "0") ?? 0)
        }
        else if textField == txtQuantyPerYear{
            handlerQntPerYear(Int(str ?? "0") ?? 0)
        }
        else if textField == txtUnitOfMeasurment{
            handlerUOM(str ?? "")
        }
        else if textField == txtCoountryOrigin{
            handlerCountryOrigin(str ?? "")
        }
        else if textField == txtProductCompetitorPrice{
            handlerCompPrice(Int(str ?? "0") ?? 0)
        }
        else if textField == txtProductCompetitorUnit{
            handlerCompUnit(str ?? "")
        }
        else if textField == txtProuctApprovedPrice{
            handlerAppPrice(Int(str ?? "0") ?? 0)
        }
        else if textField == txtProductApprovedUnit{
            handlerAppUnit(str ?? "")
        }
        else if textField == txtComment{
            handlerComment(str ?? "")
        }
        else if textField == txtRemmark{
            handlerRemark(str ?? "")
        }
        
    }
    
}
