//
//  AddEditPriceRequestVc.swift
//  Tinnakoran
//
//  Created by YASH on 11/2/19.
//  Copyright © 2019 Yash. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import AlamofireSwiftyJSON
import DropDown

class AddEditPriceRequestVc: UIViewController {
    
    @IBOutlet weak var txtName: CustomTextField!
    @IBOutlet weak var tblSalePriceRequetLine: UITableView!
    @IBOutlet weak var cosntantSalePriceRequestHeigt: NSLayoutConstraint!
    @IBOutlet weak var txtApproverName: CustomTextField!
    @IBOutlet weak var txtCustomerProduct: CustomTextField!
    @IBOutlet weak var txtRequestDate: CustomTextField!
    
    @IBOutlet weak var btnPriceRequet: CustomButton!
    @IBOutlet weak var btnSubmit: CustomButton!
    
    
    var arrayPriceRequest : [JSON] = []
    var selectedCotroller = isForAddOrEdit.add

    var arrayProductName : [JSON] = []
    
    var customerData = JSON()
    var handlerForAddPriceRequest: ()->Void = {}
    var handlerForEditPriceRequest: (JSON) -> Void = {_ in}
    
    var approverDD = DropDown()
    var selectedNameFromApproverList = ""
    var jsonApproverNameList : [JSON] = []
    var strApproverNameList = [String]()
    
    var dictData = JSON()

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        
        approverNameList()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if selectedCotroller == .add{
            setupNavigationbar(titleText: "Add Price Request")
        }else if selectedCotroller == .edit{
            setupNavigationbar(titleText: "Edit Price Request")
        }
        
        tblSalePriceRequetLine.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        tblSalePriceRequetLine.removeObserver(self, forKeyPath: "contentSize")
    }
    
    
    override func viewDidLayoutSubviews() {
        
        self.configDD(dropdown: self.approverDD, sender: self.txtApproverName)
        
        selectionIndex()
    }

    func selectionIndex()
    {
        
        self.approverDD.selectionAction = { (index, item) in
            self.txtApproverName.text = item
            self.view.endEditing(true)
            
            self.selectedNameFromApproverList = self.jsonApproverNameList[index]["id"].stringValue
            
            //            self.txtSelectCarType.text =
            //            self.txtSelectSeat.text = ""
            self.approverDD.hide()
        }

    }
    
    //MARK: - DropDown setup
    
    func configDD(dropdown: DropDown, sender: UIView)
    {
        dropdown.anchorView = sender
        dropdown.direction = .any
        dropdown.dismissMode = .onTap
        //  dropdown.topOffset = CGPoint(x: 0, y: self.view.bounds.origin.y)
        dropdown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        // print("textfield Frame \(txtPurpose.frame)")
        dropdown.width = sender.bounds.width
        dropdown.cellHeight = 40.0
        dropdown.backgroundColor = UIColor.white
        dropdown.textColor = UIColor.black
        dropdown.selectionBackgroundColor = UIColor.clear
        
    }
    
    //MARK:- Overide Method
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if object is UITableView {
            
            self.cosntantSalePriceRequestHeigt.constant = tblSalePriceRequetLine.contentSize.height
        }
    }
    
    func setupUI(){
        
        tblSalePriceRequetLine.register(UINib(nibName: "AddEditPriceRequesstTblCell", bundle: nil), forCellReuseIdentifier: "AddEditPriceRequesstTblCell")
       
        [txtName,txtApproverName,txtCustomerProduct,txtRequestDate].forEach { (txt) in
            txt?.placeHolderColor = UIColor.lightGray
            txt?.delegate = self
            txt?.font = themeFont(size: 13, fontname: .regular)
        }
        
        [btnPriceRequet,btnSubmit].forEach { (btn) in
            btn?.backgroundColor = UIColor.appThemeLightGrayColor
        }
        
        if selectedCotroller == .add{
            setArrayForAdd()
        }
        else
        {
            setData(dict: self.dictData)
        }
        
    }
    
    func setArrayForAdd(){
        
        var dict = JSON()
        dict["LineId"].intValue = 0
        dict["ProductId"].intValue = 0
        dict["ProductName"].stringValue = ""
        dict["IsNewProduct"].boolValue = false
        dict["QuantityPerOrder"].stringValue = ""
        dict["QuantityPerYear"].stringValue = ""
        dict["UnitOfMeasure"].stringValue = ""
        dict["Remark"].stringValue = ""
        dict["CountryOfOrigin"].stringValue = ""
        dict["ProductcompetitorPrice"].stringValue = ""
        dict["ProductcompetitorUnit"].stringValue = ""
        dict["ProductApprovedPrice"].stringValue = ""
        dict["ProductApprovedUnit"].stringValue = ""
        dict["ProductApprovedComment"].stringValue = ""
        
        self.arrayPriceRequest.append(dict)
        
//        var dictData = JSON()
//        dictData["ProductName"].stringValue = ""
//
//        self.arrayProductName.append(dictData)
        
        self.tblSalePriceRequetLine.reloadData()

    }
    
    func setData(dict:JSON){
        
        txtApproverName.text = dict["approver_id"][1].stringValue
        self.selectedNameFromApproverList = dict["approver_id"][0].stringValue
        txtRequestDate.text = dict["request_date"].stringValue
        txtName.text = dict["name"].stringValue
        txtCustomerProduct.text = dict["partner_fin_product"].stringValue
        
        var array = dict["saleRequestLines"].arrayValue
        
        for i in 0..<array.count{
            var dictDataProductName = JSON()
            dictDataProductName["ProductName"].stringValue = array[i]["product_id"][1].stringValue
            self.arrayProductName.append(dictDataProductName)
        }
        
        for i in 0..<array.count{
            var dict = array[i]
            
            var dictData = JSON()
            dictData["LineId"].intValue = dict["id"].intValue
            dictData["ProductId"].intValue = dict["product_id"][0].intValue
            dictData["IsNewProduct"].boolValue = dict["is_new_product"].boolValue
            dictData["ProductName"].stringValue = dict["product_name"].stringValue
            dictData["QuantityPerOrder"].stringValue = dict["quantity_time"].stringValue
            dictData["QuantityPerYear"].stringValue = dict["quantity_year"].stringValue
            dictData["UnitOfMeasure"].stringValue = dict["uom"].stringValue
            dictData["Remark"].stringValue = dict["product_comment"].stringValue
            dictData["CountryOfOrigin"].stringValue = dict["product_comp_country"].stringValue
            dictData["ProductcompetitorPrice"].stringValue = dict["product_comp_price"].stringValue
            dictData["ProductcompetitorUnit"].stringValue = dict["product_comp_unit"].stringValue
            dictData["ProductApprovedPrice"].stringValue = dict["product_app_price"].stringValue
            dictData["ProductApprovedUnit"].stringValue = dict["product_app_unit"].stringValue
            dictData["ProductApprovedComment"].stringValue = dict["product_app_comment"].stringValue
            
            array[i] = dictData
            
        }
        
        
        print("arrayPriceRequest : \(array)")
        
        self.arrayPriceRequest = array
        self.tblSalePriceRequetLine.reloadData()
    }

    @IBAction func btnApproverNameTapped(_ sender: UIButton) {
        self.approverDD.show()
    }
    @IBAction func btnAddPricerequstTapped(_ sender: UIButton) {
        self.setArrayForAdd()
    }
    
    @IBAction func btnSubmitTapped(_ sender: UIButton) {
        
        if (txtApproverName.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: "Please enter approver name")
            return
        }
        
        if arrayPriceRequest.contains(where: { (json) -> Bool in
            if json["UnitOfMeasure"].stringValue == ""{
                return true
            }
            
            return false
        }){
            makeToast(strMessage: "Please enter unit of measurment")
            return
        }
        
        if arrayPriceRequest.contains(where: { (json) -> Bool in
            if json["QuantityPerOrder"].stringValue == ""{
                return true
            }
            
            return false
        }){
            makeToast(strMessage: "Please enter quantity per time")
            return
        }
        
        if arrayPriceRequest.contains(where: { (json) -> Bool in
            if json["ProductName"].stringValue == ""{
                return true
            }
            
            return false
        }){
            makeToast(strMessage: "Please enter or select product name")
            return
        }
        
        self.AddEditPriceRequestAPICalled()
        
    }
    
    @objc func btnRemovePriceReqestList(_ sender:UIButton){
        
        if arrayPriceRequest.count == 1{
            makeToast(strMessage: "You must add atleast one sale Price Request")
        }else{
            self.arrayPriceRequest.remove(at: sender.tag)
            self.tblSalePriceRequetLine.reloadData()
        }
    }
    
    @objc func btnIsNewProductTapped(_ sender:UIButton){
        
        if let cell = tblSalePriceRequetLine.cellForRow(at: IndexPath(row: sender.tag, section: 0)) as? AddEditPriceRequesstTblCell{
            
            if sender.isSelected{
                self.arrayPriceRequest[sender.tag]["IsNewProduct"].boolValue = false
                
                sender.isSelected = false
                cell.vwSelectProductName.isHidden = false
                cell.vwEnterProductName.isHidden = true
                cell.txtSelectedProductName.text = ""
                self.arrayPriceRequest[sender.tag]["ProductName"].stringValue = ""
            }else{
                
                self.arrayPriceRequest[sender.tag]["IsNewProduct"].boolValue = true
                
                sender.isSelected = true
                
                cell.vwSelectProductName.isHidden = true
                cell.vwEnterProductName.isHidden = false
                cell.txtEnterProductName.text = ""
                self.arrayPriceRequest[sender.tag]["ProductId"].intValue = 0
                self.arrayPriceRequest[sender.tag]["ProductName"].stringValue = ""
            }
            
            print("self.arrayPriceRequest:\(self.arrayPriceRequest)")
            self.tblSalePriceRequetLine.reloadData()
        }
        
    }
    
    @objc func btnProductNameTapped(sender:UIButton){
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "ProductListVc") as! ProductListVc
        obj.selectedController = .salePriceRequest
        
        obj.handlerProductName = {[weak self] (strName,Id) in
            
            self?.arrayPriceRequest[sender.tag]["ProductId"].intValue = Id
            self?.arrayPriceRequest[sender.tag]["ProductName"].stringValue = strName
          //  self?.arrayProductName[sender.tag]["ProductName"].stringValue = strName
            
            if let cell = self?.tblSalePriceRequetLine.cellForRow(at: IndexPath(row: sender.tag, section: 0)) as? AddEditPriceRequesstTblCell{
                
                cell.txtSelectedProductName.text = strName
            }
            
            print("arr:\(self?.arrayPriceRequest[sender.tag])")
            
            self?.tblSalePriceRequetLine.reloadData()
            
        }
        
        self.navigationController?.pushViewController(obj, animated: true)

    }
}

//MARK: - TextField Delegate

extension AddEditPriceRequestVc: UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtRequestDate
        {
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "CommonDateTimeVC") as! CommonDateTimeVC
            obj.pickerDelegate = self
            obj.controlType = 1
            //   obj.isSetMinimumDate = true
            // obj.setMinimumDate = Date()
            obj.modalPresentationStyle = .overCurrentContext
            obj.modalTransitionStyle = .crossDissolve
            self.present(obj, animated: true, completion: nil)
            return false
        }
        
        return true
        
    }
    
}

extension AddEditPriceRequestVc : DateTimePickerDelegate
{
    func setDateandTime(dateValue: Date, type: Int) {
        print("dateValue  - ",dateValue)
        
        self.view.endEditing(true)
        
        if type == 1
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            dateFormatter.locale =  Locale(identifier: "en_IN")
            txtRequestDate.text = dateFormatter.string(from: dateValue) + " 00:00:00"
        }
        
    }
}


extension AddEditPriceRequestVc: UITableViewDelegate,UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.arrayPriceRequest.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddEditPriceRequesstTblCell") as! AddEditPriceRequesstTblCell
        
        var dict = self.arrayPriceRequest[indexPath.row]
        
        cell.btnRemoveFromPriceList.tag = indexPath.row
        cell.btnRemoveFromPriceList.addTarget(self, action: #selector(btnRemovePriceReqestList), for: .touchUpInside)
        
        cell.btnIsNewProduct.tag = indexPath.row
        cell.btnIsNewProduct.addTarget(self, action: #selector(btnIsNewProductTapped), for: .touchUpInside)
        
        cell.btnProductName.tag = indexPath.row
        cell.btnProductName.addTarget(self, action: #selector(btnProductNameTapped), for: .touchUpInside)
        
        cell.tag = indexPath.row
        
        if self.selectedCotroller == .add{
//            let dictData = self.arrayProductName[indexPath.row]
//            cell.txtEnterProductName.text = dictData["ProductName"].stringValue
            
            if dict["IsNewProduct"].boolValue{
                cell.txtEnterProductName.text = dict["ProductName"].stringValue
            }else{
                cell.txtSelectedProductName.text = dict["ProductName"].stringValue
            }
        }
        else{
            
            if dict["IsNewProduct"].boolValue{
                
                cell.btnIsNewProduct.isSelected = true
                cell.vwSelectProductName.isHidden = true
                cell.vwEnterProductName.isHidden = false
                
                cell.txtEnterProductName.text = dict["ProductName"].stringValue
            }else{
                cell.btnIsNewProduct.isSelected = false
                cell.vwSelectProductName.isHidden = false
                cell.vwEnterProductName.isHidden = true
                
                cell.txtSelectedProductName.text = dict["ProductName"].stringValue
            }
            
        }
        
        
        
        
//        if dict["IsNewProduct"].boolValue == true{
//            cell.vwProductName.isHidden = true
//            cell.txtProductName.text = ""
//            dict["ProductId"].intValue = 0
//            dict["ProductName"].stringValue = ""
//        }else{
//            cell.vwProductName.isHidden = false
//        }
        
        cell.txtEnterProductName.text = dict["ProductName"].stringValue
        cell.txtQntyPerOrder.text = dict["QuantityPerOrder"].stringValue
        cell.txtQuantyPerYear.text = dict["QuantityPerYear"].stringValue
        cell.txtUnitOfMeasurment.text = dict["UnitOfMeasure"].stringValue
        cell.txtRemmark.text = dict["Remark"].stringValue
        cell.txtCoountryOrigin.text = dict["CountryOfOrigin"].stringValue
        cell.txtProductCompetitorPrice.text = dict["ProductcompetitorPrice"].stringValue
        cell.txtProductCompetitorUnit.text = dict["ProductcompetitorUnit"].stringValue
        cell.txtProuctApprovedPrice.text = dict["ProductApprovedPrice"].stringValue
        cell.txtProductApprovedUnit.text = dict["ProductApprovedUnit"].stringValue
        cell.txtComment.text = dict["ProductApprovedComment"].stringValue
        
        cell.handlerEnterproductName = {[weak self] value in
            self?.arrayPriceRequest[indexPath.row]["ProductName"].stringValue = value
            
            print("array : \(self?.arrayPriceRequest)")
        }
        
        cell.handlerQntPerOrder = {[weak self] value in
            self?.arrayPriceRequest[indexPath.row]["QuantityPerOrder"].intValue = value
        }
        
        cell.handlerQntPerYear = {[weak self] value in
            self?.arrayPriceRequest[indexPath.row]["QuantityPerYear"].intValue = value
        }
        
        cell.handlerUOM = {[weak self] value in
            self?.arrayPriceRequest[indexPath.row]["UnitOfMeasure"].stringValue = value
        }
        
        cell.handlerCountryOrigin = {[weak self] value in
            self?.arrayPriceRequest[indexPath.row]["CountryOfOrigin"].stringValue = value
        }
        
        cell.handlerCompPrice = {[weak self] value in
            self?.arrayPriceRequest[indexPath.row]["ProductcompetitorPrice"].intValue = value
        }
        
        cell.handlerCompUnit = {[weak self] value in
            self?.arrayPriceRequest[indexPath.row]["ProductcompetitorUnit"].stringValue = value
        }
        
        cell.handlerAppPrice = {[weak self] value in
            self?.arrayPriceRequest[indexPath.row]["ProductApprovedPrice"].intValue = value
        }
        
        cell.handlerAppUnit = {[weak self] value in
            self?.arrayPriceRequest[indexPath.row]["ProductApprovedUnit"].stringValue = value
        }
        
        cell.handlerComment = {[weak self] value in
            self?.arrayPriceRequest[indexPath.row]["ProductApprovedComment"].stringValue = value
        }
        
        cell.handlerRemark = {[weak self] value in
            self?.arrayPriceRequest[indexPath.row]["Remark"].stringValue = value
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if self.arrayPriceRequest.count != 0{
            return "Sale Price RequestLine"
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if self.arrayPriceRequest.count != 0{
            return 50
        }
        return 0
    }
}


//MARK: - API calling

extension AddEditPriceRequestVc{
    
    func approverNameList(){
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(GlobalVariable.baseURL)getUsers?UserId=\(getUserDetail("user_id"))&Username=\(getUserDetail("username"))&Password=\(getUserDetail("password"))&Company=\(getUserDetail("company_id"))"
            
            print("URL: \(url)")
            
            self.showLoader()
            
            CommonService().ServiceGETMethod(url: url, param: [:]) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value{
                    print("JSON : \(json)")
                    
                    self.jsonApproverNameList = json.arrayValue
                    
                    for i in 0..<self.jsonApproverNameList.count
                    {
                        let value = self.jsonApproverNameList[i]["name"].stringValue
                        self.strApproverNameList.append(value)
                    }
                    
                    self.approverDD.dataSource = self.strApproverNameList
                }
                else
                {
                    makeToast(strMessage: GlobalVariable.serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: GlobalVariable.interNotWorking)
        }
        
    }
    
    
    func AddEditPriceRequestAPICalled()
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(GlobalVariable.baseURL)postSalesPriceRequest"
            
            print("URL: \(url)")
            
            let userID = Int(getUserDetail("user_id"))
            
            var param = [
                "UserId":Int(userID!),
                "UserName":getUserDetail("username"),
                "Password":getUserDetail("password"),
                "Company":getUserDetail("company_id"),
                "CustomerId": self.customerData["id"].intValue,
                "ApproverId": Int(selectedNameFromApproverList) ?? 0,
             //   "Name":txtName.text ?? "",
                "CustomerProduct" : txtCustomerProduct.text ?? "",
                "RequestDate" : txtRequestDate.text ?? ""
                ] as [String : Any]
            
            if self.selectedCotroller == .add{
                param["HeaderId"] = 0
            }else if selectedCotroller == .edit{
                param["HeaderId"] = self.dictData["id"].intValue
            }
            
//            dict["QuantityPerOrder"].intValue = 0
//            dict["QuantityPerYear"].intValue = 0
//            dict["UnitOfMeasure"].stringValue = ""
//            dict["Remark"].stringValue = ""
//            dict["CountryOfOrigin"].stringValue = ""
//            dict["ProductcompetitorPrice"].intValue = 0
//            dict["ProductcompetitorUnit"].stringValue = ""
//            dict["ProductApprovedPrice"].intValue = 0

            for i in 0..<arrayPriceRequest.count{
                var dict = arrayPriceRequest[i]
                dict["QuantityPerOrder"].intValue = Int(dict["QuantityPerOrder"].stringValue) ?? 0
                dict["QuantityPerYear"].intValue = Int(dict["QuantityPerYear"].stringValue) ?? 0
                dict["ProductcompetitorPrice"].intValue = Int(dict["ProductcompetitorPrice"].stringValue) ?? 0
                dict["ProductApprovedPrice"].intValue = Int(dict["ProductApprovedPrice"].stringValue) ?? 0
                arrayPriceRequest[i] = dict
            }
            
            print("arrayPriceRequst:\(self.arrayPriceRequest)")
            
            param["SalePriceRequestLines"] = JSON(arrayPriceRequest).arrayObject

            print("param : \(param)")
            
            self.showLoader()
            
            CommonService().ServicePostMethod(url: url, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value{
                    print("JSON : \(json)")
                    
                    if self.selectedCotroller == .add{
                        self.handlerForAddPriceRequest()
                        self.navigationController?.popViewController(animated: true)
                    }else if self.selectedCotroller == .edit{
                        //                        self.handlerForEditLoggedCalls(json)
                        self.redirectToCustomerDetailsScreen()
                    }
                    
                }
                else
                {
                    makeToast(strMessage: GlobalVariable.serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: GlobalVariable.interNotWorking)
        }
        
    }

    /*
    func AddEditPriceRequestAPICalled(){
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(GlobalVariable.baseURL)postSalesPriceRequest"
            
            print("URL: \(url)")
            
            let userID = Int(getUserDetail("user_id"))
            
            var param = [
                "UserId":Int(userID!),
                "UserName":getUserDetail("username"),
                "Password":getUserDetail("password"),
                "Company":getUserDetail("company_id"),
                "CustomerId": self.customerData["id"].intValue,
                "ApproverId": Int(selectedNameFromApproverList) ?? 0,
                "Name":txtName.text ?? "",
                "CustomerProduct" : txtCustomerProduct.text ?? "",
                "RequestDate" : txtRequestDate.text ?? ""
                ] as [String : Any]
            
            if self.selectedCotroller == .add{
                param["HeaderId"] = 0
            }else if selectedCotroller == .edit{
                // param["HeaderId"] = self.dictData["id"].intValue
            }
            var urlComponent = URLComponents(string: url)!
            let queryItems = param.map  { URLQueryItem(name: $0.key, value: $0.value as! String) }
            urlComponent.queryItems = queryItems
            
            //Now make `URLRequest` and set body and headers with it
            var request = URLRequest(url: urlComponent.url!)
            request.httpMethod = "POST"
            request.httpBody = try? JSONSerialization.data(withJSONObject: arrayPriceRequest)
            
            //Now use this URLRequest with Alamofire to make request
            Alamofire.request(request).responseJSON { response in
                //Your code
            }

            param["SalePriceRequestLines"] = try! JSONSerialization.data(withJSONObject: JSON(arrayPriceRequest).arrayObject)
            
            print("param : \(param)")
            
            self.showLoader()
            
            CommonService().ServicePostMethod(url: url, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value{
                    print("JSON : \(json)")
                    
                    //                    if self.selectedController == .add{
                    //                        self.handlerForAddLoggedCalls()
                    //                        self.navigationController?.popViewController(animated: true)
                    //                    }else if self.selectedController == .edit{
                    //                        //                        self.handlerForEditLoggedCalls(json)
                    //                        self.redirectToCustomerDetailsScreen()
                    //                    }
                    
                }
                else
                {
                    makeToast(strMessage: GlobalVariable.serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: GlobalVariable.interNotWorking)
        }
        
    }*/
}
