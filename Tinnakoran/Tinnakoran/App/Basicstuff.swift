//
//  Basicstuff.swift
//  Tinnakoran
//
//  Created by YASH on 02/10/19.
//  Copyright © 2019 Yash. All rights reserved.
//

import Foundation
import MaterialComponents
import NVActivityIndicatorView

struct GlobalVariable {
    
    static let interNotWorking = "No internet connection,Please try again later"
    static let serverNotResponding = "Server not responding,Please try again later"
    static let Defaults = UserDefaults.standard
    
    
    
//    static let baseURL = "http://tinnakorn.azurewebsites.net/"
//    static let baseURL = "http://tinnakorn.somee.com/"
    static let baseURL = "http://203.156.178.7:8000/"
    
}




//MARK: - Set Toaster

func makeToast(strMessage : String){
    
    let messageSnack = MDCSnackbarMessage()
    messageSnack.text = strMessage
    MDCSnackbarManager.show(messageSnack)
    
}

