//
//  Service.swift
//  Hand2Home
//
//  Created by YASH on 02/02/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON

struct CommonService
{
    func ServicePostMethod(url:String,param : [String:Any],completion:@escaping(Result<JSON>)-> ())
    {
        print("url - ",url)
        
        Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding.default, headers: nil).responseSwiftyJSON(completionHandler:
            {
                let statusCode = $0.response?.statusCode
                print("StatusCode : \(statusCode)")
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    print("StatusCode : \(statusCode)")
                    let str = String(decoding: $0.data ?? Data(), as: UTF8.self)
                    print("API result str - ",str)

                    if(statusCode == 500)
                    {
                        
                    }
                    else if(statusCode != nil)
                    {
                       /// APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }
                    else
                    {
                        
                        makeToast(strMessage: GlobalVariable.serverNotResponding)
                        completion($0.result)
                    }
                }
                else
                {
                       makeToast(strMessage: GlobalVariable.serverNotResponding)
//                    completion($0.result)
                }
        })
    }
 
    func ServiceGETMethod(url:String,param : [String:String],completion:@escaping(Result<JSON>)-> ())
    {
        
        print("url - ",url)
        
        Alamofire.request(url, method: .get, parameters: param, encoding: URLEncoding.default, headers: nil).responseSwiftyJSON(completionHandler:
            {
                
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    print("StatusCode : \(statusCode)")
                    if(statusCode == 500)
                    {
                        
                    }else if(statusCode != nil)
                    {
                        /// APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }
                    else
                    {
                        
                        makeToast(strMessage: GlobalVariable.serverNotResponding)
                        
                        completion($0.result)
                    }
                }else
                {
                    makeToast(strMessage: GlobalVariable.serverNotResponding)
                    
                    //                    completion($0.result)
                }
        })
    }
    
    func makeReqeusrWithMultipartData(with url: String, method: Alamofire.HTTPMethod, parameter: [String:Any]?,image:UIImage?, success: @escaping(Result<JSON>)-> (), failure: @escaping (_ error: String,_ errorcode: Int) -> Void)
    {
        /*if let param = parameter, let data = try? JSONSerialization.data(withJSONObject: param, options: .prettyPrinted) {
            print(String(data: data, encoding: .utf8) ?? "Nil Param")
        }
        let headers = ["Content-Type" : "application/json"]
        print("headers:==\(headers)")*/
        
        let headers = ["Content-Type" : "application/json"]
        
        let url = URL(string: url)!
        let jsonData = JSON(parameter).rawString()!.data(using: .utf8, allowLossyConversion: false)!
        
        var request = URLRequest(url: url)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = jsonData
        
        
        let unit64:UInt64 = 10_000_000
        //            Alamofire.uplo
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            
            for (key, value) in parameter!
            {
                print("\(key) \(value)")
//                multipartFormData.append((value).data(using: .utf8)!, withName: key)
                if let temp = value as? String {
                                            multipartFormData.append(temp.data(using: .utf8)!, withName: key)
                                        }
                                        if let temp = value as? Int {
                                            multipartFormData.append("\(temp)".data(using: .utf8)!, withName: key)
                                        }
            }
            
            if let img = image {
                let imgCertiData = img.jpegData(compressionQuality: 0.7)
                multipartFormData.append(imgCertiData!, withName: "Attachment", fileName:"images.jpeg" , mimeType: "image/jpeg")
                
            }
            
        }, usingThreshold: unit64, to: url, method: .post, headers: headers, encodingCompletion: { (encodingResult) in
            print("encoding result:\(encodingResult)")
            switch encodingResult {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (Progress) in
                    print("Upload Progress: \(Progress.fractionCompleted)")
                    //send progress using delegate
                })
                upload.responseSwiftyJSON(completionHandler: { (responce) in
                    //stop
                    switch (responce.result) {
                    case .success(let value):
                        success(responce.result)
                        /*if let jsonData = try? JSONSerialization.data(withJSONObject: value, options: .prettyPrinted) {
                            print("Response: \n",String(data: jsonData, encoding: String.Encoding.utf8) ?? "nil")
                            let res = responce.result.value! as! [String: Any?]
                            let statusCode = res["res"] as? Int
                            
                            if statusCode == 1 {
                                success(JSON(responce.result.value!))
                            }
                            else {
                                let res = responce.result.value! as! [String: Any?]
                                let msg = res["message"] as? String
                                if msg != nil {
                                    failure(msg!, statusCode ?? 403)
                                } else {
                                    failure("", statusCode ?? 403)
                                }
                            }
                        }*/
                    case .failure(let error):
                        print(error.localizedDescription)
                        failure(GlobalVariable.serverNotResponding, 401)
                    }
                })
            case .failure(let encodingError):
                //                    print(error.localizedDescription)
                failure(GlobalVariable.serverNotResponding, 401)
            }
        })
    }
    
}

