//
//  UIViewController+Extension.swift
//  Tinnakoran
//
//  Created by YASH on 02/10/19.
//  Copyright © 2019 Yash. All rights reserved.
//

import Foundation
import NVActivityIndicatorView
import SwiftyJSON

extension UIViewController{
    
    //MARK: - Storagae
    
    func getUserDetail(_ forKey: String) -> String{
        guard let userDetail = UserDefaults.standard.value(forKey: "userDetails") as? Data else { return "" }
        let data = JSON(userDetail)
        return data[forKey].stringValue
    }
    
    //MARK: - Print Font
    func printFonts()
    {
        let fontFamilyNames = UIFont.familyNames
        for familyName in fontFamilyNames {
            print("------------------------------")
            print("Font Family Name = [\(familyName)]")
            let names = UIFont.fontNames(forFamilyName: familyName )
            print("Font Names = [\(names)]")
        }
    }
    
    func addDoneButtonOnKeyboard(textfield : UITextField)
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x:0,y: 0,width: UIScreen.main.bounds.width,height: 50))
        doneToolbar.barStyle = UIBarStyle.default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem:  UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(doneButtonAction))
        done.tintColor = .appThemeGreenColor
        
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        
        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        
        textfield.inputAccessoryView = doneToolbar
        
    }
    
    @objc func doneButtonAction()
    {
        self.view.endEditing(true)
    }
    
    //MARK: - Navigation Controller Setup
    
    func setupNavigationbar(titleText:String)
    {
        
        self.edgesForExtendedLayout = UIRectEdge.init(rawValue: 0)
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.isNavigationBarHidden = false
        
        let leftButton = UIBarButtonItem(image: UIImage(named:"ic_arrow_back_header_white") , style: .plain, target: self, action: #selector(backButtonTapped))
        leftButton.tintColor = .white
        self.navigationItem.leftBarButtonItem = leftButton
        self.navigationItem.hidesBackButton = true
        
        let HeaderView = UILabel.init(frame: CGRect(x: 0, y: 0, width: 60, height: 20))
        HeaderView.isUserInteractionEnabled = false
        HeaderView.text = titleText
        HeaderView.textColor = .white
        HeaderView.font = themeFont(size: 15, fontname: .medium)
        
        self.navigationController?.navigationBar.barTintColor = UIColor.appThemeGreenColor
        
        self.navigationItem.titleView = HeaderView
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
    }
    
    @objc func backButtonTapped()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func redirectToCustomerDetailsScreen(){
        
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: CustomerDetailsVc.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
}

extension UIViewController: NVActivityIndicatorViewable {
    
    // MARK: -  For Loader NVActivityIndicatorView Process
    
    func showLoader()
    {
        let LoaderString:String = "Loading..."
        let LoaderSize = CGSize(width: 30, height: 30)
        
        startAnimating(LoaderSize, message: LoaderString, type: NVActivityIndicatorType.circleStrokeSpin)
        
    }
    
    func hideLoader()
    {
        stopAnimating()
    }
    
    func setColorAndFontString(str1:String,str1Color:UIColor = UIColor.appThemeGreenColor,str1Font:UIFont = themeFont(size: 15, fontname: .medium),str2:String,str2Color:UIColor = UIColor.appThemeLightGrayColor,str2Font:UIFont = themeFont(size: 15, fontname: .regular)) -> NSAttributedString{
        
        let attrs1 = [NSAttributedString.Key.font : str1Font, NSAttributedString.Key.foregroundColor : str1Color]
        
        let attrs2 = [NSAttributedString.Key.font : str2Font, NSAttributedString.Key.foregroundColor : str2Color]
        
        let attributedString1 = NSMutableAttributedString(string:str1, attributes:attrs1)
        
        let attributedString2 = NSMutableAttributedString(string:str2, attributes:attrs2)
        
        attributedString1.append(attributedString2)
        return attributedString1
        
    }

}

extension UIView {
    class func fromNib<T: UIView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
}

